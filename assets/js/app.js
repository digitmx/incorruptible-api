
	$( document ).ready(function() {

		//Read Values
		var base_url = $('#base_url').val();

		// Initialize collapse button
		$(".button-collapse").sideNav();
		// Initialize collapsible (uncomment the line below if you use the dropdown variation)
		//$('.collapsible').collapsible();
		
		//formLogin Validate
		$('#formLogin').validate({
			rules: {
				inputEmail: {
					required: true,
					email: true
				},
				inputPassword: {
					required: true
				}
			}
		});
		
		//btnResetApikey Click
		$('#btnResetApikey').on('click', function(e) {
			e.preventDefault();
			
			var app = $('#app').val();
			var email = $('#email').val();
			var apikey = $('#apikey').val();
			var r = confirm("¿Estás seguro de resetear el apikey? Todas las apps donde uses este apikey dejarán de funcionar hasta que lo actualices por el nuevo.");
			if (r == true) {
				$('.apikey').text('resetting...');
				var param = '{"msg": "resetApikey","fields": {"app": "' + app + '", "email": "' + email + '"}}';
				
				//Disable Button
				$('#btnResetApikey').prop( 'disabled', true );
				$('#btnResetApikey').html('PROCESANDO...');

				//API Call
				$.post(base_url + 'proccess', { param: param }).done(function( data ) {
					//Check Status Call
					if (data.status == 1)
					{
						//Change Apikey
						$('.apikey').text(data.data.apikey);
					}
					else
					{
						//Show Error
						Materialize.toast(data.msg, 4000);
						
						$('.apikey').text(apikey);
					}
					
					//Enable Button
					$('#btnResetApikey').prop( 'disabled', false );
	                $('#btnResetApikey').html('RESETEAR APIKEY');
				});
				
			} else {
			    $('.apikey').text('You pressed Cancel!');
			}
			
			return false;
		});
		
		//btnLogin Click
		$('#btnLogin').on('click', function(e) {
			e.preventDefault();
			
			//Check Valid Form
			if ($('#formLogin').valid())
			{
				//Read Values
				var email = $('#inputEmail').val();
				var password = $('#inputPassword').val();
				var param = '{"msg": "loginApp","fields": {"email": "' + email + '", "app": "' + password + '"}}';
				
				//Disable Button
				$('#btnLogin').prop( 'disabled', true );
				$('#btnLogin').html('PROCESANDO...');

				//API Call
				$.post(base_url + 'proccess', { param: param }).done(function( data ) {
					console.log( data );
					
					//Check Status Call
					if (data.status == 1)
					{
						//Reload
						location.reload();
					}
					else
					{
						//Show Error
						Materialize.toast(data.msg, 4000);

						//Enable Button
						$('#btnLogin').prop( 'disabled', false );
		                $('#btnLogin').html('INICIAR SESIÓN');
					}
				});
			}
			
			return false;
		});
		
		//formRegister Validate
		$('#formRegister').validate({
			rules: {
				inputEmail: {
					required: true,
					email: true
				},
				inputPassword: {
					required: true
				},
				inputComments: {
					required: true
				}
			}
		});
		
		//btnRegister Click
		$('#btnRegister').on('click', function(e) {
			e.preventDefault();
			
			//Check Valid Form
			if ($('#formRegister').valid())
			{
				//Read Values
				var email = $('#inputEmail').val();
				var password = $('#inputPassword').val();
				var comments = $('#inputComments').val();
				var param = '{"msg": "addApp","fields": {"email": "' + email + '", "app": "' + password + '", "comments": "' + comments + '"}}';
				
				//Disable Button
				$('#btnRegister').prop( 'disabled', true );
				$('#btnRegister').html('PROCESANDO...');

				//API Call
				$.post(base_url + 'proccess', { param: param }).done(function( data ) {
					console.log(data);
					//Check Status Call
					if (data.status == 1)
					{
						//Redirect to Admins
						Materialize.toast('Se ha registrado tu app, recibirás un correo cuando tengas habilitado el acceso a la API.', 8000);
					}
					else
					{
						//Show Error
						Materialize.toast(data.msg, 4000);
			        }
			        
			        //Enable Button
					$('#btnRegister').prop( 'disabled', false );
		            $('#btnRegister').html('REGISTRAR APP');
				});
			}
			
			return false;
		});
		
		//formUpdateProfileAdmin Validate
		$('#formUpdateProfileAdmin').validate({
			rules: {
				inputName: {
					required: true
				},
				inputEmail: {
					required: true,
					email: true
				},
				inputConfirmPassword: {
					equalTo: '#inputPassword'
				}
			}
		});
		
		//btnUpdateProfileAdmin Click
		$('#btnUpdateProfileAdmin').on('click', function(e) {
			e.preventDefault();
			
			//Check Valid Form
			if ($('#formUpdateProfileAdmin').valid())
			{
				//Read Values
				var idadmin = $('#idadmin').val();
				var name = $('#inputName').val();
				var email = $('#inputEmail').val();
				var password = $('#inputPassword').val();
				var param = '{"msg": "updateProfileAdmin","fields": {"idadmin": "' + idadmin + '", "name": "' + name + '", "email": "' + email + '", "password": "' + password + '"}}';

				//Disable Button
				$('#btnUpdateProfileAdmin').prop( 'disabled', true );
				$('#btnUpdateProfileAdmin').html('PROCESANDO...');

				//API Call
				$.post(base_url + 'api', { param: param }).done(function( data ) {
					console.log(data);
					//Check Status Call
					if (data.status == 1)
					{
						//Update Admin Name
						$('.nav-wrapper .right #admin_name').html(name + '<i class="material-icons right">arrow_drop_down</i>');
						//Reset Password Fields
						$('#inputPassword').val('');
						$('#inputConfirmPassword').val('');
					}
					
					//Show Error
					Materialize.toast(data.msg, 4000);

					//Enable Button
					$('#btnUpdateProfileAdmin').prop( 'disabled', false );
		            $('#btnUpdateProfileAdmin').html('ACTUALIZAR PERFIL');
				});
			}
			
			return false;
		});
		
		//formCreateAdmin Validate
		$('#formCreateAdmin').validate({
			rules: {
				inputName: {
					required: true
				},
				inputEmail: {
					required: true,
					email: true
				},
				inputPassword: {
					required: true
				},
				inputConfirmPassword: {
					equalTo: '#inputPassword'
				}
			}
		});
		
		//btnCreateAdmin Click
		$('#btnCreateAdmin').on('click', function(e) {
			e.preventDefault();
			
			//Check Valid Form
			if ($('#formCreateAdmin').valid())
			{
				//Read Values
				var name = $('#inputName').val();
				var email = $('#inputEmail').val();
				var password = $('#inputPassword').val();
				var param = '{"msg": "createAdmin","fields": {"name": "' + name + '", "email": "' + email + '", "password": "' + password + '"}}';
				
				//Disable Button
				$('#btnCreateAdmin').prop( 'disabled', true );
				$('#btnCreateAdmin').html('PROCESANDO...');

				//API Call
				$.post(base_url + 'api', { param: param }).done(function( data ) {
					console.log(data);
					//Check Status Call
					if (data.status == 1)
					{
						//Redirect to Admins
						window.location.href = base_url + 'admins/';
					}
					else
					{
						//Show Error
						Materialize.toast(data.msg, 4000);

						//Enable Button
						$('#btnCreateAdmin').prop( 'disabled', false );
			            $('#btnCreateAdmin').html('CREAR ADMINISTRADOR');
			        }
				});
			}
			
			return false;
		});
		
		//formUpdateAdmin Validate
		$('#formUpdateAdmin').validate({
			rules: {
				inputName: {
					required: true
				},
				inputEmail: {
					required: true,
					email: true
				},
				inputConfirmPassword: {
					equalTo: '#inputPassword'
				}
			}
		});
		
		//btnUpdateAdmin Click
		$('#btnUpdateAdmin').on('click', function(e) {
			e.preventDefault();
			
			//Check Valid Form
			if ($('#formUpdateAdmin').valid())
			{
				//Read Values
				var idadmin = $('#idadmin').val();
				var name = $('#inputName').val();
				var email = $('#inputEmail').val();
				var password = $('#inputPassword').val();
				var param = '{"msg": "updateAdmin","fields": {"idadmin": "' + idadmin + '", "name": "' + name + '", "email": "' + email + '", "password": "' + password + '"}}';

				//Disable Button
				$('#btnUpdateAdmin').prop( 'disabled', true );
				$('#btnUpdateAdmin').html('PROCESANDO...');

				//API Call
				$.post(base_url + 'api', { param: param }).done(function( data ) {
					console.log(data);
					//Check Status Call
					if (data.status == 1)
					{
						//Redirect to Admins
						window.location.href = base_url + 'admins/';
					}
					else
					{
						//Show Error
						Materialize.toast(data.msg, 4000);
	
						//Enable Button
						$('#btnUpdateAdmin').prop( 'disabled', false );
			            $('#btnUpdateAdmin').html('ACTUALIZAR ADMINISTRADOR');
			        }
				});
			}
			
			return false;
		});
		
	});