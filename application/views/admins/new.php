
		<div class="container">
			<div class="row">
				<div class="col s12 m12 l12">
					<h4 class="damask-text">Nuevo Administrador</h4>
				</div>
			</div>
			<div class="row">
				<form class="col s12" id="formCreateAdmin" name="formCreateAdmin" accept-charset="utf-8" method="post">
					<input type="hidden" id="idadmin" name="idadmin" value="" />
					<div class="row">
						<div class="input-field col s6">
							<label for="inputName">Nombre</label>
							<input placeholder="Nombre Completo" autocomplete="off" id="inputName" name="inputName" type="text" value="">
						</div>
						<div class="input-field col s6">
							<label for="inputEmail">Correo Electrónico</label>
							<input placeholder="Escribe un correo electrónico válido" autocomplete="off" id="inputEmail" name="inputEmail" type="text" value="">
						</div>
					</div>
					<div class="row">
						<div class="input-field col s6">
							<label for="inputPassword">Contraseña</label>
							<input placeholder="Dejar en blanco para no cambiar" autocomplete="off" id="inputPassword" name="inputPassword" type="password">
						</div>
						<div class="input-field col s6">
							<label for="inputConfirmPassword">Confirmar Contraseña</label>
							<input placeholder="Escribe lo mismo que en Contraseña" autocomplete="off" id="inputConfirmPassword" name="inputConfirmPassword" type="password">
						</div>
					</div>
					<div class="row">
  						<div class="col s12">
      						<center>
      							<a class="waves-effect waves-light btn acapulco" id="btnCreateAdmin">CREAR ADMINISTRADOR</a>
      						</center>
  						</div>
					</div>
				</form>
			</div>
		</div>