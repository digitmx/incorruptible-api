
		<div class="container">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="right">
						<div class="space20"></div>
						<a href="<?php echo base_url(); ?>/admins/new" class="waves-effect waves-light btn damask">Nuevo<span class="hide-on-med-and-down"> Administrador</span></a>
					</div>
					<h4 class="damask-text">Administradores</h4>
				</div>
			</div>
			<div class="row">
				<div class="col s12">
					<table class="striped responsive-table">
						<thead>
							<tr>
								<th data-field="id">#</th>
								<th data-field="name">Nombre</th>
								<th data-field="email">Correo Electrónico</th>
								<th data-field="actions">Acciones</th>
          					</tr>
        				</thead>

						<tbody>
							<?php foreach ($admins as $admin) { ?>
							<tr>
								<td><?php echo $admin['idadmin']; ?></td>
								<td><?php echo $admin['name']; ?></td>
								<td><?php echo $admin['email']; ?></td>
								<td>
									<a class="waves-effect waves-light btn damask" href="<?php echo base_url(); ?>admins/edit/<?php echo $admin['idadmin']; ?>">Editar</a> 
									<a class="waves-effect waves-light btn gray" href="<?php echo base_url(); ?>admins/delete/<?php echo $admin['idadmin']; ?>">Borrar</a>
								</td>
          					</tr>
          					<?php } ?>
        				</tbody>
      				</table>
				</div>
			</div>
		</div>