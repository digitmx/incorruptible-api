		
		<?php $slug = $this->uri->segment(1, 0); ?>
		<?php $admin_name = (isset($_SESSION['admin']['name'])) ? (string)trim($_SESSION['admin']['name']) : ''; ?>
		<div class="navbar-fixed">
			<nav class="damask">
				<div class="nav-wrapper">
					<a href="#!" class="brand-logo">
						<img src="<?php echo base_url(); ?>/assets/img/logo.png" class="responsive-img" />
					</a>
					<?php if (isset($_SESSION['admin'])) { ?>
					<a href="#" data-activates="mobile_menu" class="button-collapse white-text"><i class="material-icons">menu</i></a>
					<ul class="right hide-on-med-and-down">
						<li<?=($slug=='dashboard') ? ' class="active"' : ''; ?>><a class="white-text bold" href="<?php base_url(); ?>/dashboard">Dashboard</a></li>
						<li<?=($slug=='users') ? ' class="active"' : ''; ?>><a class="white-text bold" href="<?php base_url(); ?>/users">Usuarios</a></li>
						<li<?=($slug=='admins') ? ' class="active"' : ''; ?>><a class="white-text bold" href="<?php base_url(); ?>/admins">Administradores</a></li>
						<li<?=($slug=='profile') ? ' class="active"' : ''; ?>><a class="dropdown-button white-text bold" href="#!" data-activates="admin_menu" id="admin_name"><?php echo $admin_name; ?><i class="material-icons right">arrow_drop_down</i></a></li>
        			</ul>
        			<?php } ?>
      			</div>
    		</nav>
    		<ul class="side-nav damask" id="mobile_menu">
	    		<?php if (isset($_SESSION['admin'])) { ?>
		    		<li<?=($slug=='dashboard') ? ' class="active"' : ''; ?>><a class="white-text bold" href="<?php base_url(); ?>/dashboard">Dashboard</a></li>
					<li<?=($slug=='users') ? ' class="active"' : ''; ?>><a class="white-text bold" href="<?php base_url(); ?>/users">Usuarios</a></li>
					<li<?=($slug=='admins') ? ' class="active"' : ''; ?>><a class="white-text bold" href="<?php base_url(); ?>/admins">Administradores</a></li>
					<li class="divider"></li>
					<li<?=($slug=='profile') ? ' class="active"' : ''; ?>><a class="white-text bold" href="<?php base_url(); ?>/profile">Perfil</a></li>
					<li><a class="white-text bold" href="<?php base_url(); ?>logout">Cerrar Sesión</a></li>
				<?php } ?>
      		</ul>
    		<ul id="admin_menu" class="dropdown-content damask">
				<li><a class="white-text bold" href="<?php base_url(); ?>/profile">Perfil</a></li>
				<li class="divider"></li>
				<li><a class="white-text bold" href="<?php base_url(); ?>/logout">Cerrar Sesión</a></li>
			</ul>
			
  		</div>