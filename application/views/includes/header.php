<?php
    $controller_slug = $this->uri->segment(1);
    if (!$controller_slug) { $controller_slug = 'home'; }
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="es"> <!--<![endif]-->
	<head>
		<meta name="robots" content="noindex,nofollow">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="format-detection" content="telephone=no">
		<meta name="apple-mobile-web-app-title" content="Incorruptible App Backend">
		<title>Incorruptible App Backend</title>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.ico" type="image/x-icon; charset=binary">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.ico" type="image/x-icon; charset=binary">
		<link rel="stylesheet" id="fontMaterialDesign-css" href="//fonts.googleapis.com/icon?family=Material+Icons" type="text/css" media="all">
		<link rel="stylesheet" id="styles-css" href="<?php echo base_url(); ?>assets/css/materialize.min.css" type="text/css" media="all">
		<link rel="stylesheet" id="styles-css" href="<?php echo base_url(); ?>assets/css/app.css" type="text/css" media="all">
        <script>(function(h){h.className = h.className.replace('no-js', 'js')})(document.documentElement)</script>
		<!-- HTML5 shim and Respond.js IE8 support for HTML5 elements and media queries. -->
		<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/js/html5shiv-printshiv.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/respond.min.js"></script>
        <![endif]-->
	</head>
	<body class="<?php echo $controller_slug; ?>">
		<!--[if lt IE 8>
			<p class="browsehappy">
				You are using an <strong>outdated</strong> browser.
				Please <a href="http://browsehappy.com/">upgrade your browser</a>
				to improve your experience.
			</p>
	    <![endif]-->