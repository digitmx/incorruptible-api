
		<div class="container">
			<div class="row">
				<div class="col s12 m12 l12">
					<!--<div class="right">
						<div class="space20"></div>
						<a href="<?php echo base_url(); ?>users/create" class="waves-effect waves-light btn damask">Nuevo<span class="hide-on-med-and-down"> Usuario</span></a>
					</div>-->
					<h4 class="damask-text">Usuarios</h4>
				</div>
			</div>
			<div class="row">
				<div class="col s12">
					<table class="striped responsive-table">
						<thead>
							<tr>
								<th data-field="number">ID</th>
								<th data-field="name">Nombre</th>
								<th data-field="email">Correo Electrónico</th>
								<th data-field="actions">Acciones</th>
          					</tr>
        				</thead>

						<tbody>
							<?php foreach ($users as $user) { ?>
							<tr>
								<td><?php echo $user->iduser; ?></td>
								<td><?php echo $user->name; ?></td>
								<td><?php echo $user->email; ?></td>
								<td>
									<a class="waves-effect waves-light btn acapulco" href="<?php echo base_url(); ?>users/edit/<?php echo $user->iduser; ?>">Editar</a> 
									<!--<a class="waves-effect waves-light btn gray" href="<?php echo base_url(); ?>users/delete/<?php echo $user->iduser; ?>">Borrar</a>-->
								</td>
          					</tr>
          					<?php } ?>
        				</tbody>
      				</table>
				</div>
			</div>
			<div class="row">
				<?php if ($page > 1) { ?>
				<div class="left">
					<div class="space20"></div>
					<a href="<?php echo base_url(); ?>users/index/<?php echo (int)$page-1; ?>" class="waves-effect waves-light btn damask">Anterior</a>
				</div>
				<?php } ?>
				<?php $contador = (int)$page * 100; ?>
				<?php if ($contador < count($users_all)) { ?> 
				<div class="right">
					<div class="space20"></div>
					<a href="<?php echo base_url(); ?>users/index/<?php echo (int)$page+1; ?>" class="waves-effect waves-light btn damask">Siguiente</a>
				</div>
				<?php } ?>
			</div>
		</div>