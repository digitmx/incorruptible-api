
		<main>
			<div class="container">
				<div class="row">
					<div class="col s12">
						<div class="space40"></div>
						<span class="block font20 centered">Tu apikey es:</span>
						<span class="block apikey centered bold"><?php echo $_SESSION['app']['apikey']; ?></span>
						<input type="hidden" id="app" name="app" value="<?php echo $_SESSION['app']['app']; ?>">
						<input type="hidden" id="email" name="email" value="<?php echo $_SESSION['app']['email']; ?>">
						<input type="hidden" id="apikey" name="apikey" value="<?php echo $_SESSION['app']['apikey']; ?>">
						<center>
  							<a class="waves-effect waves-light btn acapulco" id="btnResetApikey">RESETEAR APIKEY</a>
  						</center>
						<div class="space20"></div>
						<span class="block font20 bold uppercase">Últimos Logs</span>
						<div class="logs">
							<div class="log">
								<div class="space10"></div>
								<?php if (count($logs) > 0) { ?>
								<ul class="collapsible popout" data-collapsible="accordion">
									<?php foreach ($logs as $log) { ?>
								    <li>
								    	<div class="collapsible-header">
									    	<i class="material-icons">expand_more</i>
									    	<b>msg:</b> <?php echo $log->msg; ?>, <b>date:</b> <?php echo $log->createdAt; ?>
									    </div>
										<div class="collapsible-body">
									    	<div class="fields">
										    	<span class="block bold">fields:</span>
										    	<?php
											    	$json = json_decode($log->fields);
											    	$json = json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
											    	printf("<pre>%s</pre>", $json);
											    ?>
									    	</div>
									    	<div class="response">
										    	<span class="block bold">response:</span>
										    	<?php
											    	$json = json_decode($log->response);
											    	$json = json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
											    	printf("<pre>%s</pre>", $json);
											    ?>
									    	</div>
										</div>
								    </li>
								    <?php } ?>
								</ul>
								<?php } else { ?>
								<span class="block bold">No existen logs con esta app.</span>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>