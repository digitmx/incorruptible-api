
		<main>
			<div class="container">
				<div class="row">
					<div class="col s12 call">
						<div class="space40"></div>
						<?php $calls = explode(',', $_SESSION['app']['permissions']); ?>
						<?php if (in_array($call, $calls)) { ?>
						<span class="block font20"><?php echo $call; ?></span>
						<br/>
						<span class="block font15"><?=(isset($call_content->description)) ? $call_content->description : 'Sin descripción.'; ?></span>
						<br/>
						<span class="block font15"><b>Endpoint:</b> <?=(isset($call_content->endpoint)) ? $call_content->endpoint : 'Sin endpoint.'; ?></span>
						<br/>
						<span class="block font15"><b>Método:</b> <?=(isset($call_content->method)) ? $call_content->method : 'Sin método.'; ?></span>
						<span class="block font15"><?=(isset($call_content->fields)) ? $call_content->fields : 'Falta especificar los campos requeridos.'; ?></span>
						<span class="block font15"><?=(isset($call_content->response)) ? $call_content->response : 'Falta especificar la respuesta de la llamada.'; ?></span>
						<?php } else { ?>
						<span class="block font20 centered">No tienes permiso para utilizar esta llamada.</span>
						<?php } ?>
					</div>
				</div>
			</div>
		</main>