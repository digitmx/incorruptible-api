		
		<?php $slug = $this->uri->segment(2, 0); ?>
		<?php $admin_name = (isset($_SESSION['app']['app'])) ? (string)trim($_SESSION['app']['app']) : ''; ?>
		<div class="navbar-fixed">
			<nav class="damask">
				<div class="nav-wrapper">
					<a href="<?php echo base_url(); ?>v2" class="brand-logo right">
						<img src="<?php echo base_url(); ?>/assets/img/logo.png" class="responsive-img" />
					</a>
					<?php if (isset($_SESSION['app'])) { ?>
					<a href="#" data-activates="mobile_menu" class="button-collapse white-text"><i class="material-icons">menu</i></a>
					<?php } ?>
      			</div>
    		</nav>
    		<?php if (isset($_SESSION['app'])) { $permissions = explode(',', $_SESSION['app']['permissions']); ?>
    		<ul class="side-nav fixed white" id="mobile_menu">
	    		
    			<li><div class="centered"><span class="black-text bold">Hola <?php echo $admin_name; ?></span></div></li>
				<li<?=($slug=='dashboard') ? ' class="active"' : ''; ?>><a class="black-text bold" href="<?php echo base_url(); ?>v2/dashboard" style="padding: 0 16px;">Dashboard</a></li>
				<li>
					<ul class="collapsible collapsible-accordion">
						<li class="bold"><a class="collapsible-header waves-effect waves-orange">Llamadas</a>
						<div class="collapsible-body">
						    <ul>
							    <?php foreach ($permissions as $permission) { ?>
								<li><a href="<?php echo base_url(); ?>v2/call/<?php echo $permission; ?>"><?php echo $permission; ?></a></li>
								<?php } ?>
						    </ul>
						  </div>
						</li>
					</ul>
				</li>
				<li><a class="black-text bold" href="<?php echo base_url(); ?>v2/logout" style="padding: 0 16px;">Cerrar Sesión</a></li>
      		</ul>
      		<?php } ?>
			
  		</div>