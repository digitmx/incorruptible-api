
		<input type="hidden" id="base_url" name="base_url" value="<?php echo base_url(); ?>" />

		<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery-3.2.0.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/materialize.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/validation/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/validation/additional-methods.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/validation/localization/messages_es.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.form.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/app.js?v=2.3"></script>
		<!--
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdONyfCadeDvw20F1Q03eJj80WnGmCZ4g&callback=initMap"></script>	
		-->

	</body>
</html>