
		<div class="container">
			<div class="row">
				<div class="col s12 m8 offset-m2 l6 offset-l3">
					<div class="space100"></div>
					<div class="row">
						<form class="col s12" id="formLogin" name="formLogin" accept-charset="utf-8" method="post">
							<div class="row">
								<div class="input-field col s12">
									<label for="inputEmail">Correo Electrónico</label>
									<input placeholder="Escribe un correo electrónico válido" autocomplete="off" id="inputEmail" name="inputEmail" type="text">
        						</div>
      							<div class="input-field col s12">
	      							<label for="inputPassword">App Registrada</label>
									<input placeholder="App Registrada" autocomplete="off" id="inputPassword" name="inputPassword" type="text">
        						</div>
      						</div>
      						<div class="row">
	      						<div class="col s12">
		      						<center>
		      							<a class="waves-effect waves-light btn acapulco" id="btnLogin">INICIAR SESIÓN</a>
		      						</center>
	      						</div>
      						</div>
						</form>
					</div>
				</div>
			</div>
		</div>