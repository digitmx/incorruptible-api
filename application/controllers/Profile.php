<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		//Read Session
		$logged = (isset($_SESSION['iaap_logged'])) ? $_SESSION['iaap_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url() ); }
	}

	public function index()
	{
		//Create JSON Request
		$array = array(
			'msg' => 'getAdminProfile',
			'fields' => array(
				'idadmin' => $_SESSION['admin']['idadmin']
			)
		);
		$json_array = json_encode($array);

		//Request Call
		$response = $this->functions->call($json_array);
		$response_row = json_decode($response, true);

		//Check Request Call Status
		if ((int)$response_row['status'] == 1)
		{
			//Read Areas
			$data['admin'] = $response_row['data'];
			
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('includes/navbar');
			$this->load->view('admins/profile', $data);
			$this->load->view('includes/footer');
		}
		else
		{
			//Redirect Dashboard
			redirect( base_url() . 'dashboard' );
		}		
	}
	
}