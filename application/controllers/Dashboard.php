<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		//Read Session
		$logged = (isset($_SESSION['iapi_logged'])) ? $_SESSION['iapi_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url() ); }
	}

	public function index()
	{
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('dashboard/index');
		$this->load->view('includes/footer');	
	}
	
}