<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		//Delete Session
		session_destroy();

		//Redirect to Login
		redirect( base_url() );
	}
}