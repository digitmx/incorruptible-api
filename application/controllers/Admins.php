<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admins extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		//Read Session
		$logged = (isset($_SESSION['iaap_logged'])) ? $_SESSION['iaap_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url() ); }
	}

	public function index()
	{
		//Leemos la página
		$page = $this->uri->segment(3,1);
		
		//Create JSON Request
		$array = array(
			'msg' => 'getAdmins',
			'fields' => array(
				'page' => $page
			)
		);
		$json_array = json_encode($array);

		//Request Call
		$response = $this->functions->call($json_array);
		$response_row = json_decode($response, true);

		//Check Request Call Status
		if ((int)$response_row['status'] == 1)
		{
			//Read Areas
			$data['admins'] = $response_row['data'];
			
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('includes/navbar');
			$this->load->view('admins/index', $data);
			$this->load->view('includes/footer');	
		}
		else
		{
			//Redirect Dashboard
			redirect( base_url() . 'dashboard' );
		}	
	}
	
	public function new()
	{
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('admins/new');
		$this->load->view('includes/footer');
	}
	
	public function edit()
	{
		//Leemos la página
		$idadmin = $this->uri->segment(3,0);
		
		//Veriicamos
		if ($idadmin)
		{
			//Create JSON Request
			$array = array(
				'msg' => 'getAdmin',
				'fields' => array(
					'idadmin' => $idadmin
				)
			);
			$json_array = json_encode($array);
	
			//Request Call
			$response = $this->functions->call($json_array);
			$response_row = json_decode($response, true);
	
			//Check Request Call Status
			if ((int)$response_row['status'] == 1)
			{
				//Read Areas
				$data['admin'] = $response_row['data'];
				
				//Load Views
				$this->load->view('includes/header');
				$this->load->view('includes/navbar');
				$this->load->view('admins/edit', $data);
				$this->load->view('includes/footer');	
			}
			else
			{
				//Redirect Dashboard
				redirect( base_url() . 'dashboard' );
			}
		}
		else
		{
			//Redirect Admins
			redirect( base_url() . 'admins' );
		}
	}
	
	public function delete()
	{
		//Leemos la página
		$idadmin = $this->uri->segment(3,0);
		
		//Veriicamos
		if ($idadmin)
		{
			//Actualizamos el status
			$data = array(
				'status' => 0
			);
			$this->db->where('idadmin', $idadmin);
			$this->db->update('admin', $data);
			
			//Redirect Admins
			redirect( base_url() . 'admins' );
		}
		else
		{
			//Redirect Admins
			redirect( base_url() . 'dashboard' );
		}
	}
	
}