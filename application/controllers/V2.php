<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class V2 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		error_reporting(E_ALL);
		ini_set("display_errors", 1);
	}

	public function index()
	{
		//Leemos los parametros
		$json = (isset($_POST['param'])) ? $_POST['param'] : NULL;
		if (!$json) { $json = file_get_contents("php://input"); }
		
		//Comprobamos que el parametro no es NULO
		if ($json != NULL)
		{
			//Decodificamos los valores del JSON
			$json_decode = json_decode($json, true);
			
			//Leemos el Mensaje del JSON
			$msg = (isset($json_decode['msg'])) ? $json_decode['msg'] : '';

			//Leemos los Campos del JSON
			$fields = (isset($json_decode['fields'])) ? $json_decode['fields'] : '';

			//Leemos el Identificador de la App
			$app = (isset($json_decode['app'])) ? $json_decode['app'] : '';

			//Leemos el APIKEY de la App
			$apikey = (isset($json_decode['apikey'])) ? $json_decode['apikey'] : '';
			
			//Leemos la Plataforma
			$platform = (isset($json_decode['platform'])) ? $json_decode['platform'] : '';

			//Ejecutamos el JSON que mandaron
			$this->engine2->executeJSON($msg,$fields,$app,$apikey,$platform);
		}
		else
		{
			//Redirect Home
			$this->load->view('v2/index');
		}
	}
	
	public function login()
	{
		//Read Session
		$logged = (isset($_SESSION['iapi_logged'])) ? $_SESSION['iapi_logged'] : false;
		
		//Check Session
		if ($logged) { redirect( base_url().'v2/dashboard' ); }
		
		//Load Views
		$this->load->view('v2/includes/header');
		$this->load->view('v2/includes/navbar');
		$this->load->view('v2/login');
		$this->load->view('v2/includes/footer');	
	}
	
	public function register()
	{
		//Read Session
		$logged = (isset($_SESSION['iapi_logged'])) ? $_SESSION['iapi_logged'] : false;
		
		//Check Session
		if ($logged) { redirect( base_url().'v2/dashboard' ); }
		
		//Load Views
		$this->load->view('v2/includes/header');
		$this->load->view('v2/includes/navbar');
		$this->load->view('v2/register');
		$this->load->view('v2/includes/footer');	
	}
	
	public function dashboard()
	{
		//Read Session
		$logged = (isset($_SESSION['iapi_logged'])) ? $_SESSION['iapi_logged'] : false;
		
		//Check Session
		if (!$logged) { redirect( base_url().'v2/login' ); }
		else
		{
			//Consultamos los logs de la cuenta
			$query = $this->db->query("SELECT * FROM log WHERE app = '" . $_SESSION['app']['app'] . "' AND status = 1 ORDER BY createdAt DESC LIMIT 10");
			$data['logs'] = $query->result();
			
			//Load Views
			$this->load->view('v2/includes/header');
			$this->load->view('v2/includes/navbar');
			$this->load->view('v2/dashboard', $data);
			$this->load->view('v2/includes/footer');	
		}
	}
	
	public function call()
	{
		//Read Session
		$logged = (isset($_SESSION['iapi_logged'])) ? $_SESSION['iapi_logged'] : false;
		
		//Check Session
		if (!$logged) { redirect( base_url().'v2/login' ); }
		else
		{
			//Consultamos los logs de la cuenta
			$call = (string)trim($this->uri->segment(3));
			$query = $this->db->query("SELECT * FROM `call` WHERE app = '" . $call . "' AND status = 1 LIMIT 1");
			$data['call'] = $call;
			$data['call_content'] = $query->row();
			
			//Load Views
			$this->load->view('v2/includes/header');
			$this->load->view('v2/includes/navbar');
			$this->load->view('v2/call', $data);
			$this->load->view('v2/includes/footer');	
		}
	}
	
	public function logout()
	{
		//Delete Session
		session_destroy();

		//Redirect to Login
		redirect( base_url() . 'v2' );
	}
}