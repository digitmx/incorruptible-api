<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proccess extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		error_reporting(E_ALL);
		ini_set("display_errors", 1);
	}

	public function index()
	{
		//Leemos los parametros
		$json = (isset($_POST['param'])) ? $_POST['param'] : NULL;
		if (!$json) { $json = file_get_contents("php://input"); }
		
		//Comprobamos que el parametro no es NULO
		if ($json != NULL)
		{
			//Decodificamos los valores del JSON
			$json_decode = json_decode($json, true);
						//Leemos el Mensaje del JSON
			$msg = (isset($json_decode['msg'])) ? $json_decode['msg'] : '';

			//Leemos los Campos del JSON
			$fields = (isset($json_decode['fields'])) ? $json_decode['fields'] : '';
			
			//Consultamos el Usuario de API Manager
			$query = $this->db->query('SELECT * FROM app WHERE app = "inc_am" LIMIT 1');
			$row = $query->row();

			//Leemos el Identificador de la App
			$app = $row->app;

			//Leemos el APIKEY de la App
			$apikey = $row->apikey;

			//Ejecutamos el JSON que mandaron
			$this->engine2->executeJSON($msg,$fields,$app,$apikey,'web');
		}
		else
		{
			//Redirect Home
			$array = array(
				'status' => (int)0,
				'msg' => (string)'Faltan campos.'
			);

			//Imprimimos el Arreglo
			$this->engine2->printJSON($array);
			$output = TRUE;
		}
	}
	
}