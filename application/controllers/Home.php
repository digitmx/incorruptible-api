<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		//Read Session
		$logged = (isset($_SESSION['iapi_logged'])) ? $_SESSION['iapi_logged'] : false;

		//Check Session
		if ($logged) { redirect( base_url().'dashboard' ); }
	}
	
	public function index()
	{
		//Load Views - Plus
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('home/index');
		$this->load->view('includes/footer');
	}
}
