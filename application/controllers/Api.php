<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		error_reporting(E_ALL);
		ini_set("display_errors", 1);
	}

	public function index()
	{
		//Leemos los parametros
		$json = (isset($_POST['param'])) ? $_POST['param'] : NULL;
		if (!$json) { $json = file_get_contents("php://input"); }
		
		//Comprobamos que el parametro no es NULO
		if ($json != NULL)
		{
			//Decodificamos los valores del JSON
			$json_decode = json_decode($json, true);

			//Leemos el Mensaje del JSON
			$msg = (isset($json_decode['msg'])) ? $json_decode['msg'] : '';

			//Leemos los Campos del JSON
			$fields = (isset($json_decode['fields'])) ? $json_decode['fields'] : '';

			//Leemos el Identificador de la App
			$app = (isset($json_decode['app'])) ? $json_decode['app'] : '';

			//Leemos el APIKEY de la App
			$apikey = (isset($json_decode['apikey'])) ? $json_decode['apikey'] : '';

			//Ejecutamos el JSON que mandaron
			$this->engine->executeJSON($msg,$fields,$app,$apikey);
		}
		else
		{
			//Redirect Home
			$this->load->view('welcome_message');
		}
	}
}