<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Explorer extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		error_reporting(E_ALL);
		ini_set("display_errors", 1);
	}

	public function index()
	{
		//Mostramos el Formulario
		$this->load->view('explorer/index');
	}
	
	public function v2()
	{
		//Mostramos el Formulario
		$this->load->view('explorer/v2');
	}

}