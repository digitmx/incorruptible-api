<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		//Read Session
		$logged = (isset($_SESSION['iaap_logged'])) ? $_SESSION['iaap_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url() ); }
	}

	public function index()
	{
		//Leemos los Datos
		$page = $this->uri->segment(3,1);
		$limit = '10';
		$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
			
		//Consultamos los Usuarios
		$query = $this->db->query("SELECT * FROM user WHERE status = 1 ORDER BY name ASC LIMIT " . $offset . "," . $limit);
		$data['users'] = $query->result();
		$query = $this->db->query("SELECT * FROM user WHERE status = 1 ORDER BY name ASC");
		$data['users_all'] = $query->result();
		$data['page'] = $page;
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('users/index', $data);
		$this->load->view('includes/footer');		
	}
	
}