<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		//Redirect Login
		redirect( base_url() );		
	}
	
	public function demo()
	{
		/*
		//Data
		$data['name'] = 'Emiliano';
		$data['token'] = 'http://app.incorruptible.mx/#/reset/1/nodo123';
		
		//Correo de Notificación
		$subject = 'Restablecer Contraseña';
		$body = $this->load->view('mail/reset_password', $data, TRUE);
		$altbody = strip_tags($body);
						
		//Mandamos el Correo
		$mail = new PHPMailer(true);
		$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
		$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
		$mail->CharSet = 'UTF-8';
		$mail->Encoding = "quoted-printable";
        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
        $mail->SMTPDebug  = 1;
        $mail->SetFrom("incorruptible@nodo.pw", utf8_encode('Incorruptible App'));  //Quien envía el correo
        $mail->AddReplyTo("incorruptible@nodo.pw", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
        $mail->Body      = $body;
        $mail->AltBody    = $altbody;
        $mail->AddAddress('milio.hernandez@gmail.com', 'Emiliano Hernández');
        */
	}
	
	public function newUser()
	{
		//Data
		$data = array(
			'iduser' => '3',
			'email' => 'gerardo@nodo.pw',
			'password' => 'pass',
			'name' => 'Gerardo Vidal',
			'idprofile' => '1',
		    'profile' => 'Usuario',
			'link' => 'https://app.incorruptible.mx/#/verify/1/prueba'
		);
		
		//Show Mail Template
		$this->load->view('mail/welcome_user', $data);
	}
	
	public function newUserONG()
	{
		//Data
		$data = array(
			'iduser' => '3',
			'email' => 'gerardo@nodo.pw',
			'password' => 'pass',
			'name' => 'Gerardo Vidal',
			'idprofile' => '2',
		    'profile' => 'ONG',
			'link' => 'https://app.incorruptible.mx'
		);
		
		//Show Mail Template
		$this->load->view('mail/welcome_user_ong', $data);
	}
	
	public function newAdmin()
	{
		//Data
		$data['email'] = 'incorruptible@nodo.pw';
		$data['password'] = 'nodo123';
		$data['link'] = base_url();
		
		//Show Mail Template
		$this->load->view('mail/welcome_admin', $data);
	}
	
	public function resetPassword()
	{
		//Data
		$data['name'] = 'Emiliano';
		$data['token'] = 'http://app.incorruptible.mx/#/reset/1/nodo123';
		
		//Show Mail Template
		$this->load->view('mail/reset_password', $data);
	}
	
	public function messageDependency()
	{
		//Data
		$data['name'] = 'Emiliano';
		$data['link'] = 'http://app.incorruptible.mx/#/viewcomplaint/34';
		
		//Show Mail Template
		$this->load->view('mail/message_dependency', $data);
	}
	
	public function responseUser()
	{
		//Data
		$data['name'] = 'Emiliano';
		$data['link'] = 'http://app.incorruptible.mx/#/viewcomplaint/34';
		
		//Show Mail Template
		$this->load->view('mail/response_user', $data);
	}
	
	public function sendGroup()
	{
		$data = array(
			"idgroup" => 1,
	        "title" => "Título de Prueba Grupo",
	        "description" => "Descripción del Grupo de Prueba",
	        "level" => "Estatal",
	        "iddependency" => "4",
	        "dependency" => "Administración Federal de Servicios Educativos en el Distrito Federal",
	        "iduser" => "18",
	        "email" => "emiliano@digit.mx",
	        "name" => "Emiliano Hernández",
	        "send" => "1",
	        "complaints" => array(
	            [
	                "idgroup_complaint" => "1",
	                "idgroup" => "1",
	                "idcomplaint" => "35",
	                "title" => "IMSS",
	                "description" => "En enero del 2012, la OCDE calculó que sin corrupción, el IMSS ahorraría 36% de su gasto en medicinas.",
	                "lat" => "19.42362344918704",
	                "lng" => "-99.17335143056869",
	                "address" => "Paseo de la Reforma 476, Juárez, 06600 Ciudad de México, CDMX, México",
	                "type" => "image",
	                "media" => "",
	                "idcategory" => "3",
	                "idamount" => "7",
	                "icon" => "2",
	                "iduser" => "8",
	                "amount" => "100000"
	            ],
	            [
	                "idgroup_complaint" => "2",
	                "idgroup" => "1",
	                "idcomplaint" => "33",
	                "title" => "Estela de luz",
	                "description" => "Instalaciones inmobiliarias para industrias servicios SA de CV duplicó el precio del acero utilizado para las columnas que pagó el Gob. Fed.",
	                "lat" => "19.423071989789463",
	                "lng" => "-99.17568082015987",
	                "address" => "Punto Chapultepec, Cuauhtémoc, 06500 Ciudad de México, CDMX, México",
	                "type" => "image",
	                "media" => "",
	                "idcategory" => "3",
	                "idamount" => "7",
	                "icon" => "2",
	                "iduser" => "8",
	                "amount" => "100000"
	            ]
	        )
		);
		
		//Show Mail Template
		$this->load->view('mail/send_group', $data);
	}
	
}