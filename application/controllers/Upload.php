<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// How to send the response header with PHP
if (isset($_SERVER['HTTP_ORIGIN'])) {
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		exit(0);
}

ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 10000);

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Aws;
use Aws\Common\Enum\Size;
use Aws\S3\Enum\CannedAcl;
use Aws\Common\Enum\Region;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\Common\Model\MultipartUpload\AbstractUploadBuilder;

class Upload extends CI_Controller {

	public $bucket = 'incorruptible.app'; //nombre del bucket

    //private $_key = 'AKIAIGA3MI32SFTQGHBQ'; // tu key
    //private $_key = 'AKIAJ6COTGMIJ7WLYIOQ';
    private $_key = 'AKIAJSFPQRYMB63ETS6Q';

    //private $_secret = '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'; // tu secret
    //private $_secret = 'Zv7UjMyRFq0/pnr1hOPXPPC++3KeoL8vpAVeYS2s';
    private $_secret = '/WsxEU3M43vo4PdZIrVZbP4gshWUgR34KA7OSpCk';

    public $s3 = null;

	public function __construct()
	{
		parent::__construct();
		
		$this->s3 = S3Client::factory(array(
			'version' => '2006-03-01',
			'region' => 'us-east-2',
			'credentials' => array(
                'key'    => $this->_key,
                'secret' => $this->_secret,
            )
        ));
	}
	
	public function index()
	{
		//Leemos el Tipo de Archivo
		$type = (isset($_POST['type'])) ? (string)trim($_POST['type']) : '';
		
		//Revisamos si existe un archivo
		if(isset($_FILES['file']['tmp_name'][0]))
		{
			//Creamos la ruta del archivo
			$filename = explode('.', $_FILES['file']['name']);
			$id_array = count($filename) - 1;
			
			if ($type == 'video')
			{
				$key = time() . '.mp4';
			}
			
			if ($type == 'audio')
			{
				$key = time() . '.mp3';
			}
			
			if ($type == 'image')
			{
				$key = time() . '.' . strtolower($filename[$id_array]);
			}
			
			$key = $type . '/' . $key;
			$fullPath = $_FILES['file']['tmp_name'];

			//Subimos a S3
			$result = $this->s3->upload(
	            $this->bucket, //bucket
	            $key, //key, unique by each object
	            fopen($fullPath, 'rb'), //where is the file to upload?
	            'public-read' //permissions
	        );
			$new_url = $result['ObjectURL'];
			$new_url = str_replace('%2F', '/', $new_url);
			$new_url = str_replace('https://s3.us-east-2.amazonaws.com/incorruptible.app', 'https://d3dfgsq3gyj74u.cloudfront.net', $new_url);
			
			echo $new_url;
		}
		else
		{
			echo 'error';
		}
	}
	
	public function file()
	{
		//Leemos el Tipo de Archivo
		$type = (isset($_POST['type'])) ? (string)trim($_POST['type']) : '';
		$name = (isset($_POST['name'])) ? (string)trim($_POST['name']) : '';
		$size = (isset($_POST['size'])) ? (string)trim($_POST['size']) : '';
		$fulltype = (isset($_POST['fulltype'])) ? (string)trim($_POST['fulltype']) : '';
		$idcomplaint = (isset($_POST['idcomplaint'])) ? (string)trim($_POST['idcomplaint']) : '';
		$iduser = (isset($_POST['iduser'])) ? (string)trim($_POST['iduser']) : '';
		$key = '';
		
		//Revisamos si existe un archivo
		if(isset($_FILES['file']['tmp_name'][0]))
		{
			//Creamos la ruta del archivo
			$filename = explode('.', $_FILES['file']['name']);
			$id_array = count($filename) - 1;
			
			if ($type == 'video')
			{
				$key = time() . '_' . $type . '.mp4';
			}
			
			if ($type == 'audio')
			{
				$key = time() . '_' . $type . '.mp3';
			}
			
			if ($type == 'image')
			{
				$key = time() . '_' . $type . '.' . strtolower($filename[$id_array]);
			}
			
			if (!$key) { $key = time() . '_' . $type . '.' . strtolower($filename[$id_array]); }
			
			$key = $type . '/' . $key;
			$fullPath = $_FILES['file']['tmp_name'];

			//Subimos a S3
			$result = $this->s3->upload(
	            $this->bucket, //bucket
	            $key, //key, unique by each object
	            fopen($fullPath, 'rb'), //where is the file to upload?
	            'public-read' //permissions
	        );
			$new_url = $result['ObjectURL'];
			$new_url = str_replace('%2F', '/', $new_url);
			$new_url = str_replace('https://s3.us-east-2.amazonaws.com/incorruptible.app', 'https://d3dfgsq3gyj74u.cloudfront.net', $new_url);
			
			//Insertamos en la base de datos
			$data = array(
				'name' => $name,
				'size' => $size,
				'type' => $fulltype,
				'url' => $new_url,
				'idcomplaint' => $idcomplaint,
				'iduser' => $iduser,
				'createdAt' => date('Y-m-d H:i:s'),
			    'status' => 1
			);
			$this->db->insert('file', $data);
			$idfile = $this->db->insert_id();
			
			//Consultamos al Admin
			$query = $this->db->query("SELECT * FROM file WHERE idcomplaint = " . $idcomplaint . " ORDER BY createdAt ASC LIMIT 10");
			
			//Generamos el Arreglo
			$array = array(
				'status' => (int)1,
				'msg' => 'success',
				'data' => $query->result()
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
		else
		{
			//Mostrar Error
			$array = array(
				'status' => (int)0,
				'msg' => (string)'Ocurrió un error al subir el archivo. Intenta de nuevo más tarde.',
				'data' => array()
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
	}
	
	public function file_new()
	{
		//Leemos el Tipo de Archivo
		$type = (isset($_POST['type'])) ? (string)trim($_POST['type']) : '';
		$name = (isset($_POST['name'])) ? (string)trim($_POST['name']) : '';
		$size = (isset($_POST['size'])) ? (string)trim($_POST['size']) : '';
		$fulltype = (isset($_POST['fulltype'])) ? (string)trim($_POST['fulltype']) : '';
		$key = '';
		
		//Revisamos si existe un archivo
		if(isset($_FILES['file']['tmp_name'][0]))
		{
			//Creamos la ruta del archivo
			$filename = explode('.', $_FILES['file']['name']);
			$id_array = count($filename) - 1;
			
			if ($type == 'video')
			{
				$key = time() . '_' . $type . '.mp4';
			}
			
			if ($type == 'audio')
			{
				$key = time() . '_' . $type . '.mp3';
			}
			
			if ($type == 'image')
			{
				$key = time() . '_' . $type . '.' . strtolower($filename[$id_array]);
			}
			
			if (!$key) { $key = time() . '_' . $type . '.' . strtolower($filename[$id_array]); }
			
			$key = $type . '/' . $key;
			$fullPath = $_FILES['file']['tmp_name'];

			//Subimos a S3
			$result = $this->s3->upload(
	            $this->bucket, //bucket
	            $key, //key, unique by each object
	            fopen($fullPath, 'rb'), //where is the file to upload?
	            'public-read' //permissions
	        );
			$new_url = $result['ObjectURL'];
			$new_url = str_replace('%2F', '/', $new_url);
			$new_url = str_replace('https://s3.us-east-2.amazonaws.com/incorruptible.app', 'https://d3dfgsq3gyj74u.cloudfront.net', $new_url);
			
			//Insertamos en la base de datos
			$data = array(
				'name' => $name,
				'size' => $size,
				'type' => $fulltype,
				'url' => $new_url
			);
			
			//Generamos el Arreglo
			$array = array(
				'status' => (int)1,
				'msg' => 'success',
				'data' => $data
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
		else
		{
			//Mostrar Error
			$array = array(
				'status' => (int)0,
				'msg' => (string)'Ocurrió un error al subir el archivo. Intenta de nuevo más tarde.',
				'data' => array()
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
	}
	
	//Funcion para identar JSON
	public function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	public function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo $this->indent($json);
	}
}