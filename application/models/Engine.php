<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// How to send the response header with PHP
if (isset($_SERVER['HTTP_ORIGIN'])) {
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		exit(0);
}

ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 10000);

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Aws;
use Aws\Common\Enum\Size;
use Aws\S3\Enum\CannedAcl;
use Aws\Common\Enum\Region;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\Common\Model\MultipartUpload\AbstractUploadBuilder;
use Aws\Sns\SnsClient;
use Aws\Sns\Exception\SnsException;
use Aws\Credentials\Credentials;

class Engine extends CI_Model {
	
	public $bucket = 'incorruptible'; //nombre del bucket

    private $_key = 'AKIAJ6COTGMIJ7WLYIOQ';

    private $_secret = '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';

    public $s3 = null;
    
	public $SNS_ACCESS_KEY = 'AKIAIK64RPVE6OOHGWLQ';
	public $SNS_SECRET_KEY = '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';

	public function __construct()
	{
		parent::__construct();
		
		$this->s3 = S3Client::factory(array(
			'version' => '2006-03-01',
			'region' => 'us-west-2',
            'credentials' => array(
                'key'    => $this->_key,
                'secret' => $this->_secret,
            )
        ));
	}

	//Funcion para identar JSON
	public function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	public function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo $this->indent($json);
	}

	//Manejador de Mensajes Recibidos en el JSON
	public function executeJSON($msg,$fields,$app,$apikey)
	{
		//Leemos la Configuracion
		$output = FALSE;
		
		//s3signin
		if ($msg == 's3signin')
		{
			//Leemos los Datos
			$type = (isset($fields['type'])) ? (string)trim($fields['type']) : '';
			$filename = now();
			$s3Client = S3Client::factory(array(
				'version' => '2006-03-01',
				'region' => 'us-west-2',
				'credentials' => array(
	                'key'    => 'AKIAJRCNU3A53GB532PQ',
	                'secret' => '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',
	            )
	        ));
			
			//Agregamos la Extensión
			switch ($type)
			{
				case 'video' : $filename = $filename . '.mov'; break;
				case 'audio' : $filename = $filename . '.wav'; break;
				case 'video' : $filename = $filename . '.mov'; break;
			}
			
			$cmd = $s3Client->getCommand('GetObject', [
		        'Bucket' => 'incorruptible/'.$type,
		        'Key'    => $filename
		    ]);
		
		    $request = $s3Client->createPresignedRequest($cmd, '+20 minutes');
		    $presignedUrl = (string) $request->getUri();
		    $presignedUrl = str_replace('%2F', '/', $presignedUrl);
		   
			//Generamos el Arreglo
			$array = array(
				'status' => (int)1,
				'msg' => 'success',
				'data' => array(
					'filename' => $filename,
					'url' => $presignedUrl,
					'request' => $request
				)
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
		
		// loginAdmin
		if ($msg == 'loginAdmin')
		{
			//Leemos los datos
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los datos
			if ($email && $password)
			{
				//Consultamos los datos
				$query = $this->db->query("SELECT * FROM admin WHERE email = '" . $email . "' AND status = 1 LIMIT 1");

				//Verificamos si existe el usuario
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Leemos el Hash
					$hash = $row->password;
					
					//Verificamos el Password
					if (password_verify($password, $hash)) 
					{
						//Consultamos
						$data = array(
							'idadmin' => $row->idadmin,
						    'email' => $row->email,
						    'name' => $row->name
						);
	
						//Save Admin in $_SESSION
						$this->session->set_userdata('admin', $data);
						$this->session->set_userdata('iaap_logged', true);
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Contraseña incorrecta. Intenta de nuevo.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Correo Electrónico no existe. Intenta de nuevo.'
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}

		// loginUser
		if ($msg == 'loginUser')
		{
			//Leemos los datos
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los datos
			if ($name && $password)
			{
				//Verificar si es email
				if ($this->functions->verificaremail($name))
				{
					//Consultamos los datos
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $name . "' AND status = 1 LIMIT 1");
				}
				else
				{
					//Consultamos los datos
					$query = $this->db->query("SELECT * FROM user WHERE name = '" . $name . "' AND status = 1 LIMIT 1");
				}

				//Verificamos si existe el usuario
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Consultamos el Perfil del Usuario
					$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row->idprofile . " AND status = 1");
					$row_profile = $query_profile->row();
										
					//Consultamos las Denuncias del Usuario
					$query_complaints = $this->db->query("SELECT * FROM complaint WHERE iduser = " . $row->iduser . " AND status = 1");
					
					//Consultamos las Denuncias Seguidas del Usuario
					$query_follow_complaints = $this->db->query("SELECT fc.* FROM follow_complaint as fc LEFT JOIN complaint as c ON c.idcomplaint = fc.idcomplaint WHERE fc.iduser = " . $row->iduser . " AND fc.status = 1 AND c.status = 1");
					
					//Leemos el Hash
					$hash = $row->password;
					
					//Verificamos el Password
					if (password_verify($password, $hash)) 
					{
						//Consultamos
						$data = array(
							'iduser' => $row->iduser,
							'name' => $row->name,
						    'email' => $row->email,
						    'avatar' => $row->avatar,
						    'fbid' => $row->fbid,
						    'twid' => $row->twid,
						    'complaints' => $query_complaints->num_rows(),
						    'follows' => $query_follow_complaints->num_rows(),
						    'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name
						);
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Contraseña incorrecta. Intenta de nuevo.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Usuario no existe. Intenta de nuevo.'
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// createUser
		if ($msg == 'createUser')
		{
			//Leemos los Datos
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los Datos
			if ($name && $email && $password)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() == 0)
				{
					//Guardamos los Datos
					$data = array(
						'email' => $email,
						'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
					    'name' => $name,
					    'createdAt' => date('Y-m-d H:i:s'),
					    'idprofile' => 1,
					    'status' => 1
					);
					$this->db->insert('user', $data);
					$iduser = $this->db->insert_id();
					
					//Consultamos el Perfil del Usuario
					$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = 1 AND status = 1");
					$row_profile = $query_profile->row();
					
					$data = array(
						'iduser' => $iduser,
						'email' => $email,
						'password' => $password,
						'name' => $name,
						'idprofile' => $row_profile->idprofile,
					    'profile' => $row_profile->name,
						'link' => 'https://app.incorruptible.mx'
					);
					
					$data_result = array(
						'iduser' => $iduser,
						'name' => $name,
						'email' => $email,
						'avatar' => '',
						'idprofile' => $row_profile->idprofile,
					    'profile' => $row_profile->name
					);

					//Correo de Notificación
					$subject = 'Bienvenido a Incorruptible';
					$body = $this->load->view('mail/welcome_user', $data, TRUE);
					$altbody = strip_tags($body);

					//Verificamos
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Mandamos el Correo
						$mail = new PHPMailer(true);
						$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
						$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
						$mail->CharSet = 'UTF-8';
						$mail->Encoding = "quoted-printable";
				        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
				        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
				        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
				        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
				        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
				        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
				        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
				        $mail->SMTPDebug  = 0;
				        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
				        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
				        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
				        $mail->Body      = $body;
				        $mail->AltBody    = $altbody;
				        $mail->AddAddress($email, $name);
				        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');

				        if ($mail->Send())
						{
							$response = true;
						}
						else
						{
							$response = false;
						}
					}

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data_result
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Este correo de usuario ya esta registrado.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'fields' => $fields
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// createUserONG
		if ($msg == 'createUserONG')
		{
			//Leemos los Datos
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los Datos
			if ($name && $email && $password)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() == 0)
				{
					//Guardamos los Datos
					$data = array(
						'email' => $email,
						'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
					    'name' => $name,
					    'createdAt' => date('Y-m-d H:i:s'),
					    'idprofile' => 2,
					    'status' => 1
					);
					$this->db->insert('user', $data);
					$iduser = $this->db->insert_id();
					
					//Consultamos el Perfil del Usuario
					$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = 2 AND status = 1");
					$row_profile = $query_profile->row();
					
					$data = array(
						'iduser' => $iduser,
						'email' => $email,
						'password' => $password,
						'name' => $name,
						'idprofile' => $row_profile->idprofile,
					    'profile' => $row_profile->name,
						'link' => 'https://app.incorruptible.mx'
					);
					
					$data_result = array(
						'iduser' => $iduser,
						'name' => $name,
						'email' => $email,
						'avatar' => '',
						'idprofile' => $row_profile->idprofile,
					    'profile' => $row_profile->name
					);

					//Correo de Notificación
					$subject = 'Bienvenido a Incorruptible';
					$body = $this->load->view('mail/welcome_user_ong', $data, TRUE);
					$altbody = strip_tags($body);

					//Verificamos
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Mandamos el Correo
						$mail = new PHPMailer(true);
						$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
						$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
						$mail->CharSet = 'UTF-8';
						$mail->Encoding = "quoted-printable";
				        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
				        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
				        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
				        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
				        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
				        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
				        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
				        $mail->SMTPDebug  = 0;
				        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
				        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
				        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
				        $mail->Body      = $body;
				        $mail->AltBody    = $altbody;
				        $mail->AddAddress($email, $name);
				        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');

				        if ($mail->Send())
						{
							$response = true;
						}
						else
						{
							$response = false;
						}
					}

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data_result
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Este correo de usuario ya esta registrado.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'fields' => $fields
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// forgotPassword
		if ($msg == 'forgotPassword')
		{
			//Leemos los Datos
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			
			//Verificamos los Datos
			if ($email)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Revisamos que el Usuario no sea de Facebook
					if ((string)trim($row->fbid))
					{
						//Generamos el Arreglo
						$array = array(
							'status' => (int)0,
							'msg' => 'Este usuario usa Facebook para iniciar sesión.'
						);
					}
					else
					{
						//Token
						$token = sha1(now());
						//$token = substr($code, 0, 8);
						
						//Creamos el código de invite
						$data = array(
							'token' => $token
						);
						$this->db->where('iduser', $row->iduser);
						$this->db->update('user', $data);
						
						//Enviamos el Correo
						$name = $row->name;
						$name_array = explode(' ', $name);
						$data['name'] = $name_array[0];
						$data['token'] = 'http://app.incorruptible.mx/#/reset/' . $row->iduser . '/' . $token;
						
						//Correo de Notificación
						$subject = 'Restablecer Contraseña';
						$body = $this->load->view('mail/reset_password', $data, TRUE);
						$altbody = strip_tags($body);
						$email = $row->email;
				
						//Verificamos
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Mandamos el Correo
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
					        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
					        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email, $name);
					        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
				
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'Se enviará un correo electrónico con un link para restablecer la contraseña.',
							'response' => $response
						);
					}

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Este correo de usuario no esta registrado.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'fields' => $fields
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// resetPassword
		if ($msg == 'resetPassword')
		{
			//Leemos los Datos
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			$token = (isset($fields['token'])) ? (string)trim($fields['token']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
			
			//Verificamos los Datos
			if ($iduser && $token && $password)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM user WHERE iduser = '" . $iduser . "' AND token = '" . $token . "' LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Revisamos que el Usuario no sea de Facebook
					if ((string)trim($row->fbid))
					{
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'Este usuario usa Facebook para iniciar sesión.'
						);
					}
					else
					{
						//Creamos el código de invite
						$data = array(
							'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
							'token' => ''
						);
						$this->db->where('iduser', $row->iduser);
						$this->db->update('user', $data);
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'Se ha actualizado la contraseña correctamente.'
						);
					}

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'El token ya ha sido utilizado.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'fields' => $fields
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getUsers
		if ($msg == 'getUsers')
		{
			//Leemos los Campos
			$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
			$limit = '10';
			$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
			
			$query = $this->db->query("SELECT * FROM user ORDER BY createdAt DESC LIMIT " . $offset . "," . $limit);
			$users = $query->result();
			$query = $this->db->query("SELECT * FROM user ORDER BY createdAt DESC");
			$users_all = $query->result();
			
			//Generamos el Arreglo
			$array = array(
				'status' => (int)1,
				'msg' => 'success',
				'data' => array(
					'users' => $users,
					'users_all' => $users_all,
					'page' => $page
				)
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
		
		// getUser
		if ($msg == 'getUser')
		{
			//Leemos los Datos
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '1';

			//Verificamos
			if ($iduser)
			{
				//Consultamos
				$query = $this->db->query("SELECT * FROM user WHERE iduser = " . $iduser . " LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admin
					$row = $query->row();

					//Consultamos el Perfil del Usuario
					$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row->idprofile . " AND status = 1");
					$row_profile = $query_profile->row();
										
					//Consultamos las Denuncias del Usuario
					$query_complaints = $this->db->query("SELECT * FROM complaint WHERE iduser = " . $row->iduser . " AND status = 1");
					
					//Consultamos las Denuncias Seguidas del Usuario
					$query_follow_complaints = $this->db->query("SELECT fc.* FROM follow_complaint as fc LEFT JOIN complaint as c ON c.idcomplaint = fc.idcomplaint WHERE fc.iduser = " . $row->iduser . " AND fc.status = 1 AND c.status = 1");

					//Consultamos
					$data = array(
						'iduser' => $row->iduser,
						'name' => $row->name,
					    'email' => $row->email,
					    'avatar' => $row->avatar,
					    'fbid' => $row->fbid,
					    'twid' => $row->twid,
					    'complaints' => $query_complaints->num_rows(),
					    'follows' => $query_follow_complaints->num_rows(),
					    'idprofile' => $row_profile->idprofile,
					    'profile' => $row_profile->name
					);
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe el usuario.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// updateUser
		if ($msg == 'updateUser')
		{
			//Leemos los Datos
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$profile = (isset($fields['profile'])) ? (string)trim($fields['profile']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los Datos
			if ($iduser && $name)
			{
				//Consultamos al Usuario
				$query = $this->db->query("SELECT * FROM user WHERE iduser = " . $iduser . " AND status = 1 LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Actualizamos los Datos
					$data = array();
					$data['name'] = $name;
					if ($password) { $data['password'] = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]); }
					$data['idprofile'] = $profile;
					$data['updatedAt'] = date('Y-m-d H:i:s');
					$this->db->where('iduser', $iduser);
					$this->db->update('user', $data);

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'Usuario actualizado con éxito.'
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe este usuario.'
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// deleteUser
		if ($msg == 'deleteUser')
		{
			//Leemos el Id User
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos
			if ($iduser)
			{
				//Actualizamos el status
				$data = array(
					'status' => 0
				);
				$this->db->where('iduser', $iduser);
				$this->db->update('user', $data);
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $iduser
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// activateUser
		if ($msg == 'activateUser')
		{
			//Leemos el Id User
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos
			if ($iduser)
			{
				//Actualizamos el status
				$data = array(
					'status' => 1
				);
				$this->db->where('iduser', $iduser);
				$this->db->update('user', $data);
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $iduser
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getAdminProfile
		if ($msg == 'getAdminProfile')
		{
			//Leemos los Campos
			$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '';

			//Verificamos los Datos
			if ($idadmin)
			{
				//Consultamos los Datos del Admin
				$query = $this->db->query("SELECT * FROM admin WHERE idadmin = " . $idadmin . " AND status = 1 LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Leemos el objeto
					$row = $query->row();

					//Generamos el Elemento de Admin
					$admin = array(
						'idadmin' => $row->idadmin,
						'name' => $row->name,
						'email' => $row->email
					);

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $admin
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe este administrador.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}

		}

		// updateProfileAdmin
		if ($msg == 'updateProfileAdmin')
		{
			//Leemos los Datos
			$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '';
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los Datos
			if ($idadmin && $name)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM admin WHERE idadmin = " . $idadmin . " AND status = 1 LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Actualizamos los Datos
					$data = array();
					$data['name'] = $name;
					if ($password) { $data['password'] = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]); }
					$this->db->where('idadmin', $idadmin);
					$this->db->update('admin', $data);

					//Actualizamos los datos de SESSION
					$data = array(
						'idadmin' => $idadmin,
					    'email' => $email,
					    'name' => $name
					);
					$this->session->set_userdata('admin', $data);

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'Administrador actualizado con éxito.',
						'data' => $data
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe este administrador.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}

		// createAdmin
		if ($msg == 'createAdmin')
		{
			//Leemos los Datos
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los Datos
			if ($name && $email && $password)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM admin WHERE email = '" . $email . "' LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() == 0)
				{
					//Guardamos los Datos
					$data = array(
						'email' => $email,
						'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
					    'name' => $name,
					    'createdAt' => date('Y-m-d H:i:s'),
					    'status' => 1
					);
					$this->db->insert('admin', $data);
					$idadmin = $this->db->insert_id();
					
					$data = array(
						'idadmin' => $idadmin,
						'email' => $email,
						'password' => $password,
						'name' => $name,
						'link' => base_url()
					);

					//Correo de Notificación
					$subject = 'Bienvenido a Incorruptible';
					$body = $this->load->view('mail/welcome_admin', $data, TRUE);
					$altbody = strip_tags($body);

					//Verificamos
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Mandamos el Correo
						$mail = new PHPMailer(true);
						$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
						$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
						$mail->CharSet = 'UTF-8';
						$mail->Encoding = "quoted-printable";
				        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
				        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
				        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
				        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
				        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
				        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
				        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
				        $mail->SMTPDebug  = 0;
				        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
				        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
				        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
				        $mail->Body      = $body;
				        $mail->AltBody    = $altbody;
				        $mail->AddAddress($email, $name);
				        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');

				        if ($mail->Send())
						{
							$response = true;
						}
						else
						{
							$response = false;
						}
					}

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Este correo de administrador ya esta registrado.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}

		// getAdmins
		if ($msg == 'getAdmins')
		{
			//Leemos los Datos
			$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
			$limit = '10';
			$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);

			//Consultamos
			$query = $this->db->query("SELECT * FROM admin WHERE status = 1 ORDER BY idadmin DESC LIMIT " . $offset . "," . $limit);
			$query_all = $this->db->query("SELECT * FROM admin WHERE status = 1");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Admins
				$admins = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$admins[] = array(
						'idadmin' => $row->idadmin,
						'email' => $row->email,
						'name' => $row->name,
						'status' => $row->status
					);
				}

				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $admins,
					'page' => $page,
					'offset' => $offset,
					'limit' => $limit,
					'total' => (string)$query_all->num_rows()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay administradores.',
					'data' => array(),
					'page' => $page,
					'offset' => $offset,
					'limit' => $limit,
					'total' => (string)$query_all->num_rows()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}

		// getAdmin
		if ($msg == 'getAdmin')
		{
			//Leemos los Datos
			$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '1';

			//Verificamos
			if ($idadmin)
			{
				//Consultamos
				$query = $this->db->query("SELECT * FROM admin WHERE idadmin = " . $idadmin . " LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admin
					$row = $query->row();

					//Generamos el Elemento de Area
					$admin = array(
						'idadmin' => $row->idadmin,
						'email' => $row->email,
						'name' => $row->name,
						'status' => $row->status
					);

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $admin
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe el administrador.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// deleteAdmin
		if ($msg == 'deleteAdmin')
		{
			//Leemos el Id Admin
			$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '';
			
			//Verificamos
			if ($idadmin)
			{
				//Actualizamos el status
				$data = array(
					'status' => 0
				);
				$this->db->where('idadmin', $idadmin);
				$this->db->update('admin', $data);
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $idadmin
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}

		// updateAdmin
		if ($msg == 'updateAdmin')
		{
			//Leemos los Datos
			$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '';
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los Datos
			if ($idadmin && $name)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM admin WHERE idadmin = " . $idadmin . " AND status = 1 LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Actualizamos los Datos
					$data = array();
					$data['name'] = $name;
					if ($password) { $data['password'] = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]); }
					$this->db->where('idadmin', $idadmin);
					$this->db->update('admin', $data);

					//Actualizamos los datos de SESSION
					$data = array(
						'idadmin' => $idadmin,
					    'email' => $email,
					    'name' => $name
					);
					$this->session->set_userdata('admin', $data);

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'Administrador actualizado con éxito.',
						'data' => $data
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe este administrador.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getCategories
		if ($msg == 'getCategories')
		{
			//Leemos los Datos
			$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
			$limit = '10';
			$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);

			//Consultamos
			$query = $this->db->query("SELECT * FROM category WHERE category.status = 1 ORDER BY category.order ASC");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Admins
				$categories = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$categories[] = array(
						'idcategory' => $row->idcategory,
						'name' => $row->name,
						'icon' => $row->icon,
						'order' => $row->order
					);
				}

				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $categories
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay categorías.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getAmounts
		if ($msg == 'getAmounts')
		{
			//Leemos los Datos
			$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
			$limit = '10';
			$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);

			//Consultamos
			$query = $this->db->query("SELECT * FROM amount WHERE amount.status = 1 ORDER BY amount.order ASC");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Admins
				$amounts = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$amounts[] = array(
						'idamount' => $row->idamount,
						'value' => $row->value,
						'order' => $row->order
					);
				}

				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $amounts
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay cantidades asignadas.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// saveComplaint
		if ($msg == 'saveComplaint')
		{
			//Leemos los Datos
			$title = (isset($fields['title'])) ? (string)trim($fields['title']) : '';
			$description = (isset($fields['description'])) ? (string)trim($fields['description']) : '';
			$lat = (isset($fields['lat'])) ? (string)trim($fields['lat']) : '';
			$lng = (isset($fields['lng'])) ? (string)trim($fields['lng']) : '';
			$address = (isset($fields['address'])) ? (string)trim($fields['address']) : '';
			$type = (isset($fields['type'])) ? (string)trim($fields['type']) : '';
			$media = (isset($fields['media'])) ? (string)trim($fields['media']) : '';
			$idcategory = (isset($fields['idcategory'])) ? (string)trim($fields['idcategory']) : '';
			$idamount = (isset($fields['idamount'])) ? (string)trim($fields['idamount']) : '';
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos Data
			if ($title && $description && $address && $type && $idcategory && $idamount && $iduser)
			{
				//Guardamos los Datos
				$data = array(
					'title' => $title,
					'description' => $description,
				    'lat' => $lat,
				    'lng' => $lng,
				    'address' => $address,
				    'type' => $type,
				    'media' => $media,
				    'idcategory' => $idcategory,
				    'idamount' => $idamount,
				    'icon' => 1,
				    'iduser' => $iduser,
				    'createdAt' => date('Y-m-d H:i:s'),
				    'status' => 0
				);
				$this->db->insert('complaint', $data);
				$idcomplaint = $this->db->insert_id();
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $idcomplaint
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getComplaints
		if ($msg == 'getComplaints')
		{
			//Leer Valores
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Consultamos
			$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 ORDER BY c.idcomplaint ASC");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Admins
				$complaints = array();
				$complaints_user = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$complaints[] = array(
						'idcomplaint' => $row->idcomplaint,
						'title' => $row->title,
						'description' => $row->description,
						'lat' => $row->lat,
						'lng' => $row->lng,
						'address' => $row->address,
						'type' => $row->type,
						'media' => $row->media,
						'idcategory' => $row->idcategory,
						'category' => $row->category,
						'idamount' => $row->idamount,
						'amount' => $row->amount,
						'icon' => $row->icon,
						'iduser' => $row->iduser,
						'date' => $row->date,
						'status' => $row->status
					);
				}
				
				//Vericamos el usuario
				if ($iduser)
				{
					//Consultamos
					$query_user = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status IN (0,1) AND c.iduser = ".$iduser." ORDER BY c.idcomplaint DESC");
					
					//Procesamos los Admins
					foreach ($query_user->result() as $row_user)
					{
						//Generamos el Elemento de Area
						$complaints_user[] = array(
							'idcomplaint' => $row_user->idcomplaint,
							'title' => $row_user->title,
							'description' => $row_user->description,
							'lat' => $row_user->lat,
							'lng' => $row_user->lng,
							'address' => $row_user->address,
							'type' => $row_user->type,
							'media' => $row_user->media,
							'idcategory' => $row_user->idcategory,
							'category' => $row_user->category,
							'idamount' => $row_user->idamount,
							'amount' => $row_user->amount,
							'icon' => $row_user->icon,
							'iduser' => $row_user->iduser,
							'date' => $row_user->date,
							'status' => $row->status
						);
					}
				}
				
				//Reset Duplicados
				$array_complaints= array_merge($complaints,$complaints_user);
				$duplicados = array_map("unserialize", array_unique(array_map("serialize", $array_complaints)));
				array_values($duplicados);

				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $duplicados
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay denuncias creadas.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getComplaintsByPage
		if ($msg == 'getComplaintsByPage')
		{
			//Leer Valores
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
			$limit = '10';
			$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
			
			$query = $this->db->query("SELECT * FROM user ORDER BY createdAt DESC LIMIT " . $offset . "," . $limit);
			$users = $query->result();
			$query = $this->db->query("SELECT * FROM user ORDER BY createdAt DESC");
			$users_all = $query->result();
			
			//Consultamos
			$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 ORDER BY c.idcomplaint DESC LIMIT " . $offset . "," . $limit);
			$query_all = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 ORDER BY c.idcomplaint DESC");
			$complaints_all = $query_all->result();

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Admins
				$complaints = array();
				$complaints_user = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$complaints[] = array(
						'idcomplaint' => $row->idcomplaint,
						'title' => $row->title,
						'description' => $row->description,
						'lat' => $row->lat,
						'lng' => $row->lng,
						'address' => $row->address,
						'type' => $row->type,
						'media' => $row->media,
						'idcategory' => $row->idcategory,
						'category' => $row->category,
						'idamount' => $row->idamount,
						'amount' => $row->amount,
						'icon' => $row->icon,
						'iduser' => $row->iduser,
						'date' => $row->date,
						'status' => $row->status
					);
				}
				
				//Vericamos el usuario
				if ($iduser)
				{
					//Consultamos
					$query_user = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status IN (0,1) AND c.iduser = ".$iduser." ORDER BY c.idcomplaint DESC");
					
					//Procesamos los Admins
					foreach ($query_user->result() as $row_user)
					{
						//Generamos el Elemento de Area
						$complaints_user[] = array(
							'idcomplaint' => $row_user->idcomplaint,
							'title' => $row_user->title,
							'description' => $row_user->description,
							'lat' => $row_user->lat,
							'lng' => $row_user->lng,
							'address' => $row_user->address,
							'type' => $row_user->type,
							'media' => $row_user->media,
							'idcategory' => $row_user->idcategory,
							'category' => $row_user->category,
							'idamount' => $row_user->idamount,
							'amount' => $row_user->amount,
							'icon' => $row_user->icon,
							'iduser' => $row_user->iduser,
							'date' => $row_user->date,
							'status' => $row->status
						);
					}
				}
				
				//Reset Duplicados
				$array_complaints= array_merge($complaints,$complaints_user);
				$duplicados = array_map("unserialize", array_unique(array_map("serialize", $array_complaints)));
				array_values($duplicados);

				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => array(
						'complaints' => $duplicados,
						'complaints_all' => $complaints_all,
						'page' => $page
					)
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay denuncias creadas.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}		

		// getComplaints
		if ($msg == 'getComplaintsONG')
		{
			//Consultamos
			$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status IN (0,1) ORDER BY c.idcomplaint ASC");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Admins
				$complaints = array();
				$complaints_user = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$complaints[] = array(
						'idcomplaint' => $row->idcomplaint,
						'title' => $row->title,
						'description' => $row->description,
						'lat' => $row->lat,
						'lng' => $row->lng,
						'address' => $row->address,
						'type' => $row->type,
						'media' => $row->media,
						'idcategory' => $row->idcategory,
						'category' => $row->category,
						'idamount' => $row->idamount,
						'amount' => $row->amount,
						'icon' => $row->icon,
						'iduser' => $row->iduser,
						'date' => $row->date,
						'status' => $row->status
					);
				}
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $complaints
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay denuncias creadas.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getNewComplaints
		if ($msg == 'getNewComplaints')
		{
			//Consultamos
			$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 0 ORDER BY c.idcomplaint ASC");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Admins
				$complaints = array();
				$complaints_user = array();
				
				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$complaints[] = array(
						'idcomplaint' => $row->idcomplaint,
						'title' => $row->title,
						'description' => $row->description,
						'lat' => $row->lat,
						'lng' => $row->lng,
						'address' => $row->address,
						'type' => $row->type,
						'media' => $row->media,
						'idcategory' => $row->idcategory,
						'category' => $row->category,
						'idamount' => $row->idamount,
						'amount' => $row->amount,
						'icon' => $row->icon,
						'iduser' => $row->iduser,
						'date' => $row->date,
						'status' => $row->status
					);
				}
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $complaints
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay denuncias nuevas.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getComplaintsUser
		if ($msg == 'getComplaintsUser')
		{
			//Leer Valores
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos
			if ($iduser)
			{
				//Consultamos
				$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status IN (0,1) AND c.iduser = ".$iduser." ORDER BY c.idcomplaint DESC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$complaints = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$complaints[] = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							'media' => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'date' => $row->date,
							'status' => $row->status
						);
					}
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $complaints
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay denuncias creadas.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// changeStatusComplaint
		if ($msg == 'changeStatusComplaint')
		{
			//Leer los Valores
			$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
			$status = (isset($fields['status'])) ? (string)trim($fields['status']) : '';
			
			//Verificamos
			if ($idcomplaint)
			{
				//Consultamos
				$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomplaint ASC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Leemos los datos de la denuncia
					$row = $query->row();
					
					//Actualizamos la denuncia
					$data = array(
						'status' => $status
					);
					$this->db->where('idcomplaint', $idcomplaint);
					$this->db->update('complaint', $data);
					
					//Add Notification - Si la denuncia es aprobada
					if ((string)$status == '1')
					{
						//Registramos la Notificación
						$data = array(
							'iduser' => $row->iduser,
							'title' => 'Tu denuncia ha sido aprobada',
							'message' => 'La denuncia "' . strtoupper($row->title) . '" ha sido aprobada, haz click para ver más detalles',
							'source' => 'Admin',
							'view' => 'ViewComplaint',
							'identifier' => $idcomplaint,
							'read' => 0,
							'createdAt' => date('Y-m-d H:i:s'),
							'status' => 1
						);
						$this->db->insert('notification', $data);
						$idnotification = $this->db->insert_id();
						
						//Consultamos los dispositivos que tenga registrados el usuario
						$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $row->iduser . " AND d.status = 1");
						
						//Verificamos si el usuario tiene registrados dispositivos
						if ($query_devices->num_rows() > 0)
						{
							//Obtenemos las Credenciales
							$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
							
							//Creamos el cliente de SNS
							$SnsClient = new SnsClient([
								'version' => 'latest',
								'region' => 'us-west-2',
								'credentials' => $credentials
					        ]);
					        
							//Procesamos el mensaje a los dispositivos
							foreach ($query_devices->result() as $row_device)
							{
								$result = $SnsClient->publish([
								    'Message' => $title,
								    'TargetArn' => $row_device->endpoint
								]);
							}
						}
					}
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe la denuncia.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getComplaint
		if ($msg == 'getComplaint')
		{
			//Leer los Valores
			$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
			
			//Verificamos
			if ($idcomplaint)
			{
				//Consultamos
				$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomplaint ASC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$row = $query->row();
					$comments = array();
					$groups = array();
					
					//Consultamos los Comentarios
					$query_comments = $this->db->query("SELECT c.idcomment_complaint as idcomment_complaint, c.text as text, c.idamount as idamount, amount.value as amount, c.iduser as iduser, c.createdAt as date FROM comment_complaint c INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 AND c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomment_complaint DESC");
	
					//Consultamos los Grupos
					$query_groups = $this->db->query("SELECT g.idgroup as idgroup, g.title as title, g.description as description, g.level as level, g.send as send FROM group_complaint gc INNER JOIN complaint c ON gc.idcomplaint = c.idcomplaint INNER JOIN `group` g ON gc.idgroup = g.idgroup WHERE gc.idcomplaint = " . $idcomplaint . " ORDER BY gc.idcomplaint ASC");
					
					//Consultamos los Archivos
					$query_files = $this->db->query("SELECT * FROM file WHERE idcomplaint = " . $idcomplaint . " ORDER BY createdAt ASC LIMIT 10");
					
					//Consultamos las Notificaciones
					$query_notifications = $this->db->query("SELECT n.idnotification, n.title, n.message, n.source, n.view, n.identifier, n.read, n.createdAt FROM notification n WHERE n.view = 'ViewComplaint' AND n.identifier = " . $idcomplaint . " ORDER BY n.idnotification DESC LIMIT 10");
					//Declaramos el Arreglo de Notificaciones
					$notifications = array();
					
					//Procesamos las Notificaciones
					if ($query_notifications->num_rows() > 0)
					{
						//Procesamos las Notificaciones
						foreach ($query_notifications->result() as $row_notification)
						{
							//Generamos el Elemento de Notificacion
							$notifications[] = array(
								'idnotification' => $row_notification->idnotification,
								'title' => $row_notification->title,
								'message' => $row_notification->message,
								'source' => $row_notification->source,
								'view' => $row_notification->view,
								'identifier' => $row_notification->identifier,
								'read' => $row_notification->read,
								'createdAt' => $row_notification->createdAt,
								'timeago' => $this->get_timeago($row_notification->createdAt)
							);
						}
					}
					
					//Procesamos los Grupos
					foreach ($query_groups->result() as $group)
					{
						$query_dependencies = $this->db->query("SELECT d.iddependency as iddependency, d.name as name FROM group_dependency gd INNER JOIN dependency d ON gd.iddependency = d.iddependency WHERE gd.idgroup = " . $group->idgroup);
						
						$groups[] = array(
							'idgroup' => $group->idgroup,
							'title' => $group->title,
							'description' => $group->description,
							'level' => $group->level,
							'send' => $group->send,
							'dependencies' => $query_dependencies->result(),
							'updates' => array()
						);
					}
					
					//Generamos el Elemento de Area
					$complaint = array(
						'idcomplaint' => $row->idcomplaint,
						'title' => $row->title,
						'description' => $row->description,
						'lat' => $row->lat,
						'lng' => $row->lng,
						'address' => $row->address,
						'type' => $row->type,
						$row->type => $row->media,
						'idcategory' => $row->idcategory,
						'category' => $row->category,
						'idamount' => $row->idamount,
						'amount' => $row->amount,
						'icon' => $row->icon,
						'iduser' => $row->iduser,
						'date' => $row->date,
						'status' => $row->status,
						'comments' => $row->comments,
						'follows' => $row->follows,
						'comments_data' => $query_comments->result(),
						'groups_data' => $groups,
						'files_data' => $query_files->result(),
						'notifications_data' => $notifications
					);
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $complaint
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe la denuncia.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getComplaint
		if ($msg == 'getComplaintAmounts')
		{
			//Leer los Valores
			$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
			
			//Verificamos
			if ($idcomplaint)
			{
				//Consultamos
				$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 AND c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomplaint ASC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$row = $query->row();
					$comments = array();
					
					//Consultamos los Comentarios
					$query_comments = $this->db->query("SELECT c.idcomment_complaint as idcomment_complaint, c.text as text, c.idamount as idamount, amount.value as amount, c.iduser as iduser, c.createdAt as date FROM comment_complaint c INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 AND c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomment_complaint DESC");
	
					//Consultamos
					$query_amounts = $this->db->query("SELECT * FROM amount WHERE amount.status = 1 ORDER BY amount.order ASC");
					
					//Declaramos el Arreglo de Admins
					$amounts = array();
	
					//Procesamos los Admins
					foreach ($query_amounts->result() as $row_amount)
					{
						//Generamos el Elemento de Area
						$amounts[] = array(
							'idamount' => $row_amount->idamount,
							'value' => $row_amount->value,
							'order' => $row_amount->order
						);
					}
					
					//Generamos el Elemento de Area
					$complaint = array(
						'idcomplaint' => $row->idcomplaint,
						'title' => $row->title,
						'description' => $row->description,
						'lat' => $row->lat,
						'lng' => $row->lng,
						'address' => $row->address,
						'type' => $row->type,
						$row->type => $row->media,
						'idcategory' => $row->idcategory,
						'category' => $row->category,
						'idamount' => $row->idamount,
						'amount' => $row->amount,
						'icon' => $row->icon,
						'iduser' => $row->iduser,
						'date' => $row->date,
						'comments' => $row->comments,
						'follows' => $row->follows,
						'comments_data' => $query_comments->result(),
						'amounts' => $amounts
					);
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $complaint
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe la denuncia.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// followComplaint
		if ($msg == 'followComplaint')
		{
			//Leemos los Datos
			$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos
			if ($idcomplaint && $iduser)
			{
				//Consultamos la Denuncia
				$query = $this->db->query("SELECT * FROM complaint WHERE idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
				
				//Verificamos
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Verificamos si el usuario no es el dueño
					if ($row->iduser != $iduser)
					{
						//Consultamos si el usuario es valido
						$query_user = $this->db->query("SELECT * FROM user WHERE iduser = ".$iduser." AND status = 1 LIMIT 1");
						
						//Verificamos si el usuario existe
						if ($query_user->num_rows() > 0)
						{
							//Leemos el Usuario que se unió
							$row_user = $query_user->row();
							
							//Consultamos la relación de la denuncia con el usuario
							$query_follow = $this->db->query("SELECT * FROM follow_complaint WHERE iduser = ".$iduser." AND idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
							
							//Verificamos que no exista la relacion
							if ($query_follow->num_rows() == 0)
							{
								//Generamos el Registro de Follow
								$data = array(
									'iduser' => $iduser,
									'idcomplaint' => $idcomplaint,
									'createdAt' => date('Y-m-d H:i:s'),
									'status' => 1
								);
								$this->db->insert('follow_complaint', $data);
								$idfollow_complaint = $this->db->insert_id();
								
								//Consultamos la denuncia
								$query_complaint = $this->db->query("SELECT * FROM complaint WHERE idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
								$row_complaint = $query_complaint->row();
								
								//Add Notification - Avisar al OWNER
								$data = array(
									'iduser' => $row->iduser,
									'title' => 'El usuario ' . $row_user->name . ' sigue a tu denuncia',
									'message' => 'El usuario ' . $row_user->name . ' esta siguiendo tu denuncia ' . strtoupper($row_complaint->title) . ', haz click para ver más detalles',
									'source' => 'Incorruptible',
									'view' => 'ViewComplaint',
									'identifier' => $idcomplaint,
									'read' => 0,
									'createdAt' => date('Y-m-d H:i:s'),
									'status' => 1
								);
								$this->db->insert('notification', $data);
								$idnotification = $this->db->insert_id();
								
								//Consultamos los dispositivos que tenga registrados el usuario
								$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $row->iduser . " AND d.status = 1");
								
								//Verificamos si el usuario tiene registrados dispositivos
								if ($query_devices->num_rows() > 0)
								{
									//Obtenemos las Credenciales
									$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
									
									//Creamos el cliente de SNS
									$SnsClient = new SnsClient([
										'version' => 'latest',
										'region' => 'us-west-2',
										'credentials' => $credentials
							        ]);
							        
									//Procesamos el mensaje a los dispositivos
									foreach ($query_devices->result() as $row_device)
									{
										$result = $SnsClient->publish([
										    'Message' => $title,
										    'TargetArn' => $row_device->endpoint
										]);
									}
								}
								
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success'
								);
			
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Mostrar Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'Tu ya sigues a esta denuncia.'
								);
				
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Mostrar Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'No existe el usuario o esta deshabilitado.'
							);
			
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Tu eres el propietario de la denuncia.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe la denuncia o esta deshabilitada.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// loginFacebook
		if ($msg == 'loginFacebook')
		{
			//Read Values
			$token = (string)trim($fields['token']) ? (string)trim($fields['token']) : '';
			
			//Verificamos
			if ($token)
			{
				//Obtenemos la información
				$usuario = $this->facebook->responseToken($token);
				
				//Verificamos
				if (isset($usuario['id']))
				{
					//Consultamos los datos
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $usuario['email'] . "' AND status = 1 LIMIT 1");
	
					//Verificamos si existe el usuario
					if ($query->num_rows() > 0)
					{
						//Leemos el Objeto
						$row = $query->row();
						
						//Consultamos el Perfil del Usuario
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row->idprofile . " AND status = 1");
						$row_profile = $query_profile->row();
						
						//Consultamos las Denuncias del Usuario
						$query_complaints = $this->db->query("SELECT * FROM complaint WHERE iduser = " . $row->iduser . " AND status = 1");
						
						//Consultamos las Denuncias Seguidas del Usuario
						$query_follow_complaints = $this->db->query("SELECT * FROM follow_complaint WHERE iduser = " . $row->iduser . " AND status = 1");
						
						//Actualizamos los datos
						$data = array(
							'fbid' => $usuario['id'],
							'fbtoken' => $usuario['token'],
							'avatar' => $usuario['avatar'],
							'updatedAt' => date('Y-m-d H:i:s')
						);
						$this->db->where('iduser', $row->iduser);
						$this->db->update('user', $data);
						
						//Consultamos
						$data = array(
							'iduser' => $row->iduser,
							'name' => $row->name,
						    'email' => $row->email,
						    'avatar' => $usuario['avatar'],
						    'fbid' => $usuario['id'],
						    'twid' => $row->twid,
						    'complaints' => $query_complaints->num_rows(),
						    'follows' => $query_follow_complaints->num_rows(),
						    'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name
						);
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Guardamos los Datos
						$data = array(
							'email' => $usuario['email'],
							'password' => password_hash($usuario['id'], PASSWORD_DEFAULT, ['cost' => 12]),
						    'name' => $usuario['first_name'].' '.$usuario['last_name'],
						    'fbid' => $usuario['id'],
							'fbtoken' => $usuario['token'],
							'avatar' => $usuario['avatar'],
						    'createdAt' => date('Y-m-d H:i:s'),
						    'idprofile' => 1,
						    'status' => 1
						);
						$this->db->insert('user', $data);
						$iduser = $this->db->insert_id();
						
						//Consultamos el Perfil del Usuario
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = 1 AND status = 1");
						$row_profile = $query_profile->row();
						
						$data = array(
							'iduser' => $iduser,
							'email' => $usuario['email'],
							'name' => $usuario['first_name'].' '.$usuario['last_name'],
							'link' => 'https://app.incorruptible.mx'
						);
						
						$data_result = array(
							'iduser' => $iduser,
							'name' => $usuario['first_name'].' '.$usuario['last_name'],
							'email' => $usuario['email'],
							'avatar' => $usuario['avatar'],
							'fbid' => $usuario['id'],
							'twid' => '',
							'complaints' => '0',
						    'follows' => '0',
						    'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name,
						);
	
						//Correo de Notificación
						$subject = 'Bienvenido a Incorruptible';
						$body = $this->load->view('mail/welcome_user', $data, TRUE);
						$altbody = strip_tags($body);
	
						//Verificamos
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Mandamos el Correo
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
					        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
					        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email, $name);
					        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
	
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data_result
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Ocurrió un error al procesar la información.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// joinComplaint
		if ($msg == 'joinComplaint')
		{
			//Leemos los datos
			$text = (isset($fields['text'])) ? (string)trim($fields['text']) : '';
			$idamount = (isset($fields['idamount'])) ? (string)trim($fields['idamount']) : '';
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
			
			//Verificamos los Datos
			if ($text && $idamount && $iduser && $idcomplaint)
			{
				//Verificamos si existe la denuncia
				$query = $this->db->query("SELECT * FROM complaint WHERE idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
				
				//Verificamos
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Verificamos si el usuario no es el dueño
					if ($row->iduser != $iduser)
					{
						//Consultamos si el usuario es valido
						$query_user = $this->db->query("SELECT * FROM user WHERE iduser = ".$iduser." AND status = 1 LIMIT 1");
						
						//Verificamos si el usuario existe
						if ($query_user->num_rows() > 0)
						{
							//Leemos el Usuario
							$row_user = $query_user->row();
							
							//Consultamos la relación de la denuncia con el usuario
							$query_comment = $this->db->query("SELECT * FROM comment_complaint WHERE iduser = ".$iduser." AND idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
							
							//Verificamos que no exista la relacion
							if ($query_comment->num_rows() == 0)
							{
								//Generamos el Registro de Follow
								$data = array(
									'text' => $text,
									'idamount' => $idamount,
									'iduser' => $iduser,
									'idcomplaint' => $idcomplaint,
									'createdAt' => date('Y-m-d H:i:s'),
									'status' => 1
								);
								$this->db->insert('comment_complaint', $data);
								$idcomment_complaint = $this->db->insert_id();
								
								//Consultamos la denuncia
								$query_complaint = $this->db->query("SELECT * FROM complaint WHERE idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
								$row_complaint = $query_complaint->row();
								
								//Add Notification - Avisar al OWNER
								$data = array(
									'iduser' => $row_complaint->iduser,
									'title' => 'El usuario ' . $row_user->name . ' se ha unido a tu denuncia',
									'message' => 'El usuario ' . $row_user->name . ' se ha unido a la denuncia ' . strtoupper($row_complaint->title) . ', haz click para ver más detalles',
									'source' => 'Incorruptible',
									'view' => 'ViewComplaint',
									'identifier' => $idcomplaint,
									'read' => 0,
									'createdAt' => date('Y-m-d H:i:s'),
									'status' => 1
								);
								$this->db->insert('notification', $data);
								$idnotification = $this->db->insert_id();
								
								//Consultamos los dispositivos que tenga registrados el usuario
								$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $row_complaint->iduser . " AND d.status = 1");
								
								//Verificamos si el usuario tiene registrados dispositivos
								if ($query_devices->num_rows() > 0)
								{
									//Obtenemos las Credenciales
									$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
									
									//Creamos el cliente de SNS
									$SnsClient = new SnsClient([
										'version' => 'latest',
										'region' => 'us-west-2',
										'credentials' => $credentials
							        ]);
							        
									//Procesamos el mensaje a los dispositivos
									foreach ($query_devices->result() as $row_device)
									{
										$result = $SnsClient->publish([
										    'Message' => $title,
										    'TargetArn' => $row_device->endpoint
										]);
									}
								}
								
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success'
								);
			
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Mostrar Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'Tu ya te uniste a esta denuncia.'
								);
				
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Mostrar Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'No existe el usuario o esta deshabilitado.'
							);
			
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Tu eres el propietario de la denuncia.'
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe la denuncia o esta deshabilitada.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// loginTwitter
		if ($msg == 'loginTwitter')
		{
			//Read Values
			$email = (string)trim($fields['email']) ? (string)trim($fields['email']) : '';			    
			$name = (string)trim($fields['name']) ? (string)trim($fields['name']) : '';
			$twid = (string)trim($fields['twid']) ? (string)trim($fields['twid']) : '';
			$twtoken = (string)trim($fields['twtoken']) ? (string)trim($fields['twtoken']) : '';
			$avatar = (string)trim($fields['avatar']) ? (string)trim($fields['avatar']) : '';
			
			//Verificamos
			if ($email && $name && $twid && $twtoken && $avatar)
			{
				//Consultamos los datos
				$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' AND status = 1 LIMIT 1");

				//Verificamos si existe el usuario
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Consultamos el Perfil del Usuario
					$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row->idprofile . " AND status = 1");
					$row_profile = $query_profile->row();
					
					//Consultamos las Denuncias del Usuario
					$query_complaints = $this->db->query("SELECT * FROM complaint WHERE iduser = " . $row->iduser . " AND status = 1");
					
					//Consultamos las Denuncias Seguidas del Usuario
					$query_follow_complaints = $this->db->query("SELECT * FROM follow_complaint WHERE iduser = " . $row->iduser . " AND status = 1");
					
					//Actualizamos los datos
					$data = array(
						'name' => $name,
						'twid' => $twid,
						'twtoken' => $twtoken,
						'avatar' => $avatar,
						'updatedAt' => date('Y-m-d H:i:s')
					);
					$this->db->where('iduser', $row->iduser);
					$this->db->update('user', $data);
					
					//Consultamos
					$data = array(
						'iduser' => $row->iduser,
						'name' => $name,
					    'email' => $row->email,
					    'avatar' => $avatar,
					    'fbid' => $row->fbid,
					    'twid' => $twid,
					    'complaints' => $query_complaints->num_rows(),
					    'follows' => $query_follow_complaints->num_rows(),
					    'idprofile' => $row_profile->idprofile,
					    'profile' => $row_profile->name
					);
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Guardamos los Datos
					$data = array(
						'email' => $email,
						'password' => password_hash($twid, PASSWORD_DEFAULT, ['cost' => 12]),
					    'name' => $name,
					    'twid' => $twid,
						'twtoken' => $twtoken,
						'avatar' => $avatar,
					    'createdAt' => date('Y-m-d H:i:s'),
					    'idprofile' => 1,
					    'status' => 1
					);
					$this->db->insert('user', $data);
					$iduser = $this->db->insert_id();
					
					//Consultamos el Perfil del Usuario
					$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = 1 AND status = 1");
					$row_profile = $query_profile->row();
					
					$data = array(
						'iduser' => $iduser,
						'email' => $email,
						'name' => $name,
						'link' => 'https://app.incorruptible.mx'
					);
					
					$data_result = array(
						'iduser' => $iduser,
						'name' => $name,
						'email' => $email,
						'avatar' => $avatar,
						'fbid' => '',
						'twid' => $twid,
						'complaints' => '0',
					    'follows' => '0',
					    'idprofile' => $row_profile->idprofile,
					    'profile' => $row_profile->name,
					);

					//Correo de Notificación
					$subject = 'Bienvenido a Incorruptible';
					$body = $this->load->view('mail/welcome_user', $data, TRUE);
					$altbody = strip_tags($body);

					//Verificamos
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Mandamos el Correo
						$mail = new PHPMailer(true);
						$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
						$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
						$mail->CharSet = 'UTF-8';
						$mail->Encoding = "quoted-printable";
				        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
				        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
				        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
				        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
				        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
				        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
				        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
				        $mail->SMTPDebug  = 0;
				        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
				        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
				        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
				        $mail->Body      = $body;
				        $mail->AltBody    = $altbody;
				        $mail->AddAddress($email, $name);
				        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');

				        if ($mail->Send())
						{
							$response = true;
						}
						else
						{
							$response = false;
						}
					}

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data_result
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getBlog
		if ($msg == 'getBlog')
		{
			//Leemos el Blog
			//$feed = new SimpleXMLElement(file_get_contents('http://incorruptible.mx/feed'));
			$feed = new SimpleXMLElement(file_get_contents('http://incorruptible.mx/feedmedia'));
			$items = array();
			foreach ($feed->channel->item as $item)
			{
				$items[] = $item;
			}
			
			//Generamos el Arreglo
			$array = array(
				'status' => (int)1,
				'msg' => 'success',
				'data' => $items
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
		
		// getNews
		if ($msg == 'getNews')
		{
			//Leemos el Blog
			//$feed = new SimpleXMLElement(file_get_contents('http://incorruptible.mx/feed'));
			$feed = new SimpleXMLElement(file_get_contents('http://incorruptible.mx/feednoticias'));
			$items = array();
			foreach ($feed->channel->item as $item)
			{
				$items[] = $item;
			}
			
			//Generamos el Arreglo
			$array = array(
				'status' => (int)1,
				'msg' => 'success',
				'data' => $items
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
		
		// getDependencies
		if ($msg == 'getDependencies')
		{
			//Consultamos
			$query = $this->db->query("SELECT * FROM dependency WHERE dependency.status = 1 ORDER BY dependency.iddependency ASC");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Dependencias
				$dependencies = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$dependencies[] = array(
						'iddependency' => $row->iddependency,
						'name' => $row->name
					);
				}

				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $dependencies
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay dependencias asignadas.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getDependenciesByTerm
		if ($msg == 'getDependenciesByTerm')
		{
			//Leer Term
			$term = (isset($fields['term'])) ? (string)trim($fields['term']) : '';
			
			//Consultamos
			$query = $this->db->query("SELECT * FROM dependency WHERE dependency.status = 1 AND dependency.name LIKE '%".$term."%' ORDER BY dependency.iddependency ASC");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Dependencias
				$dependencies = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$dependencies[] = array(
						'iddependency' => $row->iddependency,
						'name' => $row->name
					);
				}

				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $dependencies
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay dependencias asignadas.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		//saveGroupComplaints
		if ($msg == 'saveGroupComplaints')
		{
			//Leemos los Valores
			$title = (isset($fields['title'])) ? (string)trim($fields['title']) : '';
			$description = (isset($fields['description'])) ? (string)trim($fields['description']) : '';
			$level = (isset($fields['level'])) ? (string)trim($fields['level']) : '';
			$dependency = (isset($fields['dependency'])) ? (string)trim($fields['dependency']) : '';
			$complaints = (isset($fields['complaints'])) ? (string)trim($fields['complaints']) : '';
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos
			if ($title && $description && $level && $dependency && $complaints && $iduser)
			{
				//Leemos las Dependencias
				$array_dependencies = explode(',', $dependency);
				
				//Creamos el Grupo
				$data = array(
					'title' => $title,
					'description' => $description,
					'level' => $level,
					'iduser' => $iduser,
					'send' => 0,
					'createdAt' => date('Y-m-d H:i:s'),
					'status' => 1
				);
				$this->db->insert('group', $data);
				$idgroup = $this->db->insert_id();
				
				//Procesamos las Denuncias
				$array_complaints = explode(',', $complaints);
				
				//Creamos la Relación de las Denuncias con el Grupo
				foreach ($array_complaints as $complaint)
				{
					//Generamos la Relación
					$data = array(
						'idgroup' => $idgroup,
						'idcomplaint' => $complaint,
						'createdAt' => date('Y-m-d H:i:s'),
						'status' => 1
					);
					$this->db->insert('group_complaint', $data);
					
					//Consultamos la denuncia
					$query_complaint = $this->db->query("SELECT * FROM complaint WHERE idcomplaint = ".$complaint." AND status = 1 LIMIT 1");
					$row_complaint = $query_complaint->row();
					
					//Add Notification - Avisar al OWNER
					$data = array(
						'iduser' => $row_complaint->iduser,
						'title' => 'Han agrupado tu denuncia',
						'message' => 'Han agrupado tu denuncia "' . strtoupper($row_complaint->title) . '" en el grupo "' . strtoupper($title) . '", haz click para ver más detalles',
						'source' => 'ONG',
						'view' => 'ViewComplaint',
						'identifier' => $complaint,
						'read' => 0,
						'createdAt' => date('Y-m-d H:i:s'),
						'status' => 1
					);
					$this->db->insert('notification', $data);
					$idnotification = $this->db->insert_id();
					
					//Consultamos los dispositivos que tenga registrados el usuario
					$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $row_complaint->iduser . " AND d.status = 1");
					
					//Verificamos si el usuario tiene registrados dispositivos
					if ($query_devices->num_rows() > 0)
					{
						//Obtenemos las Credenciales
						$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
						
						//Creamos el cliente de SNS
						$SnsClient = new SnsClient([
							'version' => 'latest',
							'region' => 'us-west-2',
							'credentials' => $credentials
				        ]);
				        
						//Procesamos el mensaje a los dispositivos
						foreach ($query_devices->result() as $row_device)
						{
							$result = $SnsClient->publish([
							    'Message' => $title,
							    'TargetArn' => $row_device->endpoint
							]);
						}
					}
					
					//Actualizamos el Icono de la Denuncia
					$data = array(
						'icon' => 2
					);
					$this->db->where('idcomplaint', $complaint);
					$this->db->update('complaint', $data);
				}
				
				//Creamos la Relación del Grupo con las Dependencias
				foreach ($array_dependencies as $row)
				{
					//Generamos la Relación
					$data = array(
						'idgroup' => $idgroup,
						'iddependency' => $row,
						'createdAt' => date('Y-m-d H:i:s'),
						'status' => 1
					);
					$this->db->insert('group_dependency', $data);
				}
				
				//Mostrar Resultados
				$array = array(
					'status' => (int)1,
					'msg' => (string)'success',
					'data' => $idgroup
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		//getGroupsByUser
		if ($msg == 'getGroupsByUser')
		{
			//Leemos las Variables
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos el Valor
			if ($iduser)
			{
				//Consultamos todos los Grupos creados por el Usuario
				$query = $this->db->query("SELECT * FROM `group` g WHERE g.status = 1 AND g.iduser = " . $iduser . " ORDER BY g.idgroup ASC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Dependencias
					$groups = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Grupos
						$groups[] = array(
							'idgroup' => $row->idgroup,
							'title' => $row->title,
							'description' => $row->description,
							'level' => $row->level
						);
					}
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $groups
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay grupos creados.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		//getGroup
		if ($msg == 'getGroup')
		{
			//Leemos las Variables
			$idgroup = (isset($fields['idgroup'])) ? (string)trim($fields['idgroup']) : '';
			
			//Verificamos el Valor
			if ($idgroup)
			{
				//Consultamos todos los Grupos creados por el Usuario
				$query = $this->db->query("SELECT * FROM `group` g WHERE g.status = 1 AND g.idgroup = " . $idgroup);
				
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Procesamos los Admins
					foreach ($query->result() as $row) { }
					
					//Consultamos las Denuncias que pertenecen al grupo
					$query_complaints = $this->db->query("SELECT g.idgroup_complaint, g.idgroup, c.idcomplaint, c.title, c.description, c.lat, c.lng, c.address, c.type, c.media, c.idcategory, c.idamount, c.icon, c.iduser, (SELECT a.sum FROM amount a WHERE a.idamount = c.idamount AND a.status = 1) as sum FROM group_complaint g INNER JOIN complaint c ON g.idcomplaint = c.idcomplaint WHERE g.status = 1 AND g.idgroup = " . $idgroup);
					$query_dependencies = $this->db->query("SELECT d.iddependency as iddependency, d.name as name FROM group_dependency gd INNER JOIN dependency d ON gd.iddependency = d.iddependency WHERE gd.idgroup = " . $idgroup);
					$complaints = array();
					$sum = 0;
					$contador = 0;
					$center = '';
					$markers = '';
					$lats = 0;
					$longs = 0;
					
					//Procesamos
					foreach ($query_complaints->result() as $complaint)
					{
						$sum = $sum + floatval($complaint->sum);
						$contador++;
						$markers.= '&markers=icon:http://app.incorruptible.mx/img/yellow.png%7C'.$complaint->lat.','.$complaint->lng;
						
						//if ($contador == 1) { $center = (string)$complaint->lat . ',' . (string)$complaint->lng; }
						$lats = $lats + $complaint->lat;
						$longs = $longs + $complaint->lng;
						
						$complaints[] = array(
							'idgroup_complaint' => $complaint->idgroup_complaint,
							'idgroup' => $complaint->idgroup,
							'idcomplaint' => $complaint->idcomplaint,
							'title' => $complaint->title,
							'description' => $complaint->description,
							'lat' => $complaint->lat,
							'lng' => $complaint->lng,
							'address' => $complaint->address,
							'type' => $complaint->type,
							'media' => $complaint->media,
							'idcategory' => $complaint->idcategory,
							'idamount' => $complaint->idamount,
							'icon' => $complaint->icon,
							'iduser' => $complaint->iduser,
							'amount' => $complaint->sum	
						);
					}
					
					$center = (string)($lats/$contador) . ',' . (string)($longs/$contador);
					
					//Creamos el Map
					$map = 'https://maps.googleapis.com/maps/api/staticmap';
					$map.= '?center=' . $center;
					$map.= '&zoom=12';
					$map.= '&scale=2';
					$map.= '&size=960x480';
					$map.= '&maptype=roadmap';
					$map.= $markers;
					$map.= '&key=AIzaSyCdONyfCadeDvw20F1Q03eJj80WnGmCZ4g';
					$map.= '&format=jpg';
					$map.= '&visual_refresh=true';
					
					//Generamos el Elemento de Grupos
					$group = array(
						'idgroup' => $row->idgroup,
						'title' => $row->title,
						'description' => $row->description,
						'level' => $row->level,
						'iduser' => $row->iduser,
						'send' => $row->send,
						'complaints' => $complaints,
						'dependencies' => $query_dependencies->result(),
						'map' => $map,
						'sum' => $sum
					);
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $group
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe el grupo.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		//sendGroup
		if ($msg == 'sendGroup')
		{
			//Leemos las Variables
			$idgroup = (isset($fields['idgroup'])) ? (string)trim($fields['idgroup']) : '';
			
			//Verificamos el Valor
			if ($idgroup)
			{
				//Consultamos todos los Grupos creados por el Usuario
				$query = $this->db->query("SELECT g.idgroup, g.title, g.description, g.level, g.iddependency, (SELECT name FROM dependency d WHERE d.iddependency = g.iddependency) as dependency, g.iduser, (SELECT email FROM user u WHERE u.iduser = g.iduser) as email, (SELECT name FROM user u WHERE u.iduser = g.iduser) as name, g.send FROM `group` g WHERE g.status = 1 AND g.idgroup = " . $idgroup);
				
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Leemos el objeto
					foreach ($query->result() as $row) { }
					
					//Actualizamos el Grupo
					$data = array(
						'send' => 1
					);
					$this->db->where('idgroup', $row->idgroup);
					$this->db->update('group', $data);
					
					//Consultamos las Denuncias que pertenecen al grupo
					$query_complaints = $this->db->query("SELECT g.idgroup_complaint, g.idgroup, c.idcomplaint, c.title, c.description, c.lat, c.lng, c.address, c.type, c.media, c.idcategory, c.idamount, c.icon, c.iduser, (SELECT a.sum FROM amount a WHERE a.idamount = c.idamount AND a.status = 1) as sum FROM group_complaint g INNER JOIN complaint c ON g.idcomplaint = c.idcomplaint WHERE g.status = 1 AND g.idgroup = " . $idgroup);
					$complaints = array();
					
					//Procesamos
					foreach ($query_complaints->result() as $complaint)
					{
						$complaints[] = array(
							'idgroup_complaint' => $complaint->idgroup_complaint,
							'idgroup' => $complaint->idgroup,
							'idcomplaint' => $complaint->idcomplaint,
							'title' => $complaint->title,
							'description' => $complaint->description,
							'lat' => $complaint->lat,
							'lng' => $complaint->lng,
							'address' => $complaint->address,
							'type' => $complaint->type,
							'media' => $complaint->media,
							'idcategory' => $complaint->idcategory,
							'idamount' => $complaint->idamount,
							'icon' => $complaint->icon,
							'iduser' => $complaint->iduser,
							'amount' => $complaint->sum	
						);
					}
					
					//Enviamos la Notificación
					$data = array(
						'idgroup' => $row->idgroup,
						'title' => $row->title,
						'description' => $row->description,
						'level' => $row->level,
						'iddependency' => $row->iddependency,
						'dependency' => $row->dependency,
						'iduser' => $row->iduser,
						'email' => $row->email,
						'name' => $row->name,
						'send' => $row->send,
						'complaints' => $complaints
					);

					//Correo de Notificación
					$subject = 'Grupo "'.$row->title.'" enviado desde Incorruptible';
					$body = $this->load->view('mail/send_group', $data, TRUE);
					$altbody = strip_tags($body);
					$email = 'milio.hernandez@gmail.com';
					$name = 'Emiliano Hernández';

					//Verificamos
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Mandamos el Correo
						$mail = new PHPMailer(true);
						$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
						$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
						$mail->CharSet = 'UTF-8';
						$mail->Encoding = "quoted-printable";
				        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
				        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
				        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
				        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
				        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
				        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
				        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
				        $mail->SMTPDebug  = 0;
				        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
				        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
				        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
				        $mail->Body      = $body;
				        $mail->AltBody    = $altbody;
				        $mail->AddAddress($email, $name);
				        $mail->AddBCC('gerardo@nodo.pw', 'Gerardo Vidal');

				        if ($mail->Send())
						{
							$response = true;
						}
						else
						{
							$response = false;
						}
					}

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}	
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe el grupo.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// updateProfile
		if ($msg == 'updateProfile')
		{
			//Leemos los Datos
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			$avatar = (isset($fields['avatar'])) ? (string)trim($fields['avatar']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los Datos
			if ($iduser)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM user WHERE iduser = " . $iduser . " AND status = 1 LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Verificamos actualización
					if ($avatar || $password)
					{
						$data = array();
						if ($avatar) { $data['avatar'] = $avatar; }
						if ($password) { $data['password'] = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]); }
						$this->db->where('iduser', $iduser);
						$this->db->update('user', $data);
					}

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success'
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe este usuario.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// searchUser
		if ($msg == 'searchUser')
		{
			//Leemos los Datos
			$string = (isset($fields['string'])) ? (string)trim($fields['string']) : '';
			
			//Verificamos los Datos
			if ($string)
			{
				//Consultamos el Termino
				$query = $this->db->query('SELECT * FROM user WHERE email LIKE "%' . $string . '%" OR name LIKE "%' . $string . '%"');
				
				//Procesamos el HTML
				$html = '';
				$html.= '<div class="row">';
				$html.= '	<div class="col s12">';
				
				//Verificamos los Resultados
				if ($query->num_rows() > 0)
				{
					$html.= '		<table class="striped responsive-table">';
					$html.= '			<thead>';
					$html.= '				<tr>';
					$html.= '					<th data-field="number">ID</th>';
					$html.= '					<th data-field="name">Nombre</th>';
					$html.= '					<th data-field="email">Correo Electrónico</th>';
					$html.= '					<th data-field="profile">Perfil</th>';
					$html.= '					<th data-field="actions">Acciones</th>';
          			$html.= '				</tr>';
        			$html.= '			</thead>';

					$html.= '			<tbody>';
					
					foreach ($query->result() as $user)
					{
						$html.= '				<tr>';
						$html.= '					<td>'.$user->iduser.'</td>';
						$html.= '					<td>'.$user->name.'</td>';
						$html.= '					<td>'.$user->email.'</td>';
						
						$html.= '					<td>';
						switch ((string)$user->idprofile)
						{
							case '1': $html.= 'Usuario Normal'; break;
							case '2': $html.= 'Usuario ONG'; break;
							case '3': $html.= 'Usuario Autoridad'; break;
							default: $html.= 'Usuario Normal'; break;
						}
						$html.= '					</td>';
						
						$html.= '					<td>';
						$html.= '						<a class="waves-effect waves-light btn damask" href="/users/edit/'.$user->iduser.'">Editar</a>'; 
						if ($user->status)
						{
							$html.= '						<a rel="'.$user->iduser.'" class="btnDeleteUser waves-effect waves-light btn gray" href="#">Borrar</a>';
						}
						else
						{
							$html.= '						<a rel="'.$user->iduser.'" class="btnActivateUser waves-effect waves-light btn gray" href="#">Activar</a>';
						}
						$html.= '					</td>';
          				$html.= '				</tr>';
          			}
        			
        			$html.= '			</tbody>';
      				$html.= '		</table>';
				}
				else
				{
					$html.= '<p>No hay resultados, intenta con otro nombre o correo electrónico o recarga la página para reiniciar la consulta de usuarios.</p>';
				}
				
				$html.= '	</div>';
				$html.= '</div>';
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $html
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Necesitas escribir un nombre o email del usuario.',
					'data' => ''
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getDraftComplaints
		if ($msg == 'getDraftComplaints')
		{
			//Consultamos
			$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 0 ORDER BY c.idcomplaint DESC");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Admins
				$complaints = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$complaints[] = array(
						'idcomplaint' => $row->idcomplaint,
						'title' => $row->title,
						'description' => $row->description,
						'lat' => $row->lat,
						'lng' => $row->lng,
						'address' => $row->address,
						'type' => $row->type,
						'media' => $row->media,
						'idcategory' => $row->idcategory,
						'category' => $row->category,
						'idamount' => $row->idamount,
						'amount' => $row->amount,
						'icon' => $row->icon,
						'iduser' => $row->iduser,
						'date' => $row->date,
						'status' => $row->status
					);
				}
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $complaints
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay denuncias nuevas creadas.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// registerDevice
		if ($msg == 'registerDevice')
		{
			//Leemos los datos
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '1';
			$registrationId = (isset($fields['registrationId'])) ? (string)trim($fields['registrationId']) : '62fe891258f7c17f3f669c38eb5e2465139d98b4b4f90ab7b05b4f268804dd6e';
			$platform = (isset($fields['platform'])) ? (string)trim($fields['platform']) : 'ios';
			$SNS_APP_ARN = '';
			
			//Verificamos los datos
			if ($iduser && $registrationId && $platform)
			{
				//Obtenemos las Credenciales
				$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
				
				//Creamos el cliente de SNS
				$SnsClient = new SnsClient([
					'version' => 'latest',
					'region' => 'us-west-2',
					'credentials' => $credentials
		        ]);
		        
		        //Verificamos la plataforma
		        if ($platform == 'ios') { $SNS_APP_ARN = 'arn:aws:sns:us-west-2:903036544634:app/APNS_SANDBOX/incorruptible_dei'; } //APNS_SANDBOX
		        if ($platform == 'android') { $SNS_APP_ARN = 'arn:aws:sns:us-west-2:903036544634:app/GCM/incorruptible_android'; } //GCM
				
				//Consultamos si ya esta registrado un dispositivo con el usuario
				$query = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $iduser . " AND d.platform = '" . $platform . "' AND d.status = 1");
				
				//Verificamos si existe una relación
				if ($query->num_rows() == 0)
				{
					//Generamos el Endpoint del Dispositivo
					$SNSEndPointData = $SnsClient->createPlatformEndpoint([
				       'PlatformApplicationArn' => $SNS_APP_ARN,
				       'Token' => $registrationId
				    ]);
				    
				    //Result Data
				    $endpoint = $SNSEndPointData['EndpointArn'];
				    
					//Registramos el Dispositivo
					$data = array(
						'iduser' => $iduser,
						'registrationId' => $registrationId,
						'endpoint' => $endpoint,
						'platform' => $platform,
						'createdAt' => date('Y-m-d H:i:s'),
						'status' => 1
					);
					$this->db->insert('device', $data);
					$iddevice = $this->db->insert_id();
					
					/*
					//Mandamos una notificación del registro de las notificaciones
					$result = $SnsClient->publish([
					    'Message' => 'Este dispositivo está listo para recibir notificaciones', // REQUIRED
					    'Subject' => 'Nuevo Registro',
					    'TargetArn' => $endpoint
					]);
					*/
					
					//Mostrar Resultados
					$array = array(
						'status' => (int)1,
						'msg' => (string)'success',
						'data' => $endpoint
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Leemos el Registro
					$row = $query->row();
					$endpoint = $row->endpoint;
					
					//Verificamos si el dispositivo es diferente al registrado
					if ((string)trim($row->registrationId) != $registrationId)
					{
						//Eliminamos el Endpoint anterior del Dispositivo
						$SNSDeleteEndPointData = $SnsClient->deleteEndpoint([
					       'EndpointArn' => $row->endpoint
					    ]);
					    
					    //Generamos el Endpoint del Dispositivo
						$SNSEndPointData = $SnsClient->createPlatformEndpoint([
					       'PlatformApplicationArn' => $SNS_APP_ARN,
					       'Token' => $registrationId
					    ]);
					    
					    //Result Data
					    $endpoint = $SNSEndPointData['EndpointArn'];
					    
					    //Actualizamos el Dispositivo
						$data = array(
							'registrationId' => $registrationId,
							'endpoint' => $endpoint,
							'updatedAt' => date('Y-m-d H:i:s')
						);
						$this->db->where('iddevice', $row->iddevice);
						$this->db->update('device', $data);
						
						/*
						//Mandamos una notificación del registro de las notificaciones
						$result = $SnsClient->publish([
						    'Message' => 'Este dispositivo está listo para recibir notificaciones', // REQUIRED
						    'Subject' => 'Nuevo Registro',
						    'TargetArn' => $endpoint
						]);
						*/
					}
					
					//Mostrar Resultados
					$array = array(
						'status' => (int)1,
						'msg' => (string)'success',
						'data' => $endpoint
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos para registrar el dispositivo.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// addNotification
		if ($msg == 'addNotification')
		{
			//Leemos los datos
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			$title = (isset($fields['title'])) ? (string)trim($fields['title']) : '';
			$message = (isset($fields['message'])) ? (string)trim($fields['message']) : '';
			$source = (isset($fields['source'])) ? (string)trim($fields['source']) : '';
			$view = (isset($fields['view'])) ? (string)trim($fields['view']) : '';
			$identifier = (isset($fields['identifier'])) ? (string)trim($fields['identifier']) : '';
			$SNS_APP_ARN = '';
			
			//Verificamos los campos
			if ($iduser && $title && $message && $source)
			{
				//Consultamos al Usuario
				$query = $this->db->query("SELECT * FROM user WHERE iduser = " . $iduser . " AND status = 1 LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Leemos los datos del usuario
					$row = $query->row();
					
					//Registramos la Notificación
					$data = array(
						'iduser' => $iduser,
						'title' => $title,
						'message' => $message,
						'source' => $source,
						'view' => $view,
						'identifier' => $identifier,
						'read' => 0,
						'createdAt' => date('Y-m-d H:i:s'),
						'status' => 1
					);
					$this->db->insert('notification', $data);
					$idnotification = $this->db->insert_id();
					
					//Consultamos los dispositivos que tenga registrados el usuario
					$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $iduser . " AND d.status = 1");
					
					//Verificamos si el usuario tiene registrados dispositivos
					if ($query_devices->num_rows() > 0)
					{
						//Obtenemos las Credenciales
						$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
						
						//Creamos el cliente de SNS
						$SnsClient = new SnsClient([
							'version' => 'latest',
							'region' => 'us-west-2',
							'credentials' => $credentials
				        ]);
				        
						//Procesamos el mensaje a los dispositivos
						foreach ($query_devices->result() as $row_device)
						{
							$result = $SnsClient->publish([
							    'Message' => $title,
							    'TargetArn' => $row_device->endpoint
							]);
						}
					}
					
					//Mostrar Resultados
					$array = array(
						'status' => (int)1,
						'msg' => (string)'success',
						'data' => $idnotification
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe este usuario.'
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// readNotification
		if ($msg == 'readNotification')
		{
			//Leer Valores
			$idnotification = (isset($fields['idnotification'])) ? (string)trim($fields['idnotification']) : '';
			
			//Verificamos
			if ($idnotification)
			{
				//Consultamos
				$query = $this->db->query("SELECT * FROM notification n WHERE n.idnotification = " . $idnotification . " AND n.status");
				
				//Verificamos si existen Notificaciones
				if ($query->num_rows() > 0)
				{
					//Actualizamos la notificación
					$data = array(
						'read' => 1
					);
					$this->db->where('idnotification', $idnotification);
					$this->db->update('notification', $data);
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe la notificación.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getNotificationsUser
		if ($msg == 'getNotificationsUser')
		{
			//Leer Valores
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos
			if ($iduser)
			{
				//Consultamos
				$query = $this->db->query("SELECT n.idnotification, n.title, n.message, n.source, n.view, n.identifier, n.read, n.createdAt FROM notification n WHERE n.iduser = " . $iduser . " AND n.status ORDER BY n.idnotification DESC LIMIT 10");
	
				//Verificamos si existen Notificaciones
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Notificaciones
					$notifications = array();
	
					//Procesamos las Notificaciones
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Notificacion
						$notifications[] = array(
							'idnotification' => $row->idnotification,
							'title' => $row->title,
							'message' => $row->message,
							'source' => $row->source,
							'view' => $row->view,
							'identifier' => $row->identifier,
							'read' => $row->read,
							'createdAt' => $row->createdAt,
							'timeago' => $this->get_timeago($row->createdAt)
						);
					}
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $notifications
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay notificaciones para tu usuario.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// addNotificationComplaint
		if ($msg == 'addNotificationComplaint')
		{
			//Leemos los datos
			$title = (isset($fields['title'])) ? (string)trim($fields['title']) : '';
			$message = (isset($fields['description'])) ? (string)trim($fields['description']) : '';
			$source = (isset($fields['source'])) ? (string)trim($fields['source']) : 'ONG';
			$view = (isset($fields['view'])) ? (string)trim($fields['view']) : 'ViewComplaint';
			$identifier = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
			$idcomplaint = $identifier;
			$SNS_APP_ARN = '';
			$users = array();
			
			//Verificamos los campos
			if ($title && $message && $identifier)
			{
				//Consultamos la Denuncia
				$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.idcomplaint = " . $identifier . " ORDER BY c.idcomplaint ASC");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Leemos los datos del usuario
					$row = $query->row();
					$users[] = $row->iduser;
					
					//Consultamos los usuarios que siguen la denuncia
					$query_follows = $this->db->query("SELECT fc.* FROM follow_complaint fc WHERE fc.idcomplaint = " . $row->idcomplaint . " AND fc.status = 1");
					
					//Procesamos los usuarios que siguen la denuncia
					foreach ($query_follows->result() as $row_follow)
					{
						$users[] = $row_follow->iduser;
					}
					
					//Procesamos las notificaciones para todos los usuarios unidos a la denuncia
					foreach ($users as $iduser)
					{
						//Registramos la Notificación
						$data = array(
							'iduser' => $iduser,
							'title' => $title,
							'message' => $message,
							'source' => $source,
							'view' => $view,
							'identifier' => $identifier,
							'read' => 0,
							'createdAt' => date('Y-m-d H:i:s'),
							'status' => 1
						);
						$this->db->insert('notification', $data);
						$idnotification = $this->db->insert_id();
						
						//Consultamos los dispositivos que tenga registrados el usuario
						$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $iduser . " AND d.status = 1");
						
						//Verificamos si el usuario tiene registrados dispositivos
						if ($query_devices->num_rows() > 0)
						{
							//Obtenemos las Credenciales
							$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
							
							//Creamos el cliente de SNS
							$SnsClient = new SnsClient([
								'version' => 'latest',
								'region' => 'us-west-2',
								'credentials' => $credentials
					        ]);
					        
							//Procesamos el mensaje a los dispositivos
							foreach ($query_devices->result() as $row_device)
							{
								$result = $SnsClient->publish([
								    'Message' => $title,
								    'TargetArn' => $row_device->endpoint
								]);
							}
						}
					}
					
					$comments = array();
					$groups = array();
					
					//Consultamos los Comentarios
					$query_comments = $this->db->query("SELECT c.idcomment_complaint as idcomment_complaint, c.text as text, c.idamount as idamount, amount.value as amount, c.iduser as iduser, c.createdAt as date FROM comment_complaint c INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 AND c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomment_complaint DESC");
	
					//Consultamos los Grupos
					$query_groups = $this->db->query("SELECT g.idgroup as idgroup, g.title as title, g.description as description, g.level as level, g.send as send FROM group_complaint gc INNER JOIN complaint c ON gc.idcomplaint = c.idcomplaint INNER JOIN `group` g ON gc.idgroup = g.idgroup WHERE gc.idcomplaint = " . $idcomplaint . " ORDER BY gc.idcomplaint ASC");
					
					//Consultamos los Archivos
					$query_files = $this->db->query("SELECT * FROM file WHERE idcomplaint = " . $idcomplaint . " ORDER BY createdAt ASC LIMIT 10");
					
					//Consultamos las Notificaciones
					$query_notifications = $this->db->query("SELECT n.idnotification, n.title, n.message, n.source, n.view, n.identifier, n.read, n.createdAt FROM notification n WHERE n.view = 'ViewComplaint' AND n.identifier = " . $idcomplaint . " ORDER BY n.idnotification DESC LIMIT 10");
					//Declaramos el Arreglo de Notificaciones
					$notifications = array();
					
					//Procesamos las Notificaciones
					if ($query_notifications->num_rows() > 0)
					{
						//Procesamos las Notificaciones
						foreach ($query_notifications->result() as $row_notification)
						{
							//Generamos el Elemento de Notificacion
							$notifications[] = array(
								'idnotification' => $row_notification->idnotification,
								'title' => $row_notification->title,
								'message' => $row_notification->message,
								'source' => $row_notification->source,
								'view' => $row_notification->view,
								'identifier' => $row_notification->identifier,
								'read' => $row_notification->read,
								'createdAt' => $row_notification->createdAt,
								'timeago' => $this->get_timeago($row_notification->createdAt)
							);
						}
					}
					
					//Procesamos los Grupos
					foreach ($query_groups->result() as $group)
					{
						$query_dependencies = $this->db->query("SELECT d.iddependency as iddependency, d.name as name FROM group_dependency gd INNER JOIN dependency d ON gd.iddependency = d.iddependency WHERE gd.idgroup = " . $group->idgroup);
						
						$groups[] = array(
							'idgroup' => $group->idgroup,
							'title' => $group->title,
							'description' => $group->description,
							'level' => $group->level,
							'send' => $group->send,
							'dependencies' => $query_dependencies->result(),
							'updates' => array()
						);
					}
					
					//Generamos el Elemento de Area
					$complaint = array(
						'idcomplaint' => $row->idcomplaint,
						'title' => $row->title,
						'description' => $row->description,
						'lat' => $row->lat,
						'lng' => $row->lng,
						'address' => $row->address,
						'type' => $row->type,
						$row->type => $row->media,
						'idcategory' => $row->idcategory,
						'category' => $row->category,
						'idamount' => $row->idamount,
						'amount' => $row->amount,
						'icon' => $row->icon,
						'iduser' => $row->iduser,
						'date' => $row->date,
						'status' => $row->status,
						'comments' => $row->comments,
						'follows' => $row->follows,
						'comments_data' => $query_comments->result(),
						'groups_data' => $groups,
						'files_data' => $query_files->result(),
						'notifications_data' => $notifications
					);
					
					//Mostrar Resultados
					$array = array(
						'status' => (int)1,
						'msg' => (string)'success',
						'data' => $complaint
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe esta denuncia.'
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		//updateComplaintStatus
		if ($msg == 'updateComplaintStatus')
		{
			//Leemos los datos
			$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
			$status = (isset($fields['status'])) ? (string)trim($fields['status']) : '';
			
			//Verificamos
			if ($idcomplaint)
			{
				//Actualizamos la denuncia
				$data = array(
					'status' => (int)$status
				);
				$this->db->where('idcomplaint', $idcomplaint);
				$this->db->update('complaint', $data);
				
				//Mostrar Resultados
				$array = array(
					'status' => (int)1,
					'msg' => (string)'success'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		
		if (!$output)
		{
			//Mostrar Error
			$array = array(
				'status' => (int)0,
				'msg' => (string)'Llamada inválida.',
				'data' => $msg,
				'fields' => $fields
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
	}

	function get_timeago( $timestamp )
	{
	    $time_ago = strtotime($timestamp);  
		$current_time = time();  
		$time_difference = $current_time - $time_ago;  
		$seconds = $time_difference;  
		$minutes      = round($seconds / 60 );           // value 60 is seconds  
		$hours           = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec  
		$days          = round($seconds / 86400);          //86400 = 24 * 60 * 60;  
		$weeks          = round($seconds / 604800);          // 7*24*60*60;  
		$months          = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60  
		$years          = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60  
		
		if($seconds <= 60)  
		{  
			return "Justo ahora";  
		}  
		else if($minutes <=60)  
		{  
			if($minutes==1)  
			{  
				return "hace un minuto";  
			}  
			else  
			{  
				return "hace $minutes minutos";  
			}  
		}  
		else if($hours <=24)  
		{  
			if($hours==1)  
			{  
				return "hace una hora";  
			}  
			else  
			{  
				return "hace $hours horas";  
			}  
		}  
		else if($days <= 7)  
		{  
			if($days==1)  
			{  
				return "ayer";  
			}  
			else  
			{  
				return "hace $days días";  
			}  
		}  
		else if($weeks <= 4.3) //4.3 == 52/12  
		{  
			if($weeks==1)  
			{  
				return "hace una semana";  
			}  
			else  
			{  
				return "hace $weeks semanas";  
			}  
		}  
		else if($months <=12)  
		{  
			if($months==1)  
			{  
				return "hace un mes";  
			}  
			else  
			{  
				return "hace $months meses";  
			}  
		}  
		else  
		{  
			if($years==1)  
			{  
				return "hace un año";  
			}  
			else  
			{  
				return "hace $years años";  
			}  
		}  
	}
	
}