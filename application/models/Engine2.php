<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// How to send the response header with PHP
if (isset($_SERVER['HTTP_ORIGIN'])) {
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		exit(0);
}

ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 10000);

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Aws;
use Aws\Common\Enum\Size;
use Aws\S3\Enum\CannedAcl;
use Aws\Common\Enum\Region;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\Common\Model\MultipartUpload\AbstractUploadBuilder;
use Aws\Sns\SnsClient;
use Aws\Sns\Exception\SnsException;
use Aws\Credentials\Credentials;

class Engine2 extends CI_Model {
	
	public $bucket = 'incorruptible'; //nombre del bucket

    private $_key = 'AKIAJ6COTGMIJ7WLYIOQ';

    private $_secret = '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';

    public $s3 = null;
    
	public $SNS_ACCESS_KEY = 'AKIAIK64RPVE6OOHGWLQ';
	public $SNS_SECRET_KEY = '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';

	public function __construct()
	{
		parent::__construct();
		
		$this->s3 = S3Client::factory(array(
			'version' => '2006-03-01',
			'region' => 'us-west-2',
            'credentials' => array(
                'key'    => $this->_key,
                'secret' => $this->_secret,
            )
        ));
	}

	//Funcion para identar JSON
	public function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	public function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo $this->indent($json);
	}

	//Manejador de Mensajes Recibidos en el JSON
	public function executeJSON($msg,$fields,$app,$apikey,$platform)
	{
		//Leemos la Configuracion
		$output = FALSE;
		$array = array();
		$origin = (isset($_SERVER['HTTP_ORIGIN'])) ? (string)trim($_SERVER['HTTP_ORIGIN']) : '';
		$ip = (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) ? (string)trim($_SERVER['HTTP_X_FORWARDED_FOR']) : '';
		
		//Checamos el Origen
		if ($origin == 'https://api.incorruptible.mx' || $origin == 'http://localhost:8100' || $origin == 'http://localhost:8080' || $origin == 'https://app.incorruptible.mx' || $origin == 'file://' || $origin == 'https://backend.incorruptible.mx' || $origin == 'https://incorruptible-188405.firebaseapp.com')
		{
			switch ($platform) {
				case 'ios': $app = 'inc_ios'; $apikey = 'b492a63dcf28232853e47782bc1c8e3af515a167'; break;
				case 'android': $app = 'inc_android'; $apikey = 'e63f5cd949abecc739cb46310fa8be6352d7aae9'; break;
				case 'web': $app = 'inc_web'; $apikey = 'bb1ed64b8f863b49ebbcb0efab6ddcb7e866d77d'; break;
				default: $app = 'inc_web'; $apikey = 'bb1ed64b8f863b49ebbcb0efab6ddcb7e866d77d'; break;
			}
		}
		
		//Consultamos el Usuario de la API
		$query_app = $this->db->query("SELECT * FROM app WHERE app = '" . $app . "' AND apikey = '" . $apikey . "' AND status = 1 LIMIT 1"); 
		
		//Verificamos que exista el Usuario de la API
		if ($query_app->num_rows() > 0)
		{
			//Leemos el Objeto de la App
			$row_app = $query_app->row();
			$calls = explode(',', $row_app->permissions);
			
			//Verificar apps de Incorruptible
			if ($row_app->idapp < 6)
			{
				//Verificar dominio
				if ($app == 'inc_backend') { $origin = 'https://api.incorruptible.mx'; }				
				if ($origin != 'https://api.incorruptible.mx' && $origin != 'http://localhost:8100' && $origin != 'http://localhost:8080' && $origin != 'https://app.incorruptible.mx' && $origin != 'file://' && $origin != 'https://backend.incorruptible.mx' && $origin != 'https://incorruptible-188405.firebaseapp.com')	
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No tienes permiso para ejecutar esta llamada. Comunícate con el administrador.'
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
					
					//Generamos el Log
					$this->saveLog($msg,$fields,$app,$array);
					
					die();
				}
			}
			
			// loginAdmin
			if ($msg == 'loginAdmin' && in_array('loginAdmin', $calls))
			{
				//Leemos los datos
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
	
				//Verificamos los datos
				if ($email && $password)
				{
					//Consultamos los datos
					$query = $this->db->query("SELECT * FROM admin WHERE email = '" . $email . "' AND status = 1 LIMIT 1");
	
					//Verificamos si existe el usuario
					if ($query->num_rows() > 0)
					{
						//Leemos el Objeto
						$row = $query->row();
						
						//Leemos el Hash
						$hash = $row->password;
						
						//Verificamos el Password
						if (password_verify($password, $hash)) 
						{
							//Consultamos
							$data = array(
								'idadmin' => $row->idadmin,
							    'email' => $row->email,
							    'name' => $row->name
							);
		
							//Save Admin in $_SESSION
							$this->session->set_userdata('admin', $data);
							$this->session->set_userdata('iaap_logged', true);
		
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'success',
								'data' => $data
							);
		
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
						else
						{
							//Mostrar Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'Contraseña incorrecta. Intenta de nuevo.'
							);
		
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Correo Electrónico no existe. Intenta de nuevo.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
	
			// loginUser
			if ($msg == 'loginUser' && in_array('loginUser', $calls))
			{
				//Leemos los datos
				$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
	
				//Verificamos los datos
				if ($name && $password)
				{
					//Verificar si es email
					if ($this->functions->verificaremail($name))
					{
						//Consultamos los datos
						$query = $this->db->query("SELECT * FROM user WHERE email = '" . $name . "' LIMIT 1");
					}
					else
					{
						//Consultamos los datos
						$query = $this->db->query("SELECT * FROM user WHERE name = '" . $name . "' LIMIT 1");
					}
	
					//Verificamos si existe el usuario
					if ($query->num_rows() > 0)
					{
						//Leemos el Objeto
						$row = $query->row();
						
						//Consultamos el Perfil del Usuario
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row->idprofile . " AND status = 1");
						$row_profile = $query_profile->row();
						
						//Consultamos los Estados
						$query_states = $this->db->query("SELECT * FROM state WHERE idstate = " . $row->idstate);
						$row_state = $query_states->row();
											
						//Consultamos las Denuncias del Usuario
						$query_complaints = $this->db->query("SELECT * FROM complaint WHERE iduser = " . $row->iduser . " AND status = 1");
						
						//Consultamos las Denuncias Seguidas del Usuario
						$query_follow_complaints = $this->db->query("SELECT fc.* FROM follow_complaint as fc LEFT JOIN complaint as c ON c.idcomplaint = fc.idcomplaint WHERE fc.iduser = " . $row->iduser . " AND fc.status = 1 AND c.status = 1");
						
						//Leemos el Hash
						$hash = $row->password;
						
						//Verificamos el Password
						if (password_verify($password, $hash)) 
						{
							//Consultamos
							$data = array(
								'iduser' => $row->iduser,
								'name' => $row->name,
							    'email' => $row->email,
							    'avatar' => $row->avatar,
							    'fbid' => $row->fbid,
							    'twid' => $row->twid,
							    'complaints' => $query_complaints->num_rows(),
							    'follows' => $query_follow_complaints->num_rows(),
							    'idprofile' => $row_profile->idprofile,
							    'profile' => $row_profile->name,
							    'mailing' => $row->mailing,
							    'notifications' => $row->notifications,
							    'idstate' => $row_state->idstate,
							    'state' => $row_state->name,
							    'position' => array(
								    'lat' => $row_state->lat,
								    'lng' => $row_state->lng
							    ),
							    'status' => $row->status
							);
		
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'success',
								'data' => $data
							);
		
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
						else
						{
							//Mostrar Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'Contraseña incorrecta. Intenta de nuevo.'
							);
		
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Usuario no existe. Intenta de nuevo.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// loginApp
			if ($msg == 'loginApp' && in_array('loginApp', $calls))
			{
				//Leemos los datos
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$app = (isset($fields['app'])) ? (string)trim($fields['app']) : '';
	
				//Verificamos los datos
				if ($email && $app)
				{
					//Consultamos los datos
					$query = $this->db->query("SELECT * FROM app WHERE email = '" . $email . "' AND app = '" . $app . "' AND status = 1 LIMIT 1");
	
					//Verificamos si existe el usuario
					if ($query->num_rows() > 0)
					{
						//Leemos el Objeto
						$row = $query->row();
						
						//Consultamos
						$data = array(
							'idapp' => $row->idapp,
						    'app' => $row->app,
						    'apikey' => $row->apikey,
						    'email' => $row->email,
						    'permissions' => $row->permissions
						);
	
						//Save Admin in $_SESSION
						$this->session->set_userdata('app', $data);
						$this->session->set_userdata('iapi_logged', true);
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'El correo registrado no coincide con la app específicada.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// addApp
			if ($msg == 'addApp' && in_array('addApp', $calls))
			{
				//Leemos los datos
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$app = (isset($fields['app'])) ? (string)trim($fields['app']) : '';
				$comments = (isset($fields['comments'])) ? (string)trim($fields['comments']) : '';
	
				//Verificamos los datos
				if ($email && $app)
				{
					//Consultamos los datos
					$query = $this->db->query("SELECT * FROM app WHERE app = '" . $app . "' LIMIT 1");
	
					//Verificamos si existe el usuario
					if ($query->num_rows() == 0)
					{
						//Creamos el Registro
						$data = array(
							'app' => $app,
						    'email' => $email,
						    'comments' => $comments,
						    'createdAt' => date('Y-m-d H:i:s'),
						    'status' => 0
						);
						$this->db->insert('app', $data);
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'La app ya esta registrada.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// resetApikey
			if ($msg == 'resetApikey' && in_array('resetApikey', $calls))
			{
				//Leemos los datos
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$app = (isset($fields['app'])) ? (string)trim($fields['app']) : '';
	
				//Verificamos los datos
				if ($email && $app)
				{
					//Consultamos los datos
					$query = $this->db->query("SELECT * FROM app WHERE email = '" . $email . "' AND app = '" . $app . "' AND status = 1 LIMIT 1");
	
					//Verificamos si existe el usuario
					if ($query->num_rows() > 0)
					{
						//Leemos el Objeto
						$row = $query->row();
						$apikey = sha1(now());
						
						//Creamos el código de invite
						$data = array(
							'apikey' => $apikey,
							'updatedAt' => date('Y-m-d H:i:s')
						);
						$this->db->where('idapp', $row->idapp);
						$this->db->update('app', $data);
						
						//Consultamos
						$data = array(
							'idapp' => $row->idapp,
						    'app' => $row->app,
						    'apikey' => $apikey,
						    'email' => $row->email,
						    'permissions' => $row->permissions
						);
	
						//Save Admin in $_SESSION
						$this->session->set_userdata('app', $data);
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Ocurrió un error, contacta al administrador.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// createUser
			if ($msg == 'createUser' && in_array('createUser', $calls))
			{
				//Leemos los Datos
				$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$idprofile = (isset($fields['idprofile'])) ? (string)trim($fields['idprofile']) : '';
				$idstate = (isset($fields['idstate'])) ? (string)trim($fields['idstate']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
				$ong = (isset($fields['ong'])) ? (string)trim($fields['ong']) : '';
				$dependency = (isset($fields['dependency'])) ? (string)trim($fields['dependency']) : '';
	
				//Verificamos los Datos
				if ($name && $email && $password)
				{
					//Consultamos al Admin
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() == 0)
					{
						$token = sha1(now());
						
						//Guardamos los Datos
						$data = array(
							'email' => $email,
							'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
						    'name' => $name,
						    'idprofile' => 1,
						    'idstate' => $idstate,
						    'token' => $token,
						    'mailing' => 1,
						    'notifications' => 1,
						    'createdAt' => date('Y-m-d H:i:s'),
						    'status' => 0
						);
						$this->db->insert('user', $data);
						$iduser = $this->db->insert_id();
						
						//Consultamos el Perfil del Usuario
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = 1 AND status = 1");
						$row_profile = $query_profile->row();
						
						$data = array(
							'iduser' => $iduser,
							'email' => $email,
							'password' => $password,
							'name' => $name,
							'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name,
							'link' => 'https://app.incorruptible.mx/#/verify/'.$iduser.'/'.$token
						);
						
						$data_result = array(
							'iduser' => $iduser,
							'name' => $name,
							'email' => $email,
							'avatar' => '',
							'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name
						);
	
						//Correo de Notificación
						$subject = 'Bienvenido a Incorruptible';
						$body = $this->load->view('mail/welcome_user', $data, TRUE);
						$altbody = strip_tags($body);
	
						//Verificamos
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Mandamos el Correo
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
					        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
					        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email, $name);
					        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
	
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data_result
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Este correo de usuario ya esta registrado.',
							'data' => array()
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// createUserONG
			if ($msg == 'createUserONG' && in_array('createUserONG', $calls))
			{
				//Leemos los Datos
				$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$idprofile = (isset($fields['idprofile'])) ? (string)trim($fields['idprofile']) : '';
				$idstate = (isset($fields['idstate'])) ? (string)trim($fields['idstate']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
				$ong = (isset($fields['ong'])) ? (string)trim($fields['ong']) : '';
				$dependency = (isset($fields['dependency'])) ? (string)trim($fields['dependency']) : '';
	
				//Verificamos los Datos
				if ($name && $email && $password)
				{
					//Consultamos al Admin
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() == 0)
					{
						$token = sha1(now());
						
						//Guardamos los Datos
						$data = array(
							'email' => $email,
							'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
						    'name' => $name,
						    'idprofile' => 2,
						    'idstate' => $idstate,
						    'token' => $token,
						    'mailing' => 1,
						    'notifications' => 1,
						    'createdAt' => date('Y-m-d H:i:s'),
						    'status' => 0
						);
						$this->db->insert('user', $data);
						$iduser = $this->db->insert_id();
						
						//Generamos la Relación con la ONG
						$data = array(
							'iduser' => $iduser,
							'idong' => $ong,
							'createdAt' => date('Y-m-d H:i:s'),
						    'status' => 1
						);
						$this->db->insert('user_ong', $data);
						
						//Consultamos el Perfil del Usuario
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = 2 AND status = 1");
						$row_profile = $query_profile->row();
						
						$data = array(
							'iduser' => $iduser,
							'email' => $email,
							'password' => $password,
							'name' => $name,
							'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name,
							'link' => 'https://app.incorruptible.mx'
						);
						
						$data_result = array(
							'iduser' => $iduser,
							'name' => $name,
							'email' => $email,
							'avatar' => '',
							'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name
						);
	
						//Correo de Notificación
						$subject = 'Bienvenido a Incorruptible';
						$body = $this->load->view('mail/welcome_user_ong', $data, TRUE);
						$altbody = strip_tags($body);
	
						//Verificamos
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Mandamos el Correo
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
					        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
					        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email, $name);
					        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
	
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data_result
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Este correo de usuario ya esta registrado.',
							'data' => array()
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// createUserDependency
			if ($msg == 'createUserDependency' && in_array('createUserDependency', $calls))
			{
				//Leemos los Datos
				$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$idprofile = (isset($fields['idprofile'])) ? (string)trim($fields['idprofile']) : '';
				$idstate = (isset($fields['idstate'])) ? (string)trim($fields['idstate']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
				$ong = (isset($fields['ong'])) ? (string)trim($fields['ong']) : '';
				$dependency = (isset($fields['dependency'])) ? (string)trim($fields['dependency']) : '';
	
				//Verificamos los Datos
				if ($name && $email && $password)
				{
					//Consultamos al Admin
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() == 0)
					{
						$token = sha1(now());
						
						//Guardamos los Datos
						$data = array(
							'email' => $email,
							'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
						    'name' => $name,
						    'idprofile' => 3,
						    'idstate' => $idstate,
						    'token' => $token,
						    'mailing' => 1,
						    'notifications' => 1,
						    'createdAt' => date('Y-m-d H:i:s'),
						    'status' => 0
						);
						$this->db->insert('user', $data);
						$iduser = $this->db->insert_id();
						
						//Generamos la Relación con la Dependencia
						$data = array(
							'iduser' => $iduser,
							'iddependency' => $dependency,
							'createdAt' => date('Y-m-d H:i:s'),
						    'status' => 1
						);
						$this->db->insert('user_dependency', $data);
						
						//Consultamos el Perfil del Usuario
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = 3 AND status = 1");
						$row_profile = $query_profile->row();
						
						$data = array(
							'iduser' => $iduser,
							'email' => $email,
							'password' => $password,
							'name' => $name,
							'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name,
							'link' => 'https://app.incorruptible.mx'
						);
						
						$data_result = array(
							'iduser' => $iduser,
							'name' => $name,
							'email' => $email,
							'avatar' => '',
							'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name
						);
	
						//Correo de Notificación
						$subject = 'Bienvenido a Incorruptible';
						$body = $this->load->view('mail/welcome_user_dependency', $data, TRUE);
						$altbody = strip_tags($body);
	
						//Verificamos
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Mandamos el Correo
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
					        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
					        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email, $name);
					        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
	
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data_result
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Este correo de usuario ya esta registrado.',
							'data' => array()
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// forgotPassword
			if ($msg == 'forgotPassword' && in_array('forgotPassword', $calls))
			{
				//Leemos los Datos
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				
				//Verificamos los Datos
				if ($email)
				{
					//Consultamos al Admin
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Leemos el Objeto
						$row = $query->row();
						
						//Revisamos que el Usuario no sea de Facebook
						if ((string)trim($row->fbid))
						{
							//Generamos el Arreglo
							$array = array(
								'status' => (int)0,
								'msg' => 'Este usuario usa Facebook para iniciar sesión.'
							);
						}
						else
						{
							//Token
							$token = sha1(now());
							//$token = substr($code, 0, 8);
							
							//Creamos el código de invite
							$data = array(
								'token' => $token
							);
							$this->db->where('iduser', $row->iduser);
							$this->db->update('user', $data);
							
							//Enviamos el Correo
							$name = $row->name;
							$name_array = explode(' ', $name);
							$data['name'] = $name_array[0];
							$data['token'] = 'http://app.incorruptible.mx/#/reset/' . $row->iduser . '/' . $token;
							
							//Correo de Notificación
							$subject = 'Restablecer Contraseña';
							$body = $this->load->view('mail/reset_password', $data, TRUE);
							$altbody = strip_tags($body);
							$email = $row->email;
					
							//Verificamos
							if ($subject != '' && $body != '' && $altbody != '' && $email != '')
							{
								//Mandamos el Correo
								$mail = new PHPMailer(true);
								$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
								$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
								$mail->CharSet = 'UTF-8';
								$mail->Encoding = "quoted-printable";
						        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
						        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
						        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
						        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
						        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
						        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
						        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
						        $mail->SMTPDebug  = 0;
						        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
						        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
						        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
						        $mail->Body      = $body;
						        $mail->AltBody    = $altbody;
						        $mail->AddAddress($email, $name);
						        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
					
						        if ($mail->Send())
								{
									$response = true;
								}
								else
								{
									$response = false;
								}
							}
		
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'Se enviará un correo electrónico con un link para restablecer la contraseña.',
								'response' => $response
							);
						}
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Este correo de usuario no esta registrado.',
							'data' => array()
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// resetPassword
			if ($msg == 'resetPassword' && in_array('resetPassword', $calls))
			{
				//Leemos los Datos
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$token = (isset($fields['token'])) ? (string)trim($fields['token']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
				
				//Verificamos los Datos
				if ($iduser && $token && $password)
				{
					//Consultamos al Admin
					$query = $this->db->query("SELECT * FROM user WHERE iduser = '" . $iduser . "' AND token = '" . $token . "' LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Leemos el Objeto
						$row = $query->row();
						
						//Revisamos que el Usuario no sea de Facebook
						if ((string)trim($row->fbid))
						{
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'Este usuario usa Facebook para iniciar sesión.'
							);
						}
						else
						{
							//Creamos el código de invite
							$data = array(
								'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
								'token' => ''
							);
							$this->db->where('iduser', $row->iduser);
							$this->db->update('user', $data);
							
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'Se ha actualizado la contraseña correctamente.'
							);
						}
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'El token ya ha sido utilizado.',
							'data' => array()
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// resetPassword
			if ($msg == 'verifyUser' && in_array('verifyUser', $calls))
			{
				//Leemos los Datos
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$token = (isset($fields['token'])) ? (string)trim($fields['token']) : '';
				
				//Verificamos los Datos
				if ($iduser && $token)
				{
					//Consultamos al Admin
					$query = $this->db->query("SELECT * FROM user WHERE iduser = '" . $iduser . "' AND token = '" . $token . "' LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Leemos el Objeto
						$row = $query->row();
						
						//Creamos el código de invite
						$data = array(
							'token' => '',
							'status' => 1
						);
						$this->db->where('iduser', $row->iduser);
						$this->db->update('user', $data);
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'Se ha verificado el correo electrónico correctamente.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Tu cuenta ya ha sido verificada.',
							'data' => array()
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'fields' => $fields
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getUsers
			if ($msg == 'getUsers' && in_array('getUsers', $calls))
			{
				//Leemos los Campos
				$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
				$limit = '10';
				$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
				
				$query = $this->db->query("SELECT * FROM user ORDER BY createdAt DESC LIMIT " . $offset . "," . $limit);
				$users = $query->result();
				$query = $this->db->query("SELECT * FROM user ORDER BY createdAt DESC");
				$users_all = $query->result();
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => array(
						'users' => $users,
						'users_all' => $users_all,
						'page' => $page
					)
				);
	
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
				
				die();
			}
			
			// getUser
			if ($msg == 'getUser' && in_array('getUser', $calls))
			{
				//Leemos los Datos
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '1';
	
				//Verificamos
				if ($iduser)
				{
					//Consultamos
					$query = $this->db->query("SELECT * FROM user WHERE iduser = " . $iduser . " LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Declaramos el Arreglo de Admin
						$row = $query->row();
						$type = array();
	
						//Consultamos el Perfil del Usuario
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row->idprofile . " AND status = 1");
						$row_profile = $query_profile->row();
						
						//Consultamos el Estado
						$query_state = $this->db->query("SELECT * FROM state WHERE idstate = " . $row->idstate . " AND status = 1");
						$row_state = $query_state->row();
						
						//Consultamos si es de una ONG
						$query_ong = $this->db->query("SELECT o.idong as idong, o.name as name FROM user u INNER JOIN user_ong uo ON uo.iduser = u.iduser INNER JOIN ong o ON o.idong = uo.idong WHERE u.iduser = " . $row->iduser . " AND u.status = 1");
						if ($query_ong->num_rows() > 0) 
						{
							$row_ong = $query_ong->row();
							$type = array(
								'type' => 'ONG',
								'id' => $row_ong->idong,
								'name' => $row_ong->name
							);
						}
						
						//Consultamos si es de una Autoridad
						$query_dependency = $this->db->query("SELECT d.iddependency as iddependency, d.name as name FROM user u INNER JOIN user_dependency ud ON ud.iduser = u.iduser INNER JOIN dependency d ON d.iddependency = ud.iddependency WHERE u.iduser = " . $row->iduser . " AND u.status = 1");
						if ($query_dependency->num_rows() > 0) 
						{
							$row_dependency = $query_dependency->row();
							$type = array(
								'type' => 'Autoridad',
								'id' => $row_dependency->iddependency,
								'name' => $row_dependency->name
							);
						}
											
						//Consultamos las Denuncias del Usuario
						$query_complaints = $this->db->query("SELECT * FROM complaint WHERE iduser = " . $row->iduser . " AND status = 1");
						
						//Consultamos las Denuncias Seguidas del Usuario
						$query_follow_complaints = $this->db->query("SELECT fc.* FROM follow_complaint as fc LEFT JOIN complaint as c ON c.idcomplaint = fc.idcomplaint WHERE fc.iduser = " . $row->iduser . " AND fc.status = 1 AND c.status = 1");
	
						//Consultamos
						$data = array(
							'iduser' => $row->iduser,
							'name' => $row->name,
						    'email' => $row->email,
						    'avatar' => $row->avatar,
						    'fbid' => $row->fbid,
						    'twid' => $row->twid,
						    'complaints' => $query_complaints->num_rows(),
						    'follows' => $query_follow_complaints->num_rows(),
						    'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name,
						    'idstate' => $row_state->idstate,
						    'state' => $row_state->name,
						    'type' => $type
						);
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe el usuario.',
							'data' => array()
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// updateUser
			if ($msg == 'updateUser' && in_array('updateUser', $calls))
			{
				//Leemos los Datos
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$profile = (isset($fields['profile'])) ? (string)trim($fields['profile']) : '';
				$state = (isset($fields['state'])) ? (string)trim($fields['state']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
				$ong = (isset($fields['ong'])) ? (string)trim($fields['ong']) : '';
				$dependency = (isset($fields['dependency'])) ? (string)trim($fields['dependency']) : '';
	
				//Verificamos los Datos
				if ($iduser && $name)
				{
					//Consultamos al Usuario
					$query = $this->db->query("SELECT * FROM user WHERE iduser = " . $iduser . " AND status = 1 LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Actualizamos los Datos
						$data = array();
						$data['name'] = $name;
						if ($password) { $data['password'] = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]); }
						$data['idprofile'] = $profile;
						$data['idstate'] = $state;
						$data['updatedAt'] = date('Y-m-d H:i:s');
						$this->db->where('iduser', $iduser);
						$this->db->update('user', $data);
						
						//Actualizamos la ONG
						if ($profile == 2)
						{
							$data = array();
							$data['idong'] = $ong;
							$this->db->where('iduser', $iduser);
							$this->db->update('user_ong', $data);
						}
						
						//Actualizamos la Autoridad
						if ($profile == 3)
						{
							$data = array();
							$data['iddependency'] = $dependency;
							$this->db->where('iduser', $iduser);
							$this->db->update('user_dependency', $data);
						}
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'Usuario actualizado con éxito.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe este usuario.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// deleteUser
			if ($msg == 'deleteUser' && in_array('deleteUser', $calls))
			{
				//Leemos el Id User
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				
				//Verificamos
				if ($iduser)
				{
					//Actualizamos el status
					$data = array(
						'status' => 0
					);
					$this->db->where('iduser', $iduser);
					$this->db->update('user', $data);
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $iduser
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// activateUser
			if ($msg == 'activateUser' && in_array('activateUser', $calls))
			{
				//Leemos el Id User
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				
				//Verificamos
				if ($iduser)
				{
					//Actualizamos el status
					$data = array(
						'status' => 1
					);
					$this->db->where('iduser', $iduser);
					$this->db->update('user', $data);
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $iduser
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getAdminProfile
			if ($msg == 'getAdminProfile' && in_array('getAdminProfile', $calls))
			{
				//Leemos los Campos
				$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '';
	
				//Verificamos los Datos
				if ($idadmin)
				{
					//Consultamos los Datos del Admin
					$query = $this->db->query("SELECT * FROM admin WHERE idadmin = " . $idadmin . " AND status = 1 LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Leemos el objeto
						$row = $query->row();
	
						//Generamos el Elemento de Admin
						$admin = array(
							'idadmin' => $row->idadmin,
							'name' => $row->name,
							'email' => $row->email
						);
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $admin
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe este administrador.',
							'data' => array()
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
	
			}
	
			// updateProfileAdmin
			if ($msg == 'updateProfileAdmin' && in_array('updateProfileAdmin', $calls))
			{
				//Leemos los Datos
				$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '';
				$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
	
				//Verificamos los Datos
				if ($idadmin && $name)
				{
					//Consultamos al Admin
					$query = $this->db->query("SELECT * FROM admin WHERE idadmin = " . $idadmin . " AND status = 1 LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Actualizamos los Datos
						$data = array();
						$data['name'] = $name;
						if ($password) { $data['password'] = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]); }
						$this->db->where('idadmin', $idadmin);
						$this->db->update('admin', $data);
	
						//Actualizamos los datos de SESSION
						$data = array(
							'idadmin' => $idadmin,
						    'email' => $email,
						    'name' => $name
						);
						$this->session->set_userdata('admin', $data);
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'Administrador actualizado con éxito.',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe este administrador.',
							'data' => array()
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
	
			// createAdmin
			if ($msg == 'createAdmin' && in_array('createAdmin', $calls))
			{
				//Leemos los Datos
				$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
	
				//Verificamos los Datos
				if ($name && $email && $password)
				{
					//Consultamos al Admin
					$query = $this->db->query("SELECT * FROM admin WHERE email = '" . $email . "' LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() == 0)
					{
						//Guardamos los Datos
						$data = array(
							'email' => $email,
							'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
						    'name' => $name,
						    'createdAt' => date('Y-m-d H:i:s'),
						    'status' => 1
						);
						$this->db->insert('admin', $data);
						$idadmin = $this->db->insert_id();
						
						$data = array(
							'idadmin' => $idadmin,
							'email' => $email,
							'password' => $password,
							'name' => $name,
							'link' => base_url()
						);
	
						//Correo de Notificación
						$subject = 'Bienvenido a Incorruptible';
						$body = $this->load->view('mail/welcome_admin', $data, TRUE);
						$altbody = strip_tags($body);
	
						//Verificamos
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Mandamos el Correo
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
					        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
					        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email, $name);
					        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
	
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Este correo de administrador ya esta registrado.',
							'data' => array()
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
	
			// getAdmins
			if ($msg == 'getAdmins' && in_array('getAdmins', $calls))
			{
				//Leemos los Datos
				$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
				$limit = '10';
				$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
	
				//Consultamos
				$query = $this->db->query("SELECT * FROM admin WHERE status = 1 ORDER BY idadmin DESC LIMIT " . $offset . "," . $limit);
				$query_all = $this->db->query("SELECT * FROM admin WHERE status = 1");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$admins = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$admins[] = array(
							'idadmin' => $row->idadmin,
							'email' => $row->email,
							'name' => $row->name,
							'status' => $row->status
						);
					}
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $admins,
						'page' => $page,
						'offset' => $offset,
						'limit' => $limit,
						'total' => (string)$query_all->num_rows()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay administradores.',
						'data' => array(),
						'page' => $page,
						'offset' => $offset,
						'limit' => $limit,
						'total' => (string)$query_all->num_rows()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
	
			// getAdmin
			if ($msg == 'getAdmin' && in_array('getAdmin', $calls))
			{
				//Leemos los Datos
				$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '1';
	
				//Verificamos
				if ($idadmin)
				{
					//Consultamos
					$query = $this->db->query("SELECT * FROM admin WHERE idadmin = " . $idadmin . " LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Declaramos el Arreglo de Admin
						$row = $query->row();
	
						//Generamos el Elemento de Area
						$admin = array(
							'idadmin' => $row->idadmin,
							'email' => $row->email,
							'name' => $row->name,
							'status' => $row->status
						);
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $admin
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe el administrador.',
							'data' => array()
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// deleteAdmin
			if ($msg == 'deleteAdmin' && in_array('deleteAdmin', $calls))
			{
				//Leemos el Id Admin
				$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '';
				
				//Verificamos
				if ($idadmin)
				{
					//Actualizamos el status
					$data = array(
						'status' => 0
					);
					$this->db->where('idadmin', $idadmin);
					$this->db->update('admin', $data);
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $idadmin
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
	
			// updateAdmin
			if ($msg == 'updateAdmin' && in_array('updateAdmin', $calls))
			{
				//Leemos los Datos
				$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '';
				$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
				$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
	
				//Verificamos los Datos
				if ($idadmin && $name)
				{
					//Consultamos al Admin
					$query = $this->db->query("SELECT * FROM admin WHERE idadmin = " . $idadmin . " AND status = 1 LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Actualizamos los Datos
						$data = array();
						$data['name'] = $name;
						if ($password) { $data['password'] = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]); }
						$this->db->where('idadmin', $idadmin);
						$this->db->update('admin', $data);
	
						//Actualizamos los datos de SESSION
						$data = array(
							'idadmin' => $idadmin,
						    'email' => $email,
						    'name' => $name
						);
						$this->session->set_userdata('admin', $data);
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'Administrador actualizado con éxito.',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe este administrador.',
							'data' => array()
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getCategories
			if ($msg == 'getCategories' && in_array('getCategories', $calls))
			{
				//Leemos los Datos
				$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
				$limit = '10';
				$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
	
				//Consultamos
				$query = $this->db->query("SELECT * FROM category WHERE category.status = 1 ORDER BY category.order ASC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$categories = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$categories[] = array(
							'idcategory' => $row->idcategory,
							'name' => $row->name,
							'description' => $row->description,
							'icon' => $row->icon,
							'order' => $row->order
						);
					}
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $categories
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay categorías.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getAmounts
			if ($msg == 'getAmounts' && in_array('getAmounts', $calls))
			{
				//Leemos los Datos
				$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
				$limit = '10';
				$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
	
				//Consultamos
				$query = $this->db->query("SELECT * FROM amount WHERE amount.status = 1 ORDER BY amount.order ASC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$amounts = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$amounts[] = array(
							'idamount' => $row->idamount,
							'value' => $row->value,
							'order' => $row->order
						);
					}
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $amounts
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay cantidades asignadas.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// saveComplaint
			if ($msg == 'saveComplaint' && in_array('saveComplaint', $calls))
			{
				//Leemos los Datos
				$title = (isset($fields['title'])) ? (string)trim($fields['title']) : '';
				$description = (isset($fields['description'])) ? (string)trim($fields['description']) : '';
				$lat = (isset($fields['lat'])) ? (string)trim($fields['lat']) : '';
				$lng = (isset($fields['lng'])) ? (string)trim($fields['lng']) : '';
				$address = (isset($fields['address'])) ? (string)trim($fields['address']) : '';
				$type = (isset($fields['type'])) ? (string)trim($fields['type']) : '';
				$media = (isset($fields['media'])) ? (string)trim($fields['media']) : '';
				$idcategory = (isset($fields['idcategory'])) ? (string)trim($fields['idcategory']) : '';
				$idamount = (isset($fields['idamount'])) ? (string)trim($fields['idamount']) : '';
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$files = (isset($fields['files'])) ? $fields['files'] : array();
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $fields
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
				die();
				
				//Verificamos Data
				if ($title && $description && $address && $type && $idcategory && $idamount && $iduser)
				{
					//Guardamos los Datos
					$data = array(
						'title' => $title,
						'description' => $description,
					    'lat' => $lat,
					    'lng' => $lng,
					    'address' => $address,
					    'idstate' => 7,
					    'type' => $type,
					    'media' => $media,
					    'idcategory' => $idcategory,
					    'idamount' => $idamount,
					    'icon' => 1,
					    'iduser' => $iduser,
					    'createdAt' => date('Y-m-d H:i:s'),
					    'status' => 0
					);
					$this->db->insert('complaint', $data);
					$idcomplaint = $this->db->insert_id();
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $idcomplaint
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getComplaints
			if ($msg == 'getComplaints' && in_array('getComplaints', $calls))
			{
				//Leer Valores
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$time = (isset($fields['time'])) ? (string)trim($fields['time']) : 'year';
				$category = (isset($fields['category'])) ? (string)trim($fields['category']) : '0';
				$amount = (isset($fields['amount'])) ? (string)trim($fields['amount']) : '0';
				$type = (isset($fields['type'])) ? (string)trim($fields['type']) : '1';
				$popular = (isset($fields['popular'])) ? (string)trim($fields['popular']) : '1';
				
				//Consultamos
				$sql = "SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status, c.substatus as substatus FROM complaint c";
				
				//INNER JOIN CATEGORY
				$sql.= " INNER JOIN category ON c.idcategory = category.idcategory";
				
				//INNER JOIN AMOUNT
				$sql.= " INNER JOIN amount ON c.idamount = amount.idamount";
				
				//Filter Type
				if ($type!='1')
				{
					$sql.= " WHERE c.status = " . $type;
				}
				else
				{
					$sql.= " WHERE c.status IN (1,2,8,6,7,3,4,9)";
				}
				
				//Filter Time
				if ($time == "year")
				{
					$sql.= " AND YEAR(c.createdAt) = YEAR(CURRENT_DATE - INTERVAL 1 YEAR)";
				}
				else
				{
					$sql.= " AND YEAR(c.createdAt) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(c.createdAt) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)";
				}
				
				//Filter Category
				if ($category)
				{
					$sql.= " AND category.idcategory = " . $category;
				}
				
				//Filter Amount
				if ($amount)
				{
					$sql.= " AND amount.idamount = " . $amount;
				}
				
				
				$sql.= " ORDER BY c.idcomplaint ASC";
				$query = $this->db->query($sql);
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$complaints = array();
					$complaints_user = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$complaints[] = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							'media' => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'date' => $row->date,
							'status' => $row->status,
							'substatus' => $row->substatus
						);
					}
					
					//Vericamos el usuario
					if ($iduser)
					{
						//Consultamos
						$query_user = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status, c.substatus as substatus FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status IN (0,1) AND c.iduser = ".$iduser." ORDER BY c.idcomplaint DESC");
						
						//Procesamos los Admins
						foreach ($query_user->result() as $row_user)
						{
							//Generamos el Elemento de Area
							$complaints_user[] = array(
								'idcomplaint' => $row_user->idcomplaint,
								'title' => $row_user->title,
								'description' => $row_user->description,
								'lat' => $row_user->lat,
								'lng' => $row_user->lng,
								'address' => $row_user->address,
								'type' => $row_user->type,
								'media' => $row_user->media,
								'idcategory' => $row_user->idcategory,
								'category' => $row_user->category,
								'category_description' => $row_user->category_description,
								'idamount' => $row_user->idamount,
								'amount' => $row_user->amount,
								'icon' => $row_user->icon,
								'iduser' => $row_user->iduser,
								'date' => $row_user->date,
								'status' => $row->status,
								'substatus' => $row->substatus
							);
						}
					}
					
					//Reset Duplicados
					$array_complaints= array_merge($complaints,$complaints_user);
					$duplicados = array_map("unserialize", array_unique(array_map("serialize", $array_complaints)));
					array_values($duplicados);
					
					//Leemos las Categorias
					$query_categories = $this->db->query("SELECT c.idcategory as idcategory, c.name as name, c.description as description, c.icon as icon FROM category c ORDER BY c.order ASC");
	
					//Leemos los Montos
					$query_amounts = $this->db->query("SELECT a.idamount as idamount, a.value as value, a.sum as sum FROM amount a WHERE a.status = 1 ORDER BY a.order ASC");
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => array(
							'complaints' => $duplicados,
							'categories' => $query_categories->result(),
							'amounts' => $query_amounts->result(),
							'types' => array(
								[
									'idtype' => 0,
									'name' => 'Nuevas'
								],
								[
									'idtype' => 1,
									'name' => 'Válidas'
								],
								[
									'idtype' => 2,
									'name' => 'Agrupadas'
								],
								[
									'idtype' => 3,
									'name' => 'Resultas'
								],
								[
									'idtype' => 4,
									'name' => 'Desechadas'
								],
								[
									'idtype' => 5,
									'name' => 'Spam'
								],
								[
									'idtype' => 6,
									'name' => 'Enviadas'
								],
								[
									'idtype' => 7,
									'name' => 'En proceso'
								],
								[
									'idtype' => 8,
									'name' => 'Sin Autoridad'
								],
								[
									'idtype' => 9,
									'name' => 'No competente'
								]
							)
						),
						'query' => $sql
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay denuncias creadas con los parámetros de búsqueda.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getComplaintsByPage
			if ($msg == 'getComplaintsByPage' && in_array('getComplaintsByPage', $calls))
			{
				//Leer Valores
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
				$limit = '10';
				$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
				
				$query = $this->db->query("SELECT * FROM user ORDER BY createdAt DESC LIMIT " . $offset . "," . $limit);
				$users = $query->result();
				$query = $this->db->query("SELECT * FROM user ORDER BY createdAt DESC");
				$users_all = $query->result();
				
				//Consultamos
				$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 ORDER BY c.idcomplaint DESC LIMIT " . $offset . "," . $limit);
				$query_all = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 ORDER BY c.idcomplaint DESC");
				$complaints_all = $query_all->result();
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$complaints = array();
					$complaints_user = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$complaints[] = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							'media' => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'date' => $row->date,
							'status' => $row->status
						);
					}
					
					//Vericamos el usuario
					if ($iduser)
					{
						//Consultamos
						$query_user = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status IN (0,1) AND c.iduser = ".$iduser." ORDER BY c.idcomplaint DESC");
						
						//Procesamos los Admins
						foreach ($query_user->result() as $row_user)
						{
							//Generamos el Elemento de Area
							$complaints_user[] = array(
								'idcomplaint' => $row_user->idcomplaint,
								'title' => $row_user->title,
								'description' => $row_user->description,
								'lat' => $row_user->lat,
								'lng' => $row_user->lng,
								'address' => $row_user->address,
								'type' => $row_user->type,
								'media' => $row_user->media,
								'idcategory' => $row_user->idcategory,
								'category' => $row_user->category,
								'category_description' => $row_user->category_description,
								'idamount' => $row_user->idamount,
								'amount' => $row_user->amount,
								'icon' => $row_user->icon,
								'iduser' => $row_user->iduser,
								'date' => $row_user->date,
								'status' => $row->status
							);
						}
					}
					
					//Reset Duplicados
					$array_complaints= array_merge($complaints,$complaints_user);
					$duplicados = array_map("unserialize", array_unique(array_map("serialize", $array_complaints)));
					array_values($duplicados);
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => array(
							'complaints' => $duplicados,
							'complaints_all' => $complaints_all,
							'page' => $page
						)
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay denuncias creadas.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getComplaintsONG
			if ($msg == 'getComplaintsONG' && in_array('getComplaintsONG', $calls))
			{
				//Leemos las variables
				$time = (isset($fields['time'])) ? (string)trim($fields['time']) : 'year';
				$category = (isset($fields['category'])) ? (string)trim($fields['category']) : '0';
				$amount = (isset($fields['amount'])) ? (string)trim($fields['amount']) : '0';
				$type = (isset($fields['type'])) ? (string)trim($fields['type']) : '1';
				$popular = (isset($fields['popular'])) ? (string)trim($fields['popular']) : '1';
				
				//Consultamos
				$sql = "SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status, c.substatus as substatus FROM complaint c";
				
				//INNER JOIN CATEGORY
				$sql.= " INNER JOIN category ON c.idcategory = category.idcategory";
				
				//INNER JOIN AMOUNT
				$sql.= " INNER JOIN amount ON c.idamount = amount.idamount";
				
				//Filter Type
				if ($type!='1')
				{
					$sql.= " WHERE c.status = " . $type;
				}
				else
				{
					$sql.= " WHERE c.status IN (0,1,2,8,6,7,3,4,9)";
				}
				
				//Filter Time
				if ($time == "year")
				{
					$sql.= " AND YEAR(c.createdAt) = YEAR(CURRENT_DATE - INTERVAL 1 YEAR)";
				}
				else
				{
					$sql.= " AND YEAR(c.createdAt) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(c.createdAt) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)";
				}
				
				//Filter Category
				if ($category)
				{
					$sql.= " AND category.idcategory = " . $category;
				}
				
				//Filter Amount
				if ($amount)
				{
					$sql.= " AND amount.idamount = " . $amount;
				}
				
				
				$sql.= " ORDER BY c.idcomplaint ASC";
				$query = $this->db->query($sql);
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$complaints = array();
					$complaints_user = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$complaints[] = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							'media' => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'date' => $row->date,
							'status' => $row->status,
							'substatus' => $row->substatus,
						);
					}
					
					//Leemos las Categorias
					$query_categories = $this->db->query("SELECT c.idcategory as idcategory, c.name as name, c.description as description, c.icon as icon FROM category c ORDER BY c.order ASC");
	
					//Leemos los Montos
					$query_amounts = $this->db->query("SELECT a.idamount as idamount, a.value as value, a.sum as sum FROM amount a WHERE a.status = 1 ORDER BY a.order ASC");
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => array(
							'complaints' => $complaints,
							'categories' => $query_categories->result(),
							'amounts' => $query_amounts->result(),
							'types' => array(
								[
									'idtype' => 0,
									'name' => 'Nuevas'
								],
								[
									'idtype' => 1,
									'name' => 'Válidas'
								],
								[
									'idtype' => 2,
									'name' => 'Agrupadas'
								],
								[
									'idtype' => 3,
									'name' => 'Resultas'
								],
								[
									'idtype' => 4,
									'name' => 'Desechadas'
								],
								[
									'idtype' => 5,
									'name' => 'Spam'
								],
								[
									'idtype' => 6,
									'name' => 'Enviadas'
								],
								[
									'idtype' => 7,
									'name' => 'En proceso'
								],
								[
									'idtype' => 8,
									'name' => 'Sin Autoridad'
								],
								[
									'idtype' => 9,
									'name' => 'No competente'
								]
							)
						)
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay denuncias creadas.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getComplaintsDependency
			if ($msg == 'getComplaintsDependency' && in_array('getComplaintsDependency', $calls))
			{
				//Leemos las variables
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				
				//Consultamos
				$sql = "SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status, c.substatus as substatus, gc.idgroup as idgroup FROM group_dependency gd INNER JOIN group_complaint gc ON gc.idgroup = gd.idgroup INNER JOIN complaint c ON c.idcomplaint = gc.idcomplaint INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount INNER JOIN user_dependency ud ON ud.iddependency = gd.iddependency WHERE ud.iduser = " . $iduser . " ORDER BY c.idcomplaint ASC";
				$query = $this->db->query($sql);
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$complaints = array();
					$complaints_user = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$complaints[] = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							'media' => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'idgroup' => $row->idgroup,
							'date' => $row->date,
							'status' => $row->status,
							'substatus' => $row->substatus
						);
					}
					
					//Leemos las Categorias
					$query_categories = $this->db->query("SELECT c.idcategory as idcategory, c.name as name, c.description as description, c.icon as icon FROM category c ORDER BY c.order ASC");
	
					//Leemos los Montos
					$query_amounts = $this->db->query("SELECT a.idamount as idamount, a.value as value, a.sum as sum FROM amount a WHERE a.status = 1 ORDER BY a.order ASC");
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => array(
							'complaints' => $complaints,
							'categories' => $query_categories->result(),
							'amounts' => $query_amounts->result(),
							'types' => array(
								[
									'idtype' => 0,
									'name' => 'Nuevas'
								],
								[
									'idtype' => 1,
									'name' => 'Válidas'
								],
								[
									'idtype' => 2,
									'name' => 'Agrupadas'
								],
								[
									'idtype' => 3,
									'name' => 'Resultas'
								],
								[
									'idtype' => 4,
									'name' => 'Desechadas'
								],
								[
									'idtype' => 5,
									'name' => 'Spam'
								],
								[
									'idtype' => 6,
									'name' => 'Enviadas'
								],
								[
									'idtype' => 7,
									'name' => 'En proceso'
								],
								[
									'idtype' => 8,
									'name' => 'Sin Autoridad'
								],
								[
									'idtype' => 9,
									'name' => 'No competente'
								]
							)
						)
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay denuncias creadas.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getComplaintsGroupDependency
			if ($msg == 'getComplaintsGroupDependency' && in_array('getComplaintsGroupDependency', $calls))
			{
				//Leemos las variables
				$idgroup = (isset($fields['idgroup'])) ? (string)trim($fields['idgroup']) : '';
				$query_group = $this->db->query("SELECT * FROM `group` WHERE idgroup = " . $idgroup);
				
				//Consultamos
				$sql = "SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status, gc.idgroup as idgroup FROM group_dependency gd INNER JOIN group_complaint gc ON gc.idgroup = gd.idgroup INNER JOIN complaint c ON c.idcomplaint = gc.idcomplaint INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount INNER JOIN user_dependency ud ON ud.iddependency = gd.iddependency WHERE gc.idgroup = " . $idgroup . " AND gc.status = 1 GROUP BY c.idcomplaint ORDER BY c.idcomplaint ASC";
				$query = $this->db->query($sql);
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$complaints = array();
					$complaints_user = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$complaints[] = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							'media' => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'idgroup' => $row->idgroup,
							'date' => $row->date,
							'status' => $row->status
						);
					}
					
					//Leemos las Categorias
					$query_categories = $this->db->query("SELECT c.idcategory as idcategory, c.name as name, c.description as description, c.icon as icon FROM category c ORDER BY c.order ASC");
	
					//Leemos los Montos
					$query_amounts = $this->db->query("SELECT a.idamount as idamount, a.value as value, a.sum as sum FROM amount a WHERE a.status = 1 ORDER BY a.order ASC");
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => array(
							'group' => $query_group->row(),
							'complaints' => $complaints,
							'categories' => $query_categories->result(),
							'amounts' => $query_amounts->result(),
							'types' => array(
								[
									'idtype' => 0,
									'name' => 'Nuevas'
								],
								[
									'idtype' => 1,
									'name' => 'Válidas'
								],
								[
									'idtype' => 2,
									'name' => 'Agrupadas'
								],
								[
									'idtype' => 3,
									'name' => 'Resultas'
								],
								[
									'idtype' => 4,
									'name' => 'Desechadas'
								],
								[
									'idtype' => 5,
									'name' => 'Spam'
								],
								[
									'idtype' => 6,
									'name' => 'Enviadas'
								],
								[
									'idtype' => 7,
									'name' => 'En proceso'
								]
							)
						)
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay denuncias creadas.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getComplaintsResolvedGroupDependency
			if ($msg == 'getComplaintsResolvedGroupDependency' && in_array('getComplaintsResolvedGroupDependency', $calls))
			{
				//Leemos las variables
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				
				//Consultamos
				$sql = "SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status, gc.idgroup as idgroup FROM group_dependency gd INNER JOIN group_complaint gc ON gc.idgroup = gd.idgroup INNER JOIN complaint c ON c.idcomplaint = gc.idcomplaint INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount INNER JOIN user_dependency ud ON ud.iddependency = gd.iddependency WHERE ud.iduser = " . $iduser . " AND c.status IN (3,6,7) ORDER BY c.idcomplaint ASC";
				$query = $this->db->query($sql);
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$complaints = array();
					$complaints_user = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$complaints[] = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							'media' => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'idgroup' => $row->idgroup,
							'date' => $row->date,
							'status' => $row->status
						);
					}
					
					//Leemos las Categorias
					$query_categories = $this->db->query("SELECT c.idcategory as idcategory, c.name as name, c.description as description, c.icon as icon FROM category c ORDER BY c.order ASC");
	
					//Leemos los Montos
					$query_amounts = $this->db->query("SELECT a.idamount as idamount, a.value as value, a.sum as sum FROM amount a WHERE a.status = 1 ORDER BY a.order ASC");
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => array(
							'complaints' => $complaints,
							'categories' => $query_categories->result(),
							'amounts' => $query_amounts->result(),
							'types' => array(
								[
									'idtype' => 0,
									'name' => 'Nuevas'
								],
								[
									'idtype' => 1,
									'name' => 'Válidas'
								],
								[
									'idtype' => 2,
									'name' => 'Agrupadas'
								],
								[
									'idtype' => 3,
									'name' => 'Resultas'
								],
								[
									'idtype' => 4,
									'name' => 'Desechadas'
								],
								[
									'idtype' => 5,
									'name' => 'Spam'
								],
								[
									'idtype' => 6,
									'name' => 'Enviadas'
								],
								[
									'idtype' => 7,
									'name' => 'En proceso'
								]
							)
						)
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay denuncias creadas.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getNewComplaints
			if ($msg == 'getNewComplaints' && in_array('getNewComplaints', $calls))
			{
				//Consultamos
				$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 0 ORDER BY c.idcomplaint ASC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$complaints = array();
					$complaints_user = array();
					
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$complaints[] = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							'media' => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'date' => $row->date,
							'status' => $row->status
						);
					}
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $complaints
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay denuncias nuevas.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getComplaintsUser
			if ($msg == 'getComplaintsUser' && in_array('getComplaintsUser', $calls))
			{
				//Leer Valores
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				
				//Verificamos
				if ($iduser)
				{
					//Consultamos
					$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status IN (0,1) AND c.iduser = ".$iduser." ORDER BY c.idcomplaint DESC");
		
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Declaramos el Arreglo de Admins
						$complaints = array();
		
						//Procesamos los Admins
						foreach ($query->result() as $row)
						{
							//Generamos el Elemento de Area
							$complaints[] = array(
								'idcomplaint' => $row->idcomplaint,
								'title' => $row->title,
								'description' => $row->description,
								'lat' => $row->lat,
								'lng' => $row->lng,
								'address' => $row->address,
								'type' => $row->type,
								'media' => $row->media,
								'idcategory' => $row->idcategory,
								'category' => $row->category,
								'category_description' => $row->category_description,
								'idamount' => $row->idamount,
								'amount' => $row->amount,
								'icon' => $row->icon,
								'iduser' => $row->iduser,
								'date' => $row->date,
								'status' => $row->status
							);
						}
		
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $complaints
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No hay denuncias creadas.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan Campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// changeStatusComplaint
			if ($msg == 'changeStatusComplaint' && in_array('changeStatusComplaint', $calls))
			{
				//Leer los Valores
				$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
				$status = (isset($fields['status'])) ? (string)trim($fields['status']) : '';
				$substatus = (isset($fields['substatus'])) ? (string)trim($fields['substatus']) : '';
				$public_title = (isset($fields['public_title'])) ? (string)trim($fields['public_title']) : '';
				$public_description = (isset($fields['public_description'])) ? (string)trim($fields['public_description']) : '';
				$icon = 1;
								
				//Verificamos
				if ($idcomplaint)
				{
					//Consultamos
					$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.public_title as public_title, c.public_description as public_description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomplaint ASC");
		
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Leemos los datos de la denuncia
						$row = $query->row();
						
						//Asignamos el Icono de la denuncia
						switch ($status) {
							case '0': $icon = '1'; $substatus = '0'; break;
							case '1': $icon = '1'; $substatus = '0'; break;
							case '2': $icon = '2'; $substatus = '0'; break;
							case '3': $icon = '3'; break;
							case '4': $icon = '1'; $substatus = '0'; break;
							case '5': $icon = '1'; $substatus = '0'; break;
							case '6': $icon = '2'; $substatus = '0'; break;
							case '7': $icon = '3'; break;
							case '8': $icon = '1'; $substatus = '0'; break;
							case '9': $icon = '1'; $substatus = '0'; break;
							default: $icon = '1'; break;
						}				
						
						//Actualizamos la denuncia
						$data = array(
							'icon' => $icon,
							'status' => $status,
							'substatus' => $substatus,
							'public_title' => $public_title,
							'public_description' => $public_description
						);
						$this->db->where('idcomplaint', $idcomplaint);
						$this->db->update('complaint', $data);
						
						//Add Notification - Si la denuncia es aprobada
						if ((string)$status == '1')
						{
							//Definimos el Título de la Notificación
							$title_notification = 'Tu denuncia ha sido aprobada';
							
							//Registramos la Notificación
							$data = array(
								'iduser' => $row->iduser,
								'title' => $title_notification,
								'message' => 'La denuncia "' . strtoupper($row->title) . '" ha sido aprobada, haz click para ver más detalles',
								'source' => 'Admin',
								'view' => 'ViewComplaint',
								'identifier' => $idcomplaint,
								'read' => 0,
								'createdAt' => date('Y-m-d H:i:s'),
								'status' => 1
							);
							$this->db->insert('notification', $data);
							$idnotification = $this->db->insert_id();
							
							//Consultamos los dispositivos que tenga registrados el usuario
							$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $row->iduser . " AND d.status = 1");
							
							//Verificamos si el usuario tiene registrados dispositivos
							if ($query_devices->num_rows() > 0)
							{
								//Obtenemos las Credenciales
								$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
								
								//Creamos el cliente de SNS
								$SnsClient = new SnsClient([
									'version' => 'latest',
									'region' => 'us-west-2',
									'credentials' => $credentials
						        ]);
						        
								//Procesamos el mensaje a los dispositivos
								foreach ($query_devices->result() as $row_device)
								{
									try {
										$result = $SnsClient->publish([
										    'Message' => $title_notification,
										    'TargetArn' => $row_device->endpoint
										]);
									} catch (SnsException $e) { }
								}
							}
						}
						
						//Verificamos si la denuncia fue resuelta (status = 3)
						if ($status == '3')
						{
							//Consulta los grupos donde pertenece la denuncia
							$query_groups_complaint = $this->db->query("SELECT * FROM `group` g INNER JOIN group_complaint gc ON gc.idgroup = g.idgroup INNER JOIN complaint c ON c.idcomplaint = gc.idcomplaint WHERE gc.idcomplaint = " . $idcomplaint ." AND g.status = 1");
							
							//Procesamos los grupos
							foreach ($query_groups_complaint->result() as $row_group)
							{
								//Inicializamos la bandera
								$finish_group = true;
								
								//Consultamos las denuncias del grupo
								$query_complaints = $this->db->query("SELECT c.* FROM complaint c INNER JOIN group_complaint gc ON gc.idcomplaint = c.idcomplaint WHERE gc.idgroup = " . $row_group->idgroup);
								
								//Procesamos
								foreach ($query_complaints->result() as $row_complaint)
								{
									//Checamos si el status de la denuncia es diferente a 3
									if ((string)$row_complaint->status != '3') { $finish_group = false; }
								}
								
								//Verificamos si el grupo esta terminado
								if ($finish_group)
								{
									//Actualizamos el Grupo a Status 2 = Terminado
									$data = array(
										'status' => 2
									);
									$this->db->where('idgroup', $row_group->idgroup);
									$this->db->update('group', $data);
								}
							}
						}
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success'
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe la denuncia.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan Campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getComplaint
			if ($msg == 'getComplaint' && in_array('getComplaint', $calls))
			{
				//Leer los Valores
				$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
				
				//Verificamos
				if ($idcomplaint)
				{
					//Consultamos
					$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.public_title as public_title, c.public_description as public_description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows, c.status as status, c.substatus as substatus FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomplaint ASC");
		
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Declaramos el Arreglo de Admins
						$row = $query->row();
						$comments = array();
						$groups = array();
						
						//Consultamos los Comentarios
						$query_comments = $this->db->query("SELECT c.idcomment_complaint as idcomment_complaint, c.text as text, c.idamount as idamount, amount.value as amount, c.iduser as iduser, c.createdAt as date FROM comment_complaint c INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 AND c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomment_complaint DESC");
		
						//Consultamos los Grupos
						$query_groups = $this->db->query("SELECT g.idgroup as idgroup, g.title as title, g.description as description, g.level as level, g.send as send FROM group_complaint gc INNER JOIN complaint c ON gc.idcomplaint = c.idcomplaint INNER JOIN `group` g ON gc.idgroup = g.idgroup WHERE gc.idcomplaint = " . $idcomplaint . " ORDER BY gc.idcomplaint ASC");
						
						//Consultamos los Archivos
						$query_files = $this->db->query("SELECT * FROM file WHERE idcomplaint = " . $idcomplaint . " ORDER BY createdAt ASC LIMIT 10");
						
						//Consultamos los Mensajes
						$query_messages = $this->db->query("SELECT m.idmessage, m.message, m.owner, m.read, m.createdAt FROM message m WHERE m.idcomplaint = " . $idcomplaint . " ORDER BY m.idmessage ASC");
						
						//Declaramos el Arreglo de Mensajes
						$messages = array();
						
						//Consultamos las Notificaciones
						$query_notifications = $this->db->query("SELECT DISTINCT(n.title), n.message, n.source, n.view, n.identifier, n.read, n.createdAt FROM notification n WHERE n.view = 'ViewComplaint' AND n.identifier = " . $idcomplaint . " ORDER BY n.idnotification DESC LIMIT 10");
						
						//Declaramos el Arreglo de Notificaciones
						$notifications = array();
						
						//Procesamos los Mensajes
						if ($query_messages->num_rows() > 0)
						{
							//Procesamos los Mensajes
							foreach ($query_messages->result() as $row_message)
							{
								//Read Message or Form
								$message_json = json_decode($row_message->message);
								$message = $row_message->message;
								$type = 'message';
								$json_source = '';
								
								//Check Message or Form
								if ($message_json)
								{ 
									//Assign Variables
									$type = 'form'; 
									$html_json = '';
									$json_source = $message_json;
									$values = true;
									
									//Build HTML
									$html_json.= '<form class="list" id="form_{{complaint.idcomplaint}}_{{item.idmessage}}">';
									
									//Create HTML Form
									foreach ($message_json as $row_json)
									{
										if ((string)trim($row_json->value))
										{
											$values = false;
										}
									}
									
									//Check Form Response
									if ($values)
									{
										//Create HTML Form
										foreach ($message_json as $row_json)
										{
											$html_json.= '	<label class="item item-input item-stacked-label">';
										    $html_json.= '		<span class="input-label avenir-book white damask-text">'.$row_json->name.'</span>';
											$html_json.= '		<input type="text" id="field_'.$row_json->id.'" name="field_'.$row_message->idmessage.'['.$row_json->id.']" value="'.$row_json->value.'" placeholder="Escribe tu respuesta">';
											$html_json.= '	</label>';
										}
										
										//Build HTML
										$html_json.= '	<button class="button button-block button-positive acapulco avenir-book btn300" ng-click="updateForm(complaint,item)">';
										$html_json.= '		Enviar Formulario';
										$html_json.= '	</button>';
									}
									else
									{
										//Create HTML Form
										foreach ($message_json as $row_json)
										{
											$html_json.= '	<label class="item item-input item-stacked-label">';
										    $html_json.= '		<span class="input-label avenir-book white damask-text">'.$row_json->name.'</span>';
											$html_json.= '		<input type="text" readonly id="field_'.$row_json->id.'" name="field_'.$row_message->idmessage.'['.$row_json->id.']" value="'.$row_json->value.'" placeholder="Escribe tu respuesta">';
											$html_json.= '	</label>';
										}
									}
									
									$html_json.= '</form>';
									
									//Assign Message
									$message = $html_json;
								}
								
								//Generamos el Elemento de Mensaje
								$messages[] = array(
									'idmessage' => $row_message->idmessage,
									'message' => $message,
									'type' => $type,
									'owner' => $row_message->owner,
									'read' => $row_message->read,
									'json' => $json_source,
									'createdAt' => $row_message->createdAt,
									'timeago' => $this->get_timeago($row_message->createdAt)
								);
							}
						}
						
						//Procesamos las Notificaciones
						if ($query_notifications->num_rows() > 0)
						{
							//Procesamos las Notificaciones
							foreach ($query_notifications->result() as $row_notification)
							{
								//Generamos el Elemento de Notificacion
								$notifications[] = array(
									//'idnotification' => $row_notification->idnotification,
									'title' => $row_notification->title,
									'message' => $row_notification->message,
									'source' => $row_notification->source,
									'view' => $row_notification->view,
									'identifier' => $row_notification->identifier,
									'read' => $row_notification->read,
									'createdAt' => $row_notification->createdAt,
									'timeago' => $this->get_timeago($row_notification->createdAt)
								);
							}
						}
						
						//Procesamos los Grupos
						foreach ($query_groups->result() as $group)
						{
							$query_dependencies = $this->db->query("SELECT d.iddependency as iddependency, d.name as name FROM group_dependency gd INNER JOIN dependency d ON gd.iddependency = d.iddependency WHERE gd.idgroup = " . $group->idgroup);
							
							$groups[] = array(
								'idgroup' => $group->idgroup,
								'title' => $group->title,
								'description' => $group->description,
								'level' => $group->level,
								'send' => $group->send,
								'dependencies' => $query_dependencies->result(),
								'updates' => array()
							);
						}
						
						//Generamos el Elemento de Area
						$complaint = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'public_title' => $row->public_title,
							'public_description' => $row->public_description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							$row->type => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'date' => $row->date,
							'status' => $row->status,
							'substatus' => $row->substatus,
							'comments' => $row->comments,
							'follows' => $row->follows,
							'comments_data' => $query_comments->result(),
							'groups_data' => $groups,
							'files_data' => $query_files->result(),
							'notifications_data' => $notifications,
							'messages_data' => $messages
						);
		
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $complaint
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe la denuncia.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan Campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getComplaint
			if ($msg == 'getComplaintAmounts' && in_array('getComplaintAmounts', $calls))
			{
				//Leer los Valores
				$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
				
				//Verificamos
				if ($idcomplaint)
				{
					//Consultamos
					$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 AND c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomplaint ASC");
		
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Declaramos el Arreglo de Admins
						$row = $query->row();
						$comments = array();
						
						//Consultamos los Comentarios
						$query_comments = $this->db->query("SELECT c.idcomment_complaint as idcomment_complaint, c.text as text, c.idamount as idamount, amount.value as amount, c.iduser as iduser, c.createdAt as date FROM comment_complaint c INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 AND c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomment_complaint DESC");
		
						//Consultamos
						$query_amounts = $this->db->query("SELECT * FROM amount WHERE amount.status = 1 ORDER BY amount.order ASC");
						
						//Declaramos el Arreglo de Admins
						$amounts = array();
		
						//Procesamos los Admins
						foreach ($query_amounts->result() as $row_amount)
						{
							//Generamos el Elemento de Area
							$amounts[] = array(
								'idamount' => $row_amount->idamount,
								'value' => $row_amount->value,
								'order' => $row_amount->order
							);
						}
						
						//Generamos el Elemento de Area
						$complaint = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							$row->type => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'date' => $row->date,
							'comments' => $row->comments,
							'follows' => $row->follows,
							'comments_data' => $query_comments->result(),
							'amounts' => $amounts
						);
		
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $complaint
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe la denuncia.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan Campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// followComplaint
			if ($msg == 'followComplaint' && in_array('followComplaint', $calls))
			{
				//Leemos los Datos
				$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				
				//Verificamos
				if ($idcomplaint && $iduser)
				{
					//Consultamos la Denuncia
					$query = $this->db->query("SELECT * FROM complaint WHERE idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
					
					//Verificamos
					if ($query->num_rows() > 0)
					{
						//Leemos el Objeto
						$row = $query->row();
						
						//Verificamos si el usuario no es el dueño
						if ($row->iduser != $iduser)
						{
							//Consultamos si el usuario es valido
							$query_user = $this->db->query("SELECT * FROM user WHERE iduser = ".$iduser." AND status = 1 LIMIT 1");
							
							//Verificamos si el usuario existe
							if ($query_user->num_rows() > 0)
							{
								//Leemos el Usuario que se unió
								$row_user = $query_user->row();
								
								//Consultamos la relación de la denuncia con el usuario
								$query_follow = $this->db->query("SELECT * FROM follow_complaint WHERE iduser = ".$iduser." AND idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
								
								//Verificamos que no exista la relacion
								if ($query_follow->num_rows() == 0)
								{
									//Generamos el Registro de Follow
									$data = array(
										'iduser' => $iduser,
										'idcomplaint' => $idcomplaint,
										'createdAt' => date('Y-m-d H:i:s'),
										'status' => 1
									);
									$this->db->insert('follow_complaint', $data);
									$idfollow_complaint = $this->db->insert_id();
									
									//Consultamos la denuncia
									$query_complaint = $this->db->query("SELECT * FROM complaint WHERE idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
									$row_complaint = $query_complaint->row();
									
									//Add Notification - Avisar al OWNER
									$data = array(
										'iduser' => $row->iduser,
										'title' => 'Un nuevo usuario sigue a tu denuncia',
										'message' => 'Un nuevo usuario esta siguiendo tu denuncia ' . strtoupper($row_complaint->title) . ', haz click para ver más detalles',
										'source' => 'Incorruptible',
										'view' => 'ViewComplaint',
										'identifier' => $idcomplaint,
										'read' => 0,
										'createdAt' => date('Y-m-d H:i:s'),
										'status' => 1
									);
									$this->db->insert('notification', $data);
									$idnotification = $this->db->insert_id();
									
									//Consultamos los dispositivos que tenga registrados el usuario
									$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $row->iduser . " AND d.status = 1");
									
									//Verificamos si el usuario tiene registrados dispositivos
									if ($query_devices->num_rows() > 0)
									{
										//Obtenemos las Credenciales
										$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
										
										//Creamos el cliente de SNS
										$SnsClient = new SnsClient([
											'version' => 'latest',
											'region' => 'us-west-2',
											'credentials' => $credentials
								        ]);
								        
										//Procesamos el mensaje a los dispositivos
										foreach ($query_devices->result() as $row_device)
										{
											try {
												$result = $SnsClient->publish([
												    'Message' => $title,
												    'TargetArn' => $row_device->endpoint
												]);
											} catch (SnsException $e) { }
										}
									}
									
									//Generamos el Arreglo
									$array = array(
										'status' => (int)1,
										'msg' => 'success'
									);
				
									//Imprimimos el Arreglo
									$this->printJSON($array);
									$output = TRUE;
								}
								else
								{
									//Mostrar Error
									$array = array(
										'status' => (int)0,
										'msg' => (string)'Tu ya sigues a esta denuncia.'
									);
					
									//Imprimimos el Arreglo
									$this->printJSON($array);
									$output = TRUE;
								}
							}
							else
							{
								//Mostrar Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'No existe el usuario o esta deshabilitado.'
								);
				
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Mostrar Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'Tu eres el propietario de la denuncia.',
								'data' => array()
							);
			
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe la denuncia o esta deshabilitada.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan Campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// loginFacebook
			if ($msg == 'loginFacebook' && in_array('loginFacebook', $calls))
			{
				//Read Values
				$token = (string)trim($fields['token']) ? (string)trim($fields['token']) : '';
				
				//Verificamos
				if ($token)
				{
					//Obtenemos la información
					$usuario = $this->facebook->responseToken($token);
					
					//Verificamos
					if (isset($usuario['id']))
					{
						//Consultamos los datos
						$query = $this->db->query("SELECT * FROM user WHERE email = '" . $usuario['email'] . "' LIMIT 1");
		
						//Verificamos si existe el usuario
						if ($query->num_rows() > 0)
						{
							//Leemos el Objeto
							$row = $query->row();
							
							//Consultamos el Perfil del Usuario
							$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row->idprofile . " AND status = 1");
							$row_profile = $query_profile->row();
							
							//Consultamos los Estados
							$query_states = $this->db->query("SELECT * FROM state WHERE idstate = " . $row->idstate);
							$row_state = $query_states->row();
							
							//Consultamos las Denuncias del Usuario
							$query_complaints = $this->db->query("SELECT * FROM complaint WHERE iduser = " . $row->iduser . " AND status = 1");
							
							//Consultamos las Denuncias Seguidas del Usuario
							$query_follow_complaints = $this->db->query("SELECT * FROM follow_complaint WHERE iduser = " . $row->iduser . " AND status = 1");
							
							//Actualizamos los datos
							$data = array(
								'fbid' => $usuario['id'],
								'fbtoken' => $usuario['token'],
								'avatar' => $usuario['avatar'],
								'updatedAt' => date('Y-m-d H:i:s')
							);
							$this->db->where('iduser', $row->iduser);
							$this->db->update('user', $data);
							
							//Consultamos
							$data = array(
								'iduser' => $row->iduser,
								'name' => $row->name,
							    'email' => $row->email,
							    'avatar' => $usuario['avatar'],
							    'fbid' => $usuario['id'],
							    'twid' => $row->twid,
							    'complaints' => $query_complaints->num_rows(),
							    'follows' => $query_follow_complaints->num_rows(),
							    'idprofile' => $row_profile->idprofile,
							    'profile' => $row_profile->name,
							    'mailing' => $row->mailing,
							    'notifications' => $row->notifications,
							    'idstate' => $row_state->idstate,
							    'state' => $row_state->name,
							    'position' => array(
								    'lat' => $row_state->lat,
								    'lng' => $row_state->lng
							    ),
							    'status' => $row->status
							);
							
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'success',
								'data' => $data
							);
		
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
						else
						{
							//Guardamos los Datos
							$data = array(
								'email' => $usuario['email'],
								'password' => password_hash($usuario['id'], PASSWORD_DEFAULT, ['cost' => 12]),
							    'name' => $usuario['first_name'].' '.$usuario['last_name'],
							    'fbid' => $usuario['id'],
								'fbtoken' => $usuario['token'],
								'avatar' => $usuario['avatar'],
							    'createdAt' => date('Y-m-d H:i:s'),
							    'idprofile' => 1,
							    'mailing' => 1,
							    'notifications' => 0,
							    'status' => 0
							);
							$this->db->insert('user', $data);
							$iduser = $this->db->insert_id();
							
							//Consultamos el Perfil del Usuario
							$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = 1 AND status = 1");
							$row_profile = $query_profile->row();
							
							//Consultamos los Estados
							$query_states = $this->db->query("SELECT * FROM state WHERE idstate = " . $row->idstate);
							$row_state = $query_states->row();
							
							$data = array(
								'iduser' => $iduser,
								'email' => $usuario['email'],
								'name' => $usuario['first_name'].' '.$usuario['last_name'],
								'link' => 'https://app.incorruptible.mx'
							);
							
							$data_result = array(
								'iduser' => $iduser,
								'name' => $usuario['first_name'].' '.$usuario['last_name'],
								'email' => $usuario['email'],
								'avatar' => $usuario['avatar'],
								'fbid' => $usuario['id'],
								'twid' => '',
								'complaints' => '0',
							    'follows' => '0',
							    'idprofile' => $row_profile->idprofile,
							    'profile' => $row_profile->name,
							    'mailing' => 1,
							    'notifications' => 0,
							    'idstate' => $row_state->idstate,
							    'state' => $row_state->name,
							    'position' => array(
								    'lat' => $row_state->lat,
								    'lng' => $row_state->lng
							    ),
							    'status' => 0
							);
		
							//Correo de Notificación
							$subject = 'Bienvenido a Incorruptible';
							$body = $this->load->view('mail/welcome_user', $data, TRUE);
							$altbody = strip_tags($body);
		
							//Verificamos
							if ($subject != '' && $body != '' && $altbody != '' && $email != '')
							{
								//Mandamos el Correo
								$mail = new PHPMailer(true);
								$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
								$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
								$mail->CharSet = 'UTF-8';
								$mail->Encoding = "quoted-printable";
						        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
						        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
						        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
						        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
						        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
						        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
						        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
						        $mail->SMTPDebug  = 0;
						        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
						        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
						        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
						        $mail->Body      = $body;
						        $mail->AltBody    = $altbody;
						        $mail->AddAddress($email, $name);
						        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
		
						        if ($mail->Send())
								{
									$response = true;
								}
								else
								{
									$response = false;
								}
							}
		
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'success',
								'data' => $data_result
							);
		
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Ocurrió un error al procesar la información.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan Campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// joinComplaint
			if ($msg == 'joinComplaint' && in_array('joinComplaint', $calls))
			{
				//Leemos los datos
				$text = (isset($fields['text'])) ? (string)trim($fields['text']) : '';
				$idamount = (isset($fields['idamount'])) ? (string)trim($fields['idamount']) : '';
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
				
				//Verificamos los Datos
				if ($text && $idamount && $iduser && $idcomplaint)
				{
					//Verificamos si existe la denuncia
					$query = $this->db->query("SELECT * FROM complaint WHERE idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
					
					//Verificamos
					if ($query->num_rows() > 0)
					{
						//Leemos el Objeto
						$row = $query->row();
						
						//Verificamos si el usuario no es el dueño
						if ($row->iduser != $iduser)
						{
							//Consultamos si el usuario es valido
							$query_user = $this->db->query("SELECT * FROM user WHERE iduser = ".$iduser." AND status = 1 LIMIT 1");
							
							//Verificamos si el usuario existe
							if ($query_user->num_rows() > 0)
							{
								//Leemos el Usuario
								$row_user = $query_user->row();
								
								//Consultamos la relación de la denuncia con el usuario
								$query_comment = $this->db->query("SELECT * FROM comment_complaint WHERE iduser = ".$iduser." AND idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
								
								//Verificamos que no exista la relacion
								if ($query_comment->num_rows() == 0)
								{
									//Generamos el Registro de Follow
									$data = array(
										'text' => $text,
										'idamount' => $idamount,
										'iduser' => $iduser,
										'idcomplaint' => $idcomplaint,
										'createdAt' => date('Y-m-d H:i:s'),
										'status' => 1
									);
									$this->db->insert('comment_complaint', $data);
									$idcomment_complaint = $this->db->insert_id();
									
									//Consultamos la denuncia
									$query_complaint = $this->db->query("SELECT * FROM complaint WHERE idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
									$row_complaint = $query_complaint->row();
									
									//Add Notification - Avisar al OWNER
									$data = array(
										'iduser' => $row_complaint->iduser,
										'title' => 'Un nuevo usuario se ha unido a tu denuncia',
										'message' => 'Un nuevo usuario se ha unido a la denuncia ' . strtoupper($row_complaint->title) . ', haz click para ver más detalles',
										'source' => 'Incorruptible',
										'view' => 'ViewComplaint',
										'identifier' => $idcomplaint,
										'read' => 0,
										'createdAt' => date('Y-m-d H:i:s'),
										'status' => 1
									);
									$this->db->insert('notification', $data);
									$idnotification = $this->db->insert_id();
									
									//Consultamos los dispositivos que tenga registrados el usuario
									$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $row_complaint->iduser . " AND d.status = 1");
									
									//Verificamos si el usuario tiene registrados dispositivos
									if ($query_devices->num_rows() > 0)
									{
										//Obtenemos las Credenciales
										$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
										
										//Creamos el cliente de SNS
										$SnsClient = new SnsClient([
											'version' => 'latest',
											'region' => 'us-west-2',
											'credentials' => $credentials
								        ]);
								        
										//Procesamos el mensaje a los dispositivos
										foreach ($query_devices->result() as $row_device)
										{
											try {
												$result = $SnsClient->publish([
												    'Message' => $title,
												    'TargetArn' => $row_device->endpoint
												]);
											} catch (SnsException $e) { }
										}
									}
									
									//Generamos el Arreglo
									$array = array(
										'status' => (int)1,
										'msg' => 'success'
									);
				
									//Imprimimos el Arreglo
									$this->printJSON($array);
									$output = TRUE;
								}
								else
								{
									//Mostrar Error
									$array = array(
										'status' => (int)0,
										'msg' => (string)'Tu ya te uniste a esta denuncia.'
									);
					
									//Imprimimos el Arreglo
									$this->printJSON($array);
									$output = TRUE;
								}
							}
							else
							{
								//Mostrar Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'No existe el usuario o esta deshabilitado.'
								);
				
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Mostrar Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'Tu eres el propietario de la denuncia.'
							);
			
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe la denuncia o esta deshabilitada.'
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan Campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// loginTwitter
			if ($msg == 'loginTwitter' && in_array('loginTwitter', $calls))
			{
				//Read Values
				$email = (string)trim($fields['email']) ? (string)trim($fields['email']) : '';			    
				$name = (string)trim($fields['name']) ? (string)trim($fields['name']) : '';
				$twid = (string)trim($fields['twid']) ? (string)trim($fields['twid']) : '';
				$twtoken = (string)trim($fields['twtoken']) ? (string)trim($fields['twtoken']) : '';
				$avatar = (string)trim($fields['avatar']) ? (string)trim($fields['avatar']) : '';
				
				//Verificamos
				if ($email && $name && $twid && $twtoken && $avatar)
				{
					//Consultamos los datos
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");
	
					//Verificamos si existe el usuario
					if ($query->num_rows() > 0)
					{
						//Leemos el Objeto
						$row = $query->row();
						
						//Consultamos el Perfil del Usuario
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row->idprofile . " AND status = 1");
						$row_profile = $query_profile->row();
						
						//Consultamos los Estados
						$query_states = $this->db->query("SELECT * FROM state WHERE idstate = " . $row->idstate);
						$row_state = $query_states->row();
						
						//Consultamos las Denuncias del Usuario
						$query_complaints = $this->db->query("SELECT * FROM complaint WHERE iduser = " . $row->iduser . " AND status = 1");
						
						//Consultamos las Denuncias Seguidas del Usuario
						$query_follow_complaints = $this->db->query("SELECT * FROM follow_complaint WHERE iduser = " . $row->iduser . " AND status = 1");
						
						//Actualizamos los datos
						$data = array(
							'name' => $name,
							'twid' => $twid,
							'twtoken' => $twtoken,
							'avatar' => $avatar,
							'updatedAt' => date('Y-m-d H:i:s')
						);
						$this->db->where('iduser', $row->iduser);
						$this->db->update('user', $data);
						
						//Consultamos
						$data = array(
							'iduser' => $row->iduser,
							'name' => $name,
						    'email' => $row->email,
						    'avatar' => $avatar,
						    'fbid' => $row->fbid,
						    'twid' => $twid,
						    'complaints' => $query_complaints->num_rows(),
						    'follows' => $query_follow_complaints->num_rows(),
						    'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name,
						    'mailing' => $row->mailing,
						    'notifications' => $row->notifications,
						    'idstate' => $row_state->idstate,
						    'state' => $row_state->name,
						    'position' => array(
							    'lat' => $row_state->lat,
							    'lng' => $row_state->lng
						    ),
						    'status' => $row->status
						);
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Guardamos los Datos
						$data = array(
							'email' => $email,
							'password' => password_hash($twid, PASSWORD_DEFAULT, ['cost' => 12]),
						    'name' => $name,
						    'twid' => $twid,
							'twtoken' => $twtoken,
							'avatar' => $avatar,
						    'createdAt' => date('Y-m-d H:i:s'),
						    'idprofile' => 1,
						    'mailing' => 1,
						    'notifications' => 0,
						    'status' => 0
						);
						$this->db->insert('user', $data);
						$iduser = $this->db->insert_id();
						
						//Consultamos el Perfil del Usuario
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = 1 AND status = 1");
						$row_profile = $query_profile->row();
						
						$data = array(
							'iduser' => $iduser,
							'email' => $email,
							'name' => $name,
							'link' => 'https://app.incorruptible.mx'
						);
						
						$data_result = array(
							'iduser' => $iduser,
							'name' => $name,
							'email' => $email,
							'avatar' => $avatar,
							'fbid' => '',
							'twid' => $twid,
							'complaints' => '0',
						    'follows' => '0',
						    'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name,
						    'mailing' => 1,
						    'notifications' => 0,
						    'idstate' => $row_state->idstate,
						    'state' => $row_state->name,
						    'position' => array(
							    'lat' => $row_state->lat,
							    'lng' => $row_state->lng
						    ),
						    'status' => 0
						);
	
						//Correo de Notificación
						$subject = 'Bienvenido a Incorruptible';
						$body = $this->load->view('mail/welcome_user', $data, TRUE);
						$altbody = strip_tags($body);
	
						//Verificamos
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Mandamos el Correo
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
					        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
					        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email, $name);
					        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
	
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data_result
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan Campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getBlog
			if ($msg == 'getBlog' && in_array('getBlog', $calls))
			{
				//Leemos el Blog
				//$feed = new SimpleXMLElement(file_get_contents('http://incorruptible.mx/feed'));
				$feed = new SimpleXMLElement(file_get_contents('http://incorruptible.mx/feed/?post_type=posts'));
				$items = array();
				foreach ($feed->channel->item as $item)
				{
					$items[] = $item;
				}
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $items
				);
	
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			
			// getNews
			if ($msg == 'getNews' && in_array('getNews', $calls))
			{
				//Leemos el Blog
				//$feed = new SimpleXMLElement(file_get_contents('http://incorruptible.mx/feed'));
				$feed = new SimpleXMLElement(file_get_contents('http://incorruptible.mx/feed/?post_type=posts'));
				$items = array();
				foreach ($feed->channel->item as $item)
				{
					$items[] = $item;
				}
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $items
				);
	
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			
			// getDependencies
			if ($msg == 'getDependencies' && in_array('getDependencies', $calls))
			{
				//Consultamos
				$query = $this->db->query("SELECT * FROM dependency WHERE dependency.status = 1 ORDER BY dependency.iddependency ASC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Dependencias
					$dependencies = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$dependencies[] = array(
							'iddependency' => $row->iddependency,
							'name' => $row->name
						);
					}
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $dependencies
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay dependencias asignadas.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getDependenciesByTerm
			if ($msg == 'getDependenciesByTerm' && in_array('getDependenciesByTerm', $calls))
			{
				//Leer Term
				$term = (isset($fields['term'])) ? (string)trim($fields['term']) : '';
				
				//Consultamos
				$query = $this->db->query("SELECT * FROM dependency WHERE dependency.status = 1 AND dependency.name LIKE '%".$term."%' ORDER BY dependency.iddependency ASC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Dependencias
					$dependencies = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$dependencies[] = array(
							'iddependency' => $row->iddependency,
							'name' => $row->name
						);
					}
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $dependencies
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay dependencias asignadas.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//saveGroupComplaints
			if ($msg == 'saveGroupComplaints' && in_array('saveGroupComplaints', $calls))
			{
				//Leemos los Valores
				$title = (isset($fields['title'])) ? (string)trim($fields['title']) : '';
				$description = (isset($fields['description'])) ? (string)trim($fields['description']) : '';
				$level = (isset($fields['level'])) ? (string)trim($fields['level']) : '';
				$dependency = (isset($fields['dependency'])) ? (string)trim($fields['dependency']) : '';
				$complaints = (isset($fields['complaints'])) ? (string)trim($fields['complaints']) : '';
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				
				//Verificamos
				if ($title && $description && $level && $dependency && $complaints && $iduser)
				{
					//Leemos las Dependencias
					$array_dependencies = explode(',', $dependency);
					
					//Creamos el Grupo
					$data = array(
						'title' => $title,
						'description' => $description,
						'level' => $level,
						'iduser' => $iduser,
						'send' => 0,
						'createdAt' => date('Y-m-d H:i:s'),
						'status' => 1
					);
					$this->db->insert('group', $data);
					$idgroup = $this->db->insert_id();
					
					//Procesamos las Denuncias
					$array_complaints = explode(',', $complaints);
					
					//Creamos la Relación de las Denuncias con el Grupo
					foreach ($array_complaints as $complaint)
					{
						//Generamos la Relación
						$data = array(
							'idgroup' => $idgroup,
							'idcomplaint' => $complaint,
							'createdAt' => date('Y-m-d H:i:s'),
							'status' => 1
						);
						$this->db->insert('group_complaint', $data);
						
						//Consultamos la denuncia
						$query_complaint = $this->db->query("SELECT * FROM complaint WHERE idcomplaint = ".$complaint." AND status = 1 LIMIT 1");
						$row_complaint = $query_complaint->row();
						
						//Add Notification - Avisar al OWNER
						$data = array(
							'iduser' => $row_complaint->iduser,
							'title' => 'Han agrupado tu denuncia',
							'message' => 'Han agrupado tu denuncia "' . strtoupper($row_complaint->title) . '" en el grupo "' . strtoupper($title) . '", haz click para ver más detalles',
							'source' => 'ONG',
							'view' => 'ViewComplaint',
							'identifier' => $complaint,
							'read' => 0,
							'createdAt' => date('Y-m-d H:i:s'),
							'status' => 1
						);
						$this->db->insert('notification', $data);
						$idnotification = $this->db->insert_id();
						
						//Consultamos los dispositivos que tenga registrados el usuario
						$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $row_complaint->iduser . " AND d.status = 1");
						
						//Verificamos si el usuario tiene registrados dispositivos
						if ($query_devices->num_rows() > 0)
						{
							//Obtenemos las Credenciales
							$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
							
							//Creamos el cliente de SNS
							$SnsClient = new SnsClient([
								'version' => 'latest',
								'region' => 'us-west-2',
								'credentials' => $credentials
					        ]);
					        
							//Procesamos el mensaje a los dispositivos
							foreach ($query_devices->result() as $row_device)
							{
								try {
									$result = $SnsClient->publish([
									    'Message' => $title,
									    'TargetArn' => $row_device->endpoint
									]);
								} catch (SnsException $e) { }
							}
						}
						
						//Actualizamos el Icono de la Denuncia
						$data = array(
							'icon' => 2
						);
						$this->db->where('idcomplaint', $complaint);
						$this->db->update('complaint', $data);
					}
					
					//Creamos la Relación del Grupo con las Dependencias
					foreach ($array_dependencies as $row)
					{
						//Generamos la Relación
						$data = array(
							'idgroup' => $idgroup,
							'iddependency' => $row,
							'createdAt' => date('Y-m-d H:i:s'),
							'status' => 1
						);
						$this->db->insert('group_dependency', $data);
					}
					
					//Mostrar Resultados
					$array = array(
						'status' => (int)1,
						'msg' => (string)'success',
						'data' => $idgroup
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//getGroupsByUser
			if ($msg == 'getGroupsByUser' && in_array('getGroupsByUser', $calls))
			{
				//Leemos las Variables
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				
				//Verificamos el Valor
				if ($iduser)
				{
					//Consultamos todos los Grupos creados por el Usuario
					$query = $this->db->query("SELECT * FROM `group` g WHERE g.status = 1 AND g.iduser = " . $iduser . " ORDER BY g.idgroup ASC");
		
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Declaramos el Arreglo de Dependencias
						$groups = array();
		
						//Procesamos los Admins
						foreach ($query->result() as $row)
						{
							//Generamos el Elemento de Grupos
							$groups[] = array(
								'idgroup' => $row->idgroup,
								'title' => $row->title,
								'description' => $row->description,
								'level' => $row->level
							);
						}
		
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $groups
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No hay grupos creados.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//getGroupsByDependency
			if ($msg == 'getGroupsByDependency' && in_array('getGroupsByDependency', $calls))
			{
				//Leemos las Variables
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				
				//Verificamos el Valor
				if ($iduser)
				{
					//Consultamos todos los Grupos creados por el Usuario
					$query = $this->db->query("SELECT g.* FROM `group` g INNER JOIN group_dependency gd ON gd.idgroup = g.idgroup INNER JOIN user_dependency ud ON ud.iddependency = gd.iddependency WHERE g.status = 1 AND ud.iduser = " . $iduser . " GROUP BY g.idgroup ORDER BY g.idgroup ASC");
		
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Declaramos el Arreglo de Dependencias
						$groups = array();
		
						//Procesamos los Admins
						foreach ($query->result() as $row)
						{
							//Generamos el Elemento de Grupos
							$groups[] = array(
								'idgroup' => $row->idgroup,
								'title' => $row->title,
								'description' => $row->description,
								'level' => $row->level
							);
						}
		
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $groups
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No hay grupos creados.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//getGroupsResolvedByDependency
			if ($msg == 'getGroupsResolvedByDependency' && in_array('getGroupsResolvedByDependency', $calls))
			{
				//Leemos las Variables
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				
				//Verificamos el Valor
				if ($iduser)
				{
					//Consultamos todos los Grupos creados por el Usuario
					$query = $this->db->query("SELECT g.* FROM `group` g INNER JOIN group_dependency gd ON gd.idgroup = g.idgroup INNER JOIN user_dependency ud ON ud.iddependency = gd.iddependency WHERE g.status = 2 AND ud.iduser = " . $iduser . " ORDER BY g.idgroup ASC");
		
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Declaramos el Arreglo de Dependencias
						$groups = array();
		
						//Procesamos los Admins
						foreach ($query->result() as $row)
						{
							//Generamos el Elemento de Grupos
							$groups[] = array(
								'idgroup' => $row->idgroup,
								'title' => $row->title,
								'description' => $row->description,
								'level' => $row->level
							);
						}
		
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $groups
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No hay grupos creados.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//getGroup
			if ($msg == 'getGroup' && in_array('getGroup', $calls))
			{
				//Leemos las Variables
				$idgroup = (isset($fields['idgroup'])) ? (string)trim($fields['idgroup']) : '';
				
				//Verificamos el Valor
				if ($idgroup)
				{
					//Consultamos todos los Grupos creados por el Usuario
					$query = $this->db->query("SELECT * FROM `group` g WHERE g.status = 1 AND g.idgroup = " . $idgroup);
					
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Procesamos los Admins
						foreach ($query->result() as $row) { }
						
						//Consultamos las Denuncias que pertenecen al grupo
						$query_complaints = $this->db->query("SELECT g.idgroup_complaint, g.idgroup, c.idcomplaint, c.title, c.description, c.lat, c.lng, c.address, c.type, c.media, c.idcategory, c.idamount, c.icon, c.iduser, (SELECT a.sum FROM amount a WHERE a.idamount = c.idamount AND a.status = 1) as sum FROM group_complaint g INNER JOIN complaint c ON g.idcomplaint = c.idcomplaint WHERE g.status = 1 AND g.idgroup = " . $idgroup);
						$query_dependencies = $this->db->query("SELECT d.iddependency as iddependency, d.name as name FROM group_dependency gd INNER JOIN dependency d ON gd.iddependency = d.iddependency WHERE gd.idgroup = " . $idgroup);
						$complaints = array();
						$sum = 0;
						$contador = 0;
						$center = '';
						$markers = '';
						$lats = 0;
						$longs = 0;
						
						//Procesamos
						foreach ($query_complaints->result() as $complaint)
						{
							$sum = $sum + floatval($complaint->sum);
							$contador++;
							$markers.= '&markers=icon:http://app.incorruptible.mx/img/yellow.png%7C'.$complaint->lat.','.$complaint->lng;
							
							//if ($contador == 1) { $center = (string)$complaint->lat . ',' . (string)$complaint->lng; }
							$lats = $lats + $complaint->lat;
							$longs = $longs + $complaint->lng;
							
							$complaints[] = array(
								'idgroup_complaint' => $complaint->idgroup_complaint,
								'idgroup' => $complaint->idgroup,
								'idcomplaint' => $complaint->idcomplaint,
								'title' => $complaint->title,
								'description' => $complaint->description,
								'lat' => $complaint->lat,
								'lng' => $complaint->lng,
								'address' => $complaint->address,
								'type' => $complaint->type,
								'media' => $complaint->media,
								'idcategory' => $complaint->idcategory,
								'idamount' => $complaint->idamount,
								'icon' => $complaint->icon,
								'iduser' => $complaint->iduser,
								'amount' => $complaint->sum	
							);
						}
						
						$center = (string)($lats/$contador) . ',' . (string)($longs/$contador);
						
						//Creamos el Map
						$map = 'https://maps.googleapis.com/maps/api/staticmap';
						$map.= '?center=' . $center;
						$map.= '&zoom=12';
						$map.= '&scale=2';
						$map.= '&size=960x480';
						$map.= '&maptype=roadmap';
						$map.= $markers;
						$map.= '&key=AIzaSyCdONyfCadeDvw20F1Q03eJj80WnGmCZ4g';
						$map.= '&format=jpg';
						$map.= '&visual_refresh=true';
						
						//Generamos el Elemento de Grupos
						$group = array(
							'idgroup' => $row->idgroup,
							'title' => $row->title,
							'description' => $row->description,
							'level' => $row->level,
							'iduser' => $row->iduser,
							'send' => $row->send,
							'complaints' => $complaints,
							'dependencies' => $query_dependencies->result(),
							'map' => $map,
							'sum' => $sum
						);
		
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $group
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe el grupo.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//sendGroup
			if ($msg == 'sendGroup' && in_array('sendGroup', $calls))
			{
				//Leemos las Variables
				$idgroup = (isset($fields['idgroup'])) ? (string)trim($fields['idgroup']) : '';
				
				//Verificamos el Valor
				if ($idgroup)
				{
					//Consultamos todos los Grupos creados por el Usuario
					$query = $this->db->query("SELECT g.idgroup, g.title, g.description, g.level, (SELECT iddependency FROM dependency d WHERE d.iddependency = gd.iddependency) as iddependency, (SELECT name FROM dependency d WHERE d.iddependency = gd.iddependency) as dependency, g.iduser, (SELECT email FROM user u WHERE u.iduser = g.iduser) as email, (SELECT name FROM user u WHERE u.iduser = g.iduser) as name, g.send FROM `group` g INNER JOIN group_dependency gd ON gd.idgroup = g.idgroup WHERE g.status = 1 AND g.idgroup = " . $idgroup);
					
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Leemos el objeto
						foreach ($query->result() as $row) { }
						
						//Actualizamos el Grupo
						$data = array(
							'send' => 1
						);
						$this->db->where('idgroup', $row->idgroup);
						$this->db->update('group', $data);
						
						//Consultamos las Denuncias que pertenecen al grupo
						$query_complaints = $this->db->query("SELECT g.idgroup_complaint, g.idgroup, c.idcomplaint, c.title, c.description, c.lat, c.lng, c.address, c.type, c.media, c.idcategory, c.idamount, c.icon, c.iduser, (SELECT a.sum FROM amount a WHERE a.idamount = c.idamount AND a.status = 1) as sum FROM group_complaint g INNER JOIN complaint c ON g.idcomplaint = c.idcomplaint WHERE g.status = 1 AND g.idgroup = " . $idgroup);
						$complaints = array();
						
						//Procesamos
						foreach ($query_complaints->result() as $complaint)
						{
							$complaints[] = array(
								'idgroup_complaint' => $complaint->idgroup_complaint,
								'idgroup' => $complaint->idgroup,
								'idcomplaint' => $complaint->idcomplaint,
								'title' => $complaint->title,
								'description' => $complaint->description,
								'lat' => $complaint->lat,
								'lng' => $complaint->lng,
								'address' => $complaint->address,
								'type' => $complaint->type,
								'media' => $complaint->media,
								'idcategory' => $complaint->idcategory,
								'idamount' => $complaint->idamount,
								'icon' => $complaint->icon,
								'iduser' => $complaint->iduser,
								'amount' => $complaint->sum	
							);
						}
						
						//Enviamos la Notificación
						$data = array(
							'idgroup' => $row->idgroup,
							'title' => $row->title,
							'description' => $row->description,
							'level' => $row->level,
							'iddependency' => $row->iddependency,
							'dependency' => $row->dependency,
							'iduser' => $row->iduser,
							'email' => $row->email,
							'name' => $row->name,
							'send' => $row->send,
							'complaints' => $complaints
						);
						
						//Consultamos si la dependencia tiene una usuario designado como Autoridad
						$query_dependency = $this->db->query("SELECT u.email, u.name FROM user_dependency ud INNER JOIN user u ON ud.iduser = u.iduser WHERE iddependency = " . $row->iddependency);
						
						//Verificamos si hay una Autoridad asignada
						if ($query_dependency->num_rows() > 0)
						{
							//Leemos el objeto
							$row_dependency = $query_dependency->row();
							
							//Asignamos los Valores
							$email = $row_dependency->email;
							$name = $row_dependency->name;	
						}
						else
						{
							//Asignamos los valores por default
							$email = 'app.incorruptible@borde.mx';
							$name = 'Incorruptible';
						}
	
						//Correo de Notificación
						$subject = 'Grupo "'.$row->title.'" enviado desde Incorruptible';
						$body = $this->load->view('mail/send_group', $data, TRUE);
						$altbody = strip_tags($body);
	
						//Verificamos
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Mandamos el Correo
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
					        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
					        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email, $name);
					        //$mail->AddBCC('gerardo@nodo.pw', 'Gerardo Vidal');
	
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}	
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe el grupo.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//deleteComplaintFromGroup
			if ($msg == 'deleteComplaintFromGroup' && in_array('deleteComplaintFromGroup', $calls))
			{
				//Leemos los datos
				$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '0';
				$idgroup = (isset($fields['idgroup'])) ? (string)trim($fields['idgroup']) : '0';
				$group_title = (isset($fields['title'])) ? (string)trim($fields['title']) : '';
				$idprofile = (isset($fields['idprofile'])) ? (string)trim($fields['idprofile']) : '0';
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '0';
				
				//Verificamos los datos
				if ($idcomplaint && $idgroup && $idprofile && $iduser)
				{
					//Consultamos si el Grupo es valido y activo
					$query_group = $this->db->query("SELECT * FROM `group` WHERE status = 1 AND idgroup = " . $idgroup);
					
					//Verificamos
					if ($query_group->num_rows() > 0)
					{
						//Consultamos si la denuncia pertenece al grupo
						$query_complaint = $this->db->query("SELECT * FROM group_complaint gc WHERE gc.status = 1 AND gc.idgroup = " . $idgroup . " AND gc.idcomplaint = " . $idcomplaint . " LIMIT 1");
						
						//Verificamos 
						if ($query_complaint->num_rows() > 0)
						{
							//Leemos el Objeto
							$group_complaint_row = $query_complaint->row();
							
							//Desactivamos la Denuncia del Grupo
							$data = array(
								'status' => 0
							);
							$this->db->where('idgroup_complaint', $group_complaint_row->idgroup_complaint);
							$this->db->update('group_complaint', $data);
							
							//Mandamos la notificación al Usuario
							$title = 'Tu denuncia ha sido eliminada del grupo';
							$message = 'Tu denuncia ya no pertenece al grupo de "'. $group_title .'"';
							$source = $idprofile;
							$view = 'ViewComplaint';
							$identifier = $idcomplaint;
							$SNS_APP_ARN = '';
							$users = array();
							if ($source == '2') { $source = 'ONG'; }
							if ($source == '3') { $source = 'AUTORIDAD'; }
							
							//Verificamos los campos
							if ($title && $message && $identifier)
							{
								//Consultamos la Denuncia
								$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.idcomplaint = " . $identifier . " ORDER BY c.idcomplaint ASC");
				
								//Verificamos si existe el Admin
								if ($query->num_rows() > 0)
								{
									//Leemos los datos del usuario
									$row = $query->row();
									$users[] = $row->iduser;
									
									//Consultamos los usuarios que siguen la denuncia
									$query_follows = $this->db->query("SELECT fc.* FROM follow_complaint fc WHERE fc.idcomplaint = " . $row->idcomplaint . " AND fc.status = 1");
									
									//Procesamos los usuarios que siguen la denuncia
									foreach ($query_follows->result() as $row_follow)
									{
										$users[] = $row_follow->iduser;
									}
									
									//Procesamos las notificaciones para todos los usuarios unidos a la denuncia
									foreach ($users as $iduser)
									{
										//Registramos la misma fecha para la notificación
										$date_notification = date('Y-m-d H:i:s');
										
										//Registramos la Notificación
										$data = array(
											'iduser' => $iduser,
											'title' => $title,
											'message' => $message,
											'source' => $source,
											'view' => $view,
											'identifier' => $identifier,
											'read' => 0,
											'createdAt' => $date_notification,
											'status' => 1
										);
										$this->db->insert('notification', $data);
										$idnotification = $this->db->insert_id();
										
										//Consultamos los dispositivos que tenga registrados el usuario
										$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $iduser . " AND d.status = 1");
										
										//Verificamos si el usuario tiene registrados dispositivos
										if ($query_devices->num_rows() > 0)
										{
											//Obtenemos las Credenciales
											$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
											
											//Creamos el cliente de SNS
											$SnsClient = new SnsClient([
												'version' => 'latest',
												'region' => 'us-west-2',
												'credentials' => $credentials
									        ]);
									        
											//Procesamos el mensaje a los dispositivos
											foreach ($query_devices->result() as $row_device)
											{
												try {
													$result = $SnsClient->publish([
													    'Message' => $title,
													    'TargetArn' => $row_device->endpoint
													]);
												} catch (SnsException $e) { }
											}
										}
									}
								}
							}
							
							//Consultamos las denuncias restantes del grupo
							$query = $this->db->query("SELECT * FROM `group` g WHERE g.status = 1 AND g.idgroup = " . $idgroup);
							
							//Verificamos si existe el Admin
							if ($query->num_rows() > 0)
							{
								//Procesamos los Admins
								foreach ($query->result() as $row) { }
								
								//Consultamos las Denuncias que pertenecen al grupo
								//$query_complaints = $this->db->query("SELECT g.idgroup_complaint, g.idgroup, c.idcomplaint, c.title, c.description, c.lat, c.lng, c.address, c.type, c.media, c.idcategory, c.idamount, c.icon, c.iduser, c.createdAt as date, (SELECT a.sum FROM amount a WHERE a.idamount = c.idamount AND a.status = 1) as sum FROM group_complaint g INNER JOIN complaint c ON g.idcomplaint = c.idcomplaint WHERE g.status = 1 AND g.idgroup = " . $idgroup);
								$query_complaints = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status, gc.idgroup as idgroup FROM group_dependency gd INNER JOIN group_complaint gc ON gc.idgroup = gd.idgroup INNER JOIN complaint c ON c.idcomplaint = gc.idcomplaint INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount INNER JOIN user_dependency ud ON ud.iddependency = gd.iddependency WHERE gc.idgroup = " . $idgroup . " AND gc.status = 1 GROUP BY c.idcomplaint ORDER BY c.idcomplaint ASC");
								$complaints = array();
								
								//Procesamos
								foreach ($query_complaints->result() as $complaint)
								{
									$complaints[] = array(
										'idcomplaint' => $complaint->idcomplaint,
										'title' => $complaint->title,
										'description' => $complaint->description,
										'lat' => $complaint->lat,
										'lng' => $complaint->lng,
										'address' => $complaint->address,
										'type' => $complaint->type,
										'media' => $complaint->media,
										'idcategory' => $complaint->idcategory,
										'category' => $complaint->category,
										'category_description' => $complaint->category_description,
										'idamount' => $complaint->idamount,
										'amount' => $complaint->amount,
										'icon' => $complaint->icon,
										'iduser' => $complaint->iduser,
										'idgroup' => $complaint->idgroup,
										'date' => $complaint->date,
										'status' => $complaint->status
									);
								}
								
								//Verificamos si hay denuncias
								if (count($complaints) == 0)
								{
									//Deshabilitamos el Grupo
									$data = array(
										'status' => 0
									);
									$this->db->where('idgroup', $idgroup);
									$this->db->update('group', $data);
								}
								
								//Generamos el Elemento de Grupos
								$group = array(
									'idgroup' => $row->idgroup,
									'title' => $row->title,
									'description' => $row->description,
									'level' => $row->level,
									'iduser' => $row->iduser,
									'send' => $row->send,
									'complaints' => $complaints
								);
				
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success',
									'data' => $group
								);
				
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Mostrar Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'No existe el grupo.',
									'data' => array()
								);
				
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Mostrar Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'El denuncia no pertenece al grupo.',
								'data' => array()
							);
			
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'El grupo no está activo',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}	
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// updateProfile
			if ($msg == 'updateProfile' && in_array('updateProfile', $calls))
			{
				//Leemos los Datos
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$avatar = (isset($fields['avatar'])) ? (string)trim($fields['avatar']) : '';
				$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
				$idstate = (isset($fields['idstate'])) ? (string)trim($fields['idstate']) : '7';
				$mailing = (isset($fields['mailing'])) ? (string)trim($fields['mailing']) : '0';
				$notifications = (isset($fields['notifications'])) ? (string)trim($fields['notifications']) : '0';
	
				//Verificamos los Datos
				if ($iduser)
				{
					//Consultamos al Admin
					$query = $this->db->query("SELECT * FROM user WHERE iduser = " . $iduser . " AND status = 1 LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Consultamos los Estados
						$query_states = $this->db->query("SELECT * FROM state WHERE idstate = " . $idstate);
						$row_state = $query_states->row();
						
						//Verificamos actualización
						if ($avatar || $password)
						{
							$data = array();
							if ($avatar) { $data['avatar'] = $avatar; }
							if ($password) { $data['password'] = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]); }
						}
						$data['idstate'] = $idstate;
						$data['mailing'] = $mailing;
						$data['notifications'] = $notifications;
						$this->db->where('iduser', $iduser);
						$this->db->update('user', $data);
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => array(
								'idstate' => $row_state->idstate,
							    'state' => $row_state->name,
							    'position' => array(
								    'lat' => $row_state->lat,
								    'lng' => $row_state->lng
							    )
							)
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe este usuario.',
							'data' => array()
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// searchUser
			if ($msg == 'searchUser' && in_array('searchUser', $calls))
			{
				//Leemos los Datos
				$string = (isset($fields['string'])) ? (string)trim($fields['string']) : '';
				
				//Verificamos los Datos
				if ($string)
				{
					//Consultamos el Termino
					$query = $this->db->query('SELECT * FROM user WHERE email LIKE "%' . $string . '%" OR name LIKE "%' . $string . '%"');
					
					//Procesamos el HTML
					$html = '';
					$html.= '<div class="row">';
					$html.= '	<div class="col s12">';
					
					//Verificamos los Resultados
					if ($query->num_rows() > 0)
					{
						$html.= '		<table class="striped responsive-table">';
						$html.= '			<thead>';
						$html.= '				<tr>';
						$html.= '					<th data-field="number">ID</th>';
						$html.= '					<th data-field="name">Nombre</th>';
						$html.= '					<th data-field="email">Correo Electrónico</th>';
						$html.= '					<th data-field="profile">Perfil</th>';
						$html.= '					<th data-field="actions">Acciones</th>';
	          			$html.= '				</tr>';
	        			$html.= '			</thead>';
	
						$html.= '			<tbody>';
						
						foreach ($query->result() as $user)
						{
							$html.= '				<tr>';
							$html.= '					<td>'.$user->iduser.'</td>';
							$html.= '					<td>'.$user->name.'</td>';
							$html.= '					<td>'.$user->email.'</td>';
							
							$html.= '					<td>';
							switch ((string)$user->idprofile)
							{
								case '1': $html.= 'Usuario Normal'; break;
								case '2': $html.= 'Usuario ONG'; break;
								case '3': $html.= 'Usuario Autoridad'; break;
								default: $html.= 'Usuario Normal'; break;
							}
							$html.= '					</td>';
							
							$html.= '					<td>';
							$html.= '						<a class="waves-effect waves-light btn damask" href="/users/edit/'.$user->iduser.'">Editar</a>'; 
							if ($user->status)
							{
								$html.= '						<a rel="'.$user->iduser.'" class="btnDeleteUser waves-effect waves-light btn gray" href="#">Borrar</a>';
							}
							else
							{
								$html.= '						<a rel="'.$user->iduser.'" class="btnActivateUser waves-effect waves-light btn gray" href="#">Activar</a>';
							}
							$html.= '					</td>';
	          				$html.= '				</tr>';
	          			}
	        			
	        			$html.= '			</tbody>';
	      				$html.= '		</table>';
					}
					else
					{
						$html.= '<p>No hay resultados, intenta con otro nombre o correo electrónico o recarga la página para reiniciar la consulta de usuarios.</p>';
					}
					
					$html.= '	</div>';
					$html.= '</div>';
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $html
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Necesitas escribir un nombre o email del usuario.',
						'data' => ''
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getDraftComplaints
			if ($msg == 'getDraftComplaints' && in_array('getDraftComplaints', $calls))
			{
				//Leemos los Datos
				$sort = (isset($fields['sort'])) ? (string)trim($fields['sort']) : '';
				$base_url = (isset($fields['base_url'])) ? (string)trim($fields['base_url']) : '';
				
				//Verificamos si existe un ordenamiento
				if ($sort)
				{
					//Consultamos los Filtros
					switch ($sort) 
					{
						case 'sortID': 
							$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 0 ORDER BY c.idcomplaint ASC");
							break;
						case 'sortTitle':
							$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 0 ORDER BY c.title ASC");
							break;
						case 'sortAddress':
							$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 0 ORDER BY c.address ASC");
							break;
						case 'sortCategory':
							$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 0 ORDER BY category.name ASC");
							break;
						case 'sortAmount':
							$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 0 ORDER BY c.idamount ASC");
							break;
						case 'sortMedia':
							$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 0 ORDER BY c.media DESC");
							break;
						case 'sortDate':
							$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 0 ORDER BY c.createdAt ASC");
							break;
						default: 
							$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 0 ORDER BY c.idcomplaint ASC");
							break;
					}
				}
				else
				{
					//Consultamos
					$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 0 ORDER BY c.idcomplaint ASC");
				}
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$complaints = array();
					$html = '';
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$complaints[] = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							'media' => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'date' => $row->date,
							'status' => $row->status
						);
					}
					
					//Creamos la tabla de resultados en HTML
					$html .= '<table class="striped responsive-table">';
					$html .= '	<thead>';
					$html .= '		<tr>';
					$html .= '			<th data-field="id"><a class="sortModerate damask-text" data="sortID" href="#">#</a></th>';
					$html .= '			<th data-field="title"><a class="sortModerate damask-text" data="sortTitle" href="#">Título</a></th>';
					$html .= '			<th data-field="address"><a class="sortModerate damask-text" data="sortAddress" href="#">Dirección</a></th>';
					$html .= '			<th data-field="category"><a class="sortModerate damask-text" data="sortCategory" href="#">Categoría</a></th>';
					$html .= '			<th data-field="amount" style="width: 140px;"><a class="sortModerate damask-text" data="sortAmount" href="#">Monto</a></th>';
					$html .= '			<th data-field="type"><a class="sortModerate damask-text" data="sortMedia" href="#">Media</a></th>';
					$html .= '			<th data-field="date" style="width: 100px;"><a class="sortModerate damask-text" data="sortDate" href="#">Fecha</a></th>';
					$html .= '			<th data-field="actions">Acciones</th>';
      				$html .= '		</tr>';
    				$html .= '	</thead>';

					$html .= '	<tbody>';
					
					//Procesamos los resultados
					foreach ($complaints as $complaint)
					{
						$html .= '		<tr>';
						$html .= '			<td>'.$complaint['idcomplaint'].'</td>';
						$html .= '			<td>'.$complaint['title'].'</td>';
						$html .= '			<td>'.$complaint['address'].'</td>';
						$html .= '			<td>'.$complaint['category'].'</td>';
						$html .= '			<td>'.$complaint['amount'].'</td>';
						$html .= '			<td>';
						
						switch ($complaint['type']) 
						{
							case 'none': $html.= 'Ninguno'; break;
							case 'image': $html.= 'Imagen'; break;
							case 'video': $html.= 'Video'; break;
							default: $html.= 'Ninguno'; break;
						}
						
						$html .= '			</td>';
						$html .= '			<td>'.$complaint['date'].'</td>';
						$html .= '			<td>';
						$html .= '				<a class="waves-effect waves-light btn damask" href="'.$base_url.'moderate/complaint/'.$complaint['idcomplaint'].'">Ver</a>'; 
						$html .= '			</td>';
          				$html .= '		</tr>';
          			}
        			$html .= '	</tbody>';
      				$html .= '</table>';
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $complaints,
						'html' => $html
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay denuncias nuevas creadas.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// registerDevice
			if ($msg == 'registerDevice' && in_array('registerDevice', $calls))
			{
				//Leemos los datos
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '1';
				$registrationId = (isset($fields['registrationId'])) ? (string)trim($fields['registrationId']) : '62fe891258f7c17f3f669c38eb5e2465139d98b4b4f90ab7b05b4f268804dd6e';
				$platform = (isset($fields['platform'])) ? (string)trim($fields['platform']) : 'ios';
				$SNS_APP_ARN = '';
				
				//Verificamos los datos
				if ($iduser && $registrationId && $platform)
				{
					//Obtenemos las Credenciales
					$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
					
					//Creamos el cliente de SNS
					$SnsClient = new SnsClient([
						'version' => 'latest',
						'region' => 'us-west-2',
						'credentials' => $credentials
			        ]);
			        
			        //Verificamos la plataforma
			        if ($platform == 'ios') { $SNS_APP_ARN = 'arn:aws:sns:us-west-2:903036544634:app/APNS_SANDBOX/incorruptible_dei'; } //APNS_SANDBOX
			        if ($platform == 'android') { $SNS_APP_ARN = 'arn:aws:sns:us-west-2:903036544634:app/GCM/incorruptible_android'; } //GCM
					
					//Consultamos si ya esta registrado un dispositivo con el usuario
					$query = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $iduser . " AND d.platform = '" . $platform . "' AND d.status = 1");
					
					//Verificamos si existe una relación
					if ($query->num_rows() == 0)
					{
						//Generamos el Endpoint del Dispositivo
						$SNSEndPointData = $SnsClient->createPlatformEndpoint([
					       'PlatformApplicationArn' => $SNS_APP_ARN,
					       'Token' => $registrationId
					    ]);
					    
					    //Result Data
					    $endpoint = $SNSEndPointData['EndpointArn'];
					    
						//Registramos el Dispositivo
						$data = array(
							'iduser' => $iduser,
							'registrationId' => $registrationId,
							'endpoint' => $endpoint,
							'platform' => $platform,
							'createdAt' => date('Y-m-d H:i:s'),
							'status' => 1
						);
						$this->db->insert('device', $data);
						$iddevice = $this->db->insert_id();
						
						/*
						//Mandamos una notificación del registro de las notificaciones
						$result = $SnsClient->publish([
						    'Message' => 'Este dispositivo está listo para recibir notificaciones', // REQUIRED
						    'Subject' => 'Nuevo Registro',
						    'TargetArn' => $endpoint
						]);
						*/
						
						//Mostrar Resultados
						$array = array(
							'status' => (int)1,
							'msg' => (string)'success',
							'data' => $endpoint
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Leemos el Registro
						$row = $query->row();
						$endpoint = $row->endpoint;
						
						//Verificamos si el dispositivo es diferente al registrado
						if ((string)trim($row->registrationId) != $registrationId)
						{
							//Eliminamos el Endpoint anterior del Dispositivo
							$SNSDeleteEndPointData = $SnsClient->deleteEndpoint([
						       'EndpointArn' => $row->endpoint
						    ]);
						    
						    //Generamos el Endpoint del Dispositivo
							$SNSEndPointData = $SnsClient->createPlatformEndpoint([
						       'PlatformApplicationArn' => $SNS_APP_ARN,
						       'Token' => $registrationId
						    ]);
						    
						    //Result Data
						    $endpoint = $SNSEndPointData['EndpointArn'];
						    
						    //Actualizamos el Dispositivo
							$data = array(
								'registrationId' => $registrationId,
								'endpoint' => $endpoint,
								'updatedAt' => date('Y-m-d H:i:s')
							);
							$this->db->where('iddevice', $row->iddevice);
							$this->db->update('device', $data);
							
							/*
							//Mandamos una notificación del registro de las notificaciones
							$result = $SnsClient->publish([
							    'Message' => 'Este dispositivo está listo para recibir notificaciones', // REQUIRED
							    'Subject' => 'Nuevo Registro',
							    'TargetArn' => $endpoint
							]);
							*/
						}
						
						//Mostrar Resultados
						$array = array(
							'status' => (int)1,
							'msg' => (string)'success',
							'data' => $endpoint
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos para registrar el dispositivo.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// addNotification
			if ($msg == 'addNotification' && in_array('addNotification', $calls))
			{
				//Leemos los datos
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$title = (isset($fields['title'])) ? (string)trim($fields['title']) : '';
				$message = (isset($fields['message'])) ? (string)trim($fields['message']) : '';
				$source = (isset($fields['source'])) ? (string)trim($fields['source']) : '';
				$view = (isset($fields['view'])) ? (string)trim($fields['view']) : '';
				$identifier = (isset($fields['identifier'])) ? (string)trim($fields['identifier']) : '';
				$SNS_APP_ARN = '';
				
				//Verificamos los campos
				if ($iduser && $title && $message && $source)
				{
					//Consultamos al Usuario
					$query = $this->db->query("SELECT * FROM user WHERE iduser = " . $iduser . " AND status = 1 LIMIT 1");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Leemos los datos del usuario
						$row = $query->row();
						
						//Registramos la Notificación
						$data = array(
							'iduser' => $iduser,
							'title' => $title,
							'message' => $message,
							'source' => $source,
							'view' => $view,
							'identifier' => $identifier,
							'read' => 0,
							'createdAt' => date('Y-m-d H:i:s'),
							'status' => 1
						);
						$this->db->insert('notification', $data);
						$idnotification = $this->db->insert_id();
						
						//Consultamos los dispositivos que tenga registrados el usuario
						$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $iduser . " AND d.status = 1");
						
						//Verificamos si el usuario tiene registrados dispositivos
						if ($query_devices->num_rows() > 0)
						{
							//Obtenemos las Credenciales
							$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
							
							//Creamos el cliente de SNS
							$SnsClient = new SnsClient([
								'version' => 'latest',
								'region' => 'us-west-2',
								'credentials' => $credentials
					        ]);
					        
							//Procesamos el mensaje a los dispositivos
							foreach ($query_devices->result() as $row_device)
							{
								try {
									$result = $SnsClient->publish([
									    'Message' => $title,
									    'TargetArn' => $row_device->endpoint
									]);
								} catch (SnsException $e) { }
							}
						}
						
						//Mostrar Resultados
						$array = array(
							'status' => (int)1,
							'msg' => (string)'success',
							'data' => $idnotification
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe este usuario.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// readNotification
			if ($msg == 'readNotification' && in_array('readNotification', $calls))
			{
				//Leer Valores
				$idnotification = (isset($fields['idnotification'])) ? (string)trim($fields['idnotification']) : '';
				
				//Verificamos
				if ($idnotification)
				{
					//Consultamos
					$query = $this->db->query("SELECT * FROM notification n WHERE n.idnotification = " . $idnotification . " AND n.status");
					
					//Verificamos si existen Notificaciones
					if ($query->num_rows() > 0)
					{
						//Actualizamos la notificación
						$data = array(
							'read' => 1
						);
						$this->db->where('idnotification', $idnotification);
						$this->db->update('notification', $data);
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success'
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe la notificación.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan Campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getNotificationsUser
			if ($msg == 'getNotificationsUser' && in_array('getNotificationsUser', $calls))
			{
				//Leer Valores
				$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				
				//Verificamos
				if ($iduser)
				{
					//Consultamos
					$query = $this->db->query("SELECT n.idnotification, n.title, n.message, n.source, n.view, n.identifier, n.read, n.createdAt FROM notification n WHERE n.iduser = " . $iduser . " AND n.status ORDER BY n.idnotification DESC LIMIT 10");
		
					//Verificamos si existen Notificaciones
					if ($query->num_rows() > 0)
					{
						//Declaramos el Arreglo de Notificaciones
						$notifications = array();
		
						//Procesamos las Notificaciones
						foreach ($query->result() as $row)
						{
							//Generamos el Elemento de Notificacion
							$notifications[] = array(
								'idnotification' => $row->idnotification,
								'title' => $row->title,
								'message' => $row->message,
								'source' => $row->source,
								'view' => $row->view,
								'identifier' => $row->identifier,
								'read' => $row->read,
								'createdAt' => $row->createdAt,
								'timeago' => $this->get_timeago($row->createdAt)
							);
						}
		
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $notifications
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No hay notificaciones para tu usuario.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan Campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// addNotificationComplaint
			if ($msg == 'addNotificationComplaint' && in_array('getNotificationsUser', $calls))
			{
				//Leemos los datos
				$title = (isset($fields['title'])) ? (string)trim($fields['title']) : '';
				$message = (isset($fields['description'])) ? (string)trim($fields['description']) : '';
				$source = (isset($fields['idprofile'])) ? (string)trim($fields['idprofile']) : '2';
				$view = (isset($fields['view'])) ? (string)trim($fields['view']) : 'ViewComplaint';
				$identifier = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
				$idcomplaint = $identifier;
				$SNS_APP_ARN = '';
				$users = array();
				if ($source == '2') { $source = 'ONG'; }
				if ($source == '3') { $source = 'AUTORIDAD'; }
				
				//Verificamos los campos
				if ($title && $message && $identifier)
				{
					//Consultamos la Denuncia
					$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.idcomplaint = " . $identifier . " ORDER BY c.idcomplaint ASC");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Leemos los datos del usuario
						$row = $query->row();
						$users[] = $row->iduser;
						
						//Consultamos los usuarios que siguen la denuncia
						$query_follows = $this->db->query("SELECT fc.* FROM follow_complaint fc WHERE fc.idcomplaint = " . $row->idcomplaint . " AND fc.status = 1");
						
						//Procesamos los usuarios que siguen la denuncia
						foreach ($query_follows->result() as $row_follow)
						{
							$users[] = $row_follow->iduser;
						}
						
						//Procesamos las notificaciones para todos los usuarios unidos a la denuncia
						foreach ($users as $iduser)
						{
							//Registramos la misma fecha para la notificación
							$date_notification = date('Y-m-d H:i:s');
							
							//Registramos la Notificación
							$data = array(
								'iduser' => $iduser,
								'title' => $title,
								'message' => $message,
								'source' => $source,
								'view' => $view,
								'identifier' => $identifier,
								'read' => 0,
								'createdAt' => $date_notification,
								'status' => 1
							);
							$this->db->insert('notification', $data);
							$idnotification = $this->db->insert_id();
							
							//Consultamos los dispositivos que tenga registrados el usuario
							$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $iduser . " AND d.status = 1");
							
							//Verificamos si el usuario tiene registrados dispositivos
							if ($query_devices->num_rows() > 0)
							{
								//Obtenemos las Credenciales
								$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
								
								//Creamos el cliente de SNS
								$SnsClient = new SnsClient([
									'version' => 'latest',
									'region' => 'us-west-2',
									'credentials' => $credentials
						        ]);
						        
								//Procesamos el mensaje a los dispositivos
								foreach ($query_devices->result() as $row_device)
								{
									try {
										$result = $SnsClient->publish([
										    'Message' => $title,
										    'TargetArn' => $row_device->endpoint
										]);
									} catch (SnsException $e) { }
								}
							}
						}
						
						$comments = array();
						$groups = array();
						
						//Consultamos los Comentarios
						$query_comments = $this->db->query("SELECT c.idcomment_complaint as idcomment_complaint, c.text as text, c.idamount as idamount, amount.value as amount, c.iduser as iduser, c.createdAt as date FROM comment_complaint c INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 AND c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomment_complaint DESC");
		
						//Consultamos los Grupos
						$query_groups = $this->db->query("SELECT g.idgroup as idgroup, g.title as title, g.description as description, g.level as level, g.send as send FROM group_complaint gc INNER JOIN complaint c ON gc.idcomplaint = c.idcomplaint INNER JOIN `group` g ON gc.idgroup = g.idgroup WHERE gc.idcomplaint = " . $idcomplaint . " ORDER BY gc.idcomplaint ASC");
						
						//Consultamos los Archivos
						$query_files = $this->db->query("SELECT * FROM file WHERE idcomplaint = " . $idcomplaint . " ORDER BY createdAt ASC LIMIT 10");
						
						//Consultamos las Notificaciones
						$query_notifications = $this->db->query("SELECT DISTINCT(n.title), n.message, n.source, n.view, n.identifier, n.read, n.createdAt FROM notification n WHERE n.view = 'ViewComplaint' AND n.identifier = " . $idcomplaint . " ORDER BY n.idnotification DESC LIMIT 10");
						//Declaramos el Arreglo de Notificaciones
						$notifications = array();
						
						//Procesamos las Notificaciones
						if ($query_notifications->num_rows() > 0)
						{
							//Procesamos las Notificaciones
							foreach ($query_notifications->result() as $row_notification)
							{
								//Generamos el Elemento de Notificacion
								$notifications[] = array(
									//'idnotification' => $row_notification->idnotification,
									'title' => $row_notification->title,
									'message' => $row_notification->message,
									'source' => $row_notification->source,
									'view' => $row_notification->view,
									'identifier' => $row_notification->identifier,
									'read' => $row_notification->read,
									'createdAt' => $row_notification->createdAt,
									'timeago' => $this->get_timeago($row_notification->createdAt)
								);
							}
						}
						
						//Procesamos los Grupos
						foreach ($query_groups->result() as $group)
						{
							$query_dependencies = $this->db->query("SELECT d.iddependency as iddependency, d.name as name FROM group_dependency gd INNER JOIN dependency d ON gd.iddependency = d.iddependency WHERE gd.idgroup = " . $group->idgroup);
							
							$groups[] = array(
								'idgroup' => $group->idgroup,
								'title' => $group->title,
								'description' => $group->description,
								'level' => $group->level,
								'send' => $group->send,
								'dependencies' => $query_dependencies->result(),
								'updates' => array()
							);
						}
						
						//Generamos el Elemento de Area
						$complaint = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							$row->type => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'date' => $row->date,
							'status' => $row->status,
							'comments' => $row->comments,
							'follows' => $row->follows,
							'comments_data' => $query_comments->result(),
							'groups_data' => $groups,
							'files_data' => $query_files->result(),
							'notifications_data' => $notifications
						);
						
						//Mostrar Resultados
						$array = array(
							'status' => (int)1,
							'msg' => (string)'success',
							'data' => $complaint
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe esta denuncia.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// addMessageComplaint
			if ($msg == 'addMessageComplaint' && in_array('addMessageComplaint', $calls))
			{
				//Leemos los datos
				$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
				$message = (isset($fields['text'])) ? (string)trim($fields['text']) : '';
				$idsender = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$SNS_APP_ARN = '';
				$source = 'AUTORIDAD';
				
				//Verificamos los campos
				if ($message)
				{
					//Consultamos la Denuncia
					$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomplaint ASC");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Leemos los datos del usuario
						$row = $query->row();
						$users[] = $row->iduser;
						
						//Registramos la misma fecha para la notificación
						$date_notification = date('Y-m-d H:i:s');
						
						//Guardamos el Mensaje
						$data = array(
							'idcomplaint' => $idcomplaint,
							'message' => $message,
							'owner' => $idsender,
							'read' => 0,
							'createdAt' => $date_notification,
							'status' => 1
						);
						$this->db->insert('message', $data);
						
						//Procesamos las notificaciones para todos los usuarios unidos a la denuncia
						foreach ($users as $iduser)
						{
							//Consultamos el Usuario
							$query_user = $this->db->query("SELECT * FROM user WHERE iduser = " . $iduser . " LIMIT 1");
							$row_user = $query_user->row();
							
							//Enviamos un correo con el mensaje
							$subject = 'Has recibido un mensaje de la Autoridad - Incorruptible';
							$data['link'] = 'http://app.incorruptible.mx/#/viewcomplaint/' . $idcomplaint;
							$email = $row_user->email;
							$name = $row_user->name;
							$body = $this->load->view('mail/message_dependency', $data, TRUE);
							$altbody = strip_tags($body);
		
							//Verificamos
							if ($subject != '' && $body != '' && $altbody != '' && $email != '')
							{
								//Mandamos el Correo
								$mail = new PHPMailer(true);
								$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
								$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
								$mail->CharSet = 'UTF-8';
								$mail->Encoding = "quoted-printable";
						        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
						        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
						        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
						        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
						        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
						        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
						        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
						        $mail->SMTPDebug  = 0;
						        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
						        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
						        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
						        $mail->Body      = $body;
						        $mail->AltBody    = $altbody;
						        $mail->AddAddress($email, $name);
						        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
		
						        if ($mail->Send())
								{
									$response = true;
								}
								else
								{
									$response = false;
								}
							}
							
							//Registramos la Notificación
							$data = array(
								'iduser' => $row->iduser,
								'title' => 'La autoridad ha enviado un mensaje en tu denuncia',
								'message' => 'La autoridad ha enviado un mensaje respecto a tu denuncia, no compartas datos personales ni de contacto con la autoridad',
								'source' => $source,
								'view' => 'ViewComplaint',
								'identifier' => $idcomplaint,
								'read' => 0,
								'createdAt' => $date_notification,
								'status' => 1
							);
							$this->db->insert('notification', $data);
							$idnotification = $this->db->insert_id();
							
							//Consultamos los dispositivos que tenga registrados el usuario
							$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $iduser . " AND d.status = 1");
							
							//Verificamos si el usuario tiene registrados dispositivos
							if ($query_devices->num_rows() > 0)
							{
								//Obtenemos las Credenciales
								$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
								
								//Creamos el cliente de SNS
								$SnsClient = new SnsClient([
									'version' => 'latest',
									'region' => 'us-west-2',
									'credentials' => $credentials
						        ]);
						        
								//Procesamos el mensaje a los dispositivos
								foreach ($query_devices->result() as $row_device)
								{
									try {
										$result = $SnsClient->publish([
										    'Message' => 'La autoridad ha enviado un mensaje en tu denuncia',
										    'TargetArn' => $row_device->endpoint
										]);
									} catch (SnsException $e) { }
								}
							}
						}
						
						//Consultamos los Mensajes
						$query_messages = $this->db->query("SELECT DISTINCT(m.idmessage), m.message, m.owner, m.read, m.createdAt FROM message m WHERE m.idcomplaint = " . $idcomplaint . " ORDER BY m.idmessage ASC");
						//Declaramos el Arreglo de Notificaciones
						$messages = array();
						
						//Procesamos las Notificaciones
						if ($query_messages->num_rows() > 0)
						{
							//Procesamos las Notificaciones
							foreach ($query_messages->result() as $row_message)
							{
								//Generamos el Elemento de Notificacion
								$messages[] = array(
									'idmessage' => $row_message->idmessage,
									'message' => $row_message->message,
									'owner' => $row_message->owner,
									'read' => $row_message->read,
									'createdAt' => $row_message->createdAt,
									'timeago' => $this->get_timeago($row_message->createdAt)
								);
							}
						}
						
						//Generamos el Elemento de Area
						$complaint = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							$row->type => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'date' => $row->date,
							'status' => $row->status,
							'comments' => $row->comments,
							'follows' => $row->follows,
							'messages_data' => $messages
						);
						
						//Mostrar Resultados
						$array = array(
							'status' => (int)1,
							'msg' => (string)'success',
							'data' => $complaint
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe esta denuncia.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// updateMessageComplaint
			if ($msg == 'updateMessageComplaint' && in_array('updateMessageComplaint', $calls))
			{
				//Leemos los datos
				$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
				$message = (isset($fields['text'])) ? (string)trim($fields['text']) : '';
				$idsender = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$idmessage = (isset($fields['idmessage'])) ? (string)trim($fields['idmessage']) : '';
				$SNS_APP_ARN = '';
				$source = 'AUTORIDAD';
				
				//Verificamos los campos
				if ($message)
				{
					//Consultamos la Denuncia
					$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomplaint ASC");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Leemos los datos del usuario
						$row = $query->row();
						$users[] = $row->iduser;
						
						//Registramos la misma fecha para la notificación
						$date_notification = date('Y-m-d H:i:s');
						
						//Guardamos el Mensaje
						$data = array(
							'message' => $message,
							'updatedAt' => $date_notification,
							'status' => 1
						);
						$this->db->where('idmessage', $idmessage);
						$this->db->update('message', $data);
						
						//Procesamos las notificaciones para todos los usuarios unidos a la denuncia
						foreach ($users as $iduser)
						{
							//Consultamos el Usuario
							$query_user = $this->db->query("SELECT * FROM user WHERE iduser = " . $iduser . " LIMIT 1");
							$row_user = $query_user->row();
							
							//Enviamos un correo con el mensaje
							$subject = 'Has recibido un mensaje de la Autoridad - Incorruptible';
							$data['link'] = 'http://app.incorruptible.mx/#/viewcomplaint/' . $idcomplaint;
							$email = $row_user->email;
							$name = $row_user->name;
							$body = $this->load->view('mail/message_dependency', $data, TRUE);
							$altbody = strip_tags($body);
		
							//Verificamos
							if ($subject != '' && $body != '' && $altbody != '' && $email != '')
							{
								//Mandamos el Correo
								$mail = new PHPMailer(true);
								$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
								$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
								$mail->CharSet = 'UTF-8';
								$mail->Encoding = "quoted-printable";
						        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
						        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
						        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
						        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
						        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
						        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
						        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
						        $mail->SMTPDebug  = 0;
						        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
						        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
						        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
						        $mail->Body      = $body;
						        $mail->AltBody    = $altbody;
						        $mail->AddAddress($email, $name);
						        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
		
						        if ($mail->Send())
								{
									$response = true;
								}
								else
								{
									$response = false;
								}
							}
							
							//Registramos la Notificación
							$data = array(
								'iduser' => $row->iduser,
								'title' => 'La autoridad ha enviado un mensaje en tu denuncia',
								'message' => 'La autoridad ha enviado un mensaje respecto a tu denuncia, no compartas datos personales ni de contacto con la autoridad',
								'source' => $source,
								'view' => 'ViewComplaint',
								'identifier' => $idcomplaint,
								'read' => 0,
								'createdAt' => $date_notification,
								'status' => 1
							);
							$this->db->insert('notification', $data);
							$idnotification = $this->db->insert_id();
							
							//Consultamos los dispositivos que tenga registrados el usuario
							$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $iduser . " AND d.status = 1");
							
							//Verificamos si el usuario tiene registrados dispositivos
							if ($query_devices->num_rows() > 0)
							{
								//Obtenemos las Credenciales
								$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
								
								//Creamos el cliente de SNS
								$SnsClient = new SnsClient([
									'version' => 'latest',
									'region' => 'us-west-2',
									'credentials' => $credentials
						        ]);
						        
								//Procesamos el mensaje a los dispositivos
								foreach ($query_devices->result() as $row_device)
								{
									try {
										$result = $SnsClient->publish([
										    'Message' => 'La autoridad ha enviado un mensaje en tu denuncia',
										    'TargetArn' => $row_device->endpoint
										]);
									} catch (SnsException $e) { }
								}
							}
						}
						
						//Consultamos los Mensajes
						$query_messages = $this->db->query("SELECT DISTINCT(m.idmessage), m.message, m.owner, m.read, m.createdAt FROM message m WHERE m.idcomplaint = " . $idcomplaint . " ORDER BY m.idmessage ASC");
						//Declaramos el Arreglo de Notificaciones
						$messages = array();
						
						//Procesamos las Notificaciones
						if ($query_messages->num_rows() > 0)
						{
							//Procesamos las Notificaciones
							foreach ($query_messages->result() as $row_message)
							{
								//Generamos el Elemento de Notificacion
								$messages[] = array(
									'idmessage' => $row_message->idmessage,
									'message' => $row_message->message,
									'owner' => $row_message->owner,
									'read' => $row_message->read,
									'createdAt' => $row_message->createdAt,
									'timeago' => $this->get_timeago($row_message->createdAt)
								);
							}
						}
						
						//Generamos el Elemento de Area
						$complaint = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							$row->type => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'date' => $row->date,
							'status' => $row->status,
							'comments' => $row->comments,
							'follows' => $row->follows,
							'messages_data' => $messages
						);
						
						//Mostrar Resultados
						$array = array(
							'status' => (int)1,
							'msg' => (string)'success',
							'data' => $complaint
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe esta denuncia.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// addResponseMessageComplaint
			if ($msg == 'addResponseMessageComplaint' && in_array('addResponseMessageComplaint', $calls))
			{
				//Leemos los datos
				$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
				$message = (isset($fields['text'])) ? (string)trim($fields['text']) : '';
				$idsender = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
				$SNS_APP_ARN = '';
				$source = 'Incorruptible';
				
				//Verificamos los campos
				if ($message)
				{
					//Consultamos la Denuncia
					$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, category.description as category_description, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows, c.status as status FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomplaint ASC");
	
					//Verificamos si existe el Admin
					if ($query->num_rows() > 0)
					{
						//Leemos los datos del usuario
						$row = $query->row();
						$users[] = array();
						
						//Buscar a la autoridad que esta validando a denuncia
						$query_users_dependency = $this->db->query("SELECT (SELECT iduser FROM user WHERE user.iduser = ud.iduser) as iduser, (SELECT email FROM user WHERE user.iduser = ud.iduser) as email, (SELECT name FROM user WHERE user.iduser = ud.iduser) as name, (SELECT idprofile FROM user WHERE user.iduser = ud.iduser) as idprofile, (SELECT mailing FROM user WHERE user.iduser = ud.iduser) as mailing, (SELECT notifications FROM user WHERE user.iduser = ud.iduser) as notifications, (SELECT status FROM user WHERE user.iduser = ud.iduser) as status FROM complaint c INNER JOIN group_complaint gc ON gc.idcomplaint = c.idcomplaint INNER JOIN group_dependency gd ON gd.idgroup = gc.idgroup INNER JOIN dependency as d ON d.iddependency = gd.iddependency INNER JOIN user_dependency ud ON ud.iddependency = d.iddependency WHERE c.idcomplaint = " . $idcomplaint . " AND ud.status = 1");
						
						//Registramos la misma fecha para la notificación
						$date_notification = date('Y-m-d H:i:s');
						
						//Guardamos el Mensaje
						$data = array(
							'idcomplaint' => $idcomplaint,
							'message' => $message,
							'owner' => $idsender,
							'read' => 0,
							'createdAt' => $date_notification,
							'status' => 1
						);
						$this->db->insert('message', $data);
						
						//Procesamos las notificaciones para todos los usuarios unidos a la denuncia
						foreach ($query_users_dependency->result() as $row_user)
						{
							//Enviamos un correo con el mensaje
							$subject = 'Has recibido una respuesta en una denuncia - Incorruptible';
							$data['link'] = 'http://app.incorruptible.mx/#/viewcomplaint/' . $idcomplaint;
							$email = $row_user->email;
							$name = $row_user->name;
							$body = $this->load->view('mail/response_user', $data, TRUE);
							$altbody = strip_tags($body);
		
							//Verificamos
							if ($subject != '' && $body != '' && $altbody != '' && $email != '')
							{
								//Mandamos el Correo
								$mail = new PHPMailer(true);
								$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
								$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
								$mail->CharSet = 'UTF-8';
								$mail->Encoding = "quoted-printable";
						        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
						        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
						        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
						        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
						        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
						        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
						        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
						        $mail->SMTPDebug  = 0;
						        $mail->SetFrom("noreply@incorruptible.mx", utf8_encode('Incorruptible App'));  //Quien envía el correo
						        $mail->AddReplyTo("noreply@incorruptible.mx", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
						        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
						        $mail->Body      = $body;
						        $mail->AltBody    = $altbody;
						        $mail->AddAddress($email, $name);
						        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
		
						        if ($mail->Send())
								{
									$response = true;
								}
								else
								{
									$response = false;
								}
							}
							
							//Registramos la Notificación
							$data = array(
								'iduser' => $row_user->iduser,
								'title' => 'El denunciante ha contestado un mensaje en su denuncia',
								'message' => 'El creador de la denuncia ha contestado al mensaje enviado a su denuncia, no solicites datos personales ni de contacto del denunciante',
								'source' => $source,
								'view' => 'ViewComplaint',
								'identifier' => $idcomplaint,
								'read' => 0,
								'createdAt' => $date_notification,
								'status' => 1
							);
							$this->db->insert('notification', $data);
							$idnotification = $this->db->insert_id();
							
							//Consultamos los dispositivos que tenga registrados el usuario
							$query_devices = $this->db->query("SELECT * FROM device d WHERE d.iduser = " . $row_user->iduser . " AND d.status = 1");
							
							//Verificamos si el usuario tiene registrados dispositivos
							if ($query_devices->num_rows() > 0)
							{
								//Obtenemos las Credenciales
								$credentials = new Credentials($this->SNS_ACCESS_KEY, $this->SNS_SECRET_KEY);
								
								//Creamos el cliente de SNS
								$SnsClient = new SnsClient([
									'version' => 'latest',
									'region' => 'us-west-2',
									'credentials' => $credentials
						        ]);
						        
								//Procesamos el mensaje a los dispositivos
								foreach ($query_devices->result() as $row_device)
								{
									try {
										$result = $SnsClient->publish([
										    'Message' => 'El denunciante ha contestado un mensaje en su denuncia',
										    'TargetArn' => $row_device->endpoint
										]);
									} catch (SnsException $e) { }
								}
							}
						}
						
						//Consultamos los Mensajes
						$query_messages = $this->db->query("SELECT DISTINCT(m.idmessage), m.message, m.owner, m.read, m.createdAt FROM message m WHERE m.idcomplaint = " . $idcomplaint . " ORDER BY m.idmessage ASC");
						
						//Declaramos el Arreglo de Notificaciones
						$messages = array();
						
						//Procesamos las Notificaciones
						if ($query_messages->num_rows() > 0)
						{
							//Procesamos las Notificaciones
							foreach ($query_messages->result() as $row_message)
							{
								//Generamos el Elemento de Notificacion
								$messages[] = array(
									'idmessage' => $row_message->idmessage,
									'message' => $row_message->message,
									'owner' => $row_message->owner,
									'read' => $row_message->read,
									'createdAt' => $row_message->createdAt,
									'timeago' => $this->get_timeago($row_message->createdAt)
								);
							}
						}
						
						//Generamos el Elemento de Area
						$complaint = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							$row->type => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'category_description' => $row->category_description,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'date' => $row->date,
							'status' => $row->status,
							'comments' => $row->comments,
							'follows' => $row->follows,
							'messages_data' => $messages
						);
						
						//Mostrar Resultados
						$array = array(
							'status' => (int)1,
							'msg' => (string)'success',
							'data' => $complaint
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'No existe esta denuncia.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//updateComplaintStatus
			if ($msg == 'updateComplaintStatus' && in_array('updateComplaintStatus', $calls))
			{
				//Leemos los datos
				$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
				$status = (isset($fields['status'])) ? (string)trim($fields['status']) : '';
				
				//Verificamos
				if ($idcomplaint)
				{
					//Actualizamos la denuncia
					$data = array(
						'status' => (int)$status
					);
					$this->db->where('idcomplaint', $idcomplaint);
					$this->db->update('complaint', $data);
					
					//Mostrar Resultados
					$array = array(
						'status' => (int)1,
						'msg' => (string)'success'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getStates
			if ($msg == 'getStates' && in_array('getStates', $calls))
			{
				//Consultamos
				$query = $this->db->query("SELECT * FROM state WHERE state.status = 1 ORDER BY state.idstate ASC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Estados
					$states = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$states[] = array(
							'idstate' => $row->idstate,
							'name' => $row->name,
							'iso' => $row->iso,
							'lat' => $row->lat,
							'lng' => $row->lng
						);
					}
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $states
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay estados activos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getMails
			if ($msg == 'getDependenciesBackend' && in_array('getDependenciesBackend', $calls))
			{
				//Leemos los Campos
				$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
				$limit = '10';
				$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
				
				$query = $this->db->query("SELECT d.*,s.name as state FROM dependency d INNER JOIN state s ON s.idstate = d.idstate ORDER BY d.createdAt DESC LIMIT " . $offset . "," . $limit);
				$dependencies = $query->result();
				$query = $this->db->query("SELECT d.*,s.name as state FROM dependency d INNER JOIN state s ON s.idstate = d.idstate ORDER BY d.createdAt DESC");
				$dependencies_all = $query->result();
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => array(
						'dependencies' => $dependencies,
						'dependencies_all' => $dependencies_all,
						'page' => $page
					)
				);
	
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
				
				die();
			}
			
			// searchDependency
			if ($msg == 'searchDependency' && in_array('searchDependency', $calls))
			{
				//Leemos los Datos
				$string = (isset($fields['string'])) ? (string)trim($fields['string']) : '';
				
				//Verificamos los Datos
				if ($string)
				{
					//Consultamos el Termino
					$query = $this->db->query('SELECT d.*,s.name as state FROM dependency d INNER JOIN state s ON s.idstate = d.idstate WHERE d.name LIKE "%' . $string . '%"');
					
					//Procesamos el HTML
					$html = '';
					$html.= '<div class="row">';
					$html.= '	<div class="col s12">';
					
					//Verificamos los Resultados
					if ($query->num_rows() > 0)
					{
						$html.= '		<table class="striped responsive-table">';
						$html.= '			<thead>';
						$html.= '				<tr>';
						$html.= '					<th data-field="number">ID</th>';
						$html.= '					<th data-field="name">Nombre</th>';
						$html.= '					<th data-field="level">Nivel</th>';
						$html.= '					<th data-field="state">Estado</th>';
						$html.= '					<th data-field="actions">Acciones</th>';
	          			$html.= '				</tr>';
	        			$html.= '			</thead>';
	
						$html.= '			<tbody>';
						
						foreach ($query->result() as $dependency)
						{
							$html.= '				<tr>';
							$html.= '					<td>'.$dependency->iddependency.'</td>';
							$html.= '					<td>'.$dependency->name.'</td>';
							$html.= '					<td>'.$dependency->level.'</td>';
							$html.= '					<td>'.$dependency->state.'</td>';
							
							$html.= '					<td>';
							$html.= '						<a class="waves-effect waves-light btn damask" href="/dependency/edit/'.$dependency->iddependency.'">Editar</a>'; 
							if ($dependency->status)
							{
								$html.= '						<a rel="'.$dependency->iddependency.'" class="btnDeleteDependency waves-effect waves-light btn gray" href="#">Borrar</a>';
							}
							else
							{
								$html.= '						<a rel="'.$dependency->iddependency.'" class="btnActivateDependency waves-effect waves-light btn gray" href="#">Activar</a>';
							}
							$html.= '					</td>';
	          				$html.= '				</tr>';
	          			}
	        			
	        			$html.= '			</tbody>';
	      				$html.= '		</table>';
					}
					else
					{
						$html.= '<p>No hay resultados, intenta con otro nombre de Autoridad o recarga la página para reiniciar la consulta de autoridades.</p>';
					}
					
					$html.= '	</div>';
					$html.= '</div>';
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $html
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Necesitas escribir un nombre de autoridad.',
						'data' => ''
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//processDependency
			if ($msg == 'processDependency' && in_array('processDependency', $calls))
			{
				//Leemos los datos
				$id = (isset($fields['id'])) ? (string)trim($fields['id']) : '';
				$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
				$file = (isset($fields['file'])) ? (string)trim($fields['file']) : '';
				$photo = (isset($fields['photo'])) ? (string)trim($fields['photo']) : '';
				$level = (isset($fields['level'])) ? (string)trim($fields['level']) : '';
				$idstate = (isset($fields['idstate'])) ? (string)trim($fields['idstate']) : '';
				
				//Verificamos si es nueva Autoridad o actualización de Autoridad
				if ($id)
				{
					//Creamos el Arreglo de Autoridad
					$data = array(
						'name' => $name,
						'file' => $file,
						'photo' => $photo,
						'level' => $level,
						'idstate' => $idstate,
						'updatedAt' => date('Y-m-d H:i:s')
					);
					$this->db->where('iddependency', $id);
					$this->db->update('dependency', $data);
					
					//Mostrar Resultados
					$array = array(
						'status' => (int)1,
						'msg' => (string)'success'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Creamos el Arreglo de Autoridad
					$data = array(
						'name' => $name,
						'file' => $file,
						'photo' => $photo,
						'level' => $level,
						'idstate' => $idstate,
						'createdAt' => date('Y-m-d H:i:s'),
						'status' => 1
					);
					$this->db->insert('dependency', $data);
					
					//Mostrar Resultados
					$array = array(
						'status' => (int)1,
						'msg' => (string)'success'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//getDependency
			if ($msg == 'getDependency' && in_array('getDependency', $calls))
			{
				//Leemos los Datos
				$iddependency = (isset($fields['iddependency'])) ? (string)trim($fields['iddependency']) : '';
				
				//Verificamos
				if ($iddependency)
				{
					//Consultamos la Dependencia
					$query = $this->db->query('SELECT d.iddependency as iddependency,d.name as name,d.file as file,d.photo as photo,d.level as level,d.idstate as idstate,s.name as state FROM dependency d INNER JOIN state s ON s.idstate = d.idstate WHERE d.iddependency = '.$iddependency.' AND d.status = 1');
				
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $query->row()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Debes enviar el identificador de la Autoridad.',
						'data' => ''
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// deleteDependency
			if ($msg == 'deleteDependency' && in_array('deleteDependency', $calls))
			{
				//Leemos el Id Dependency
				$iddependency = (isset($fields['iddependency'])) ? (string)trim($fields['iddependency']) : '';
				
				//Verificamos
				if ($iddependency)
				{
					//Actualizamos el status
					$data = array(
						'status' => 0
					);
					$this->db->where('iddependency', $iddependency);
					$this->db->update('dependency', $data);
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $iddependency
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// activateDependency
			if ($msg == 'activateDependency' && in_array('activateDependency', $calls))
			{
				//Leemos el Id User
				$iddependency = (isset($fields['iddependency'])) ? (string)trim($fields['iddependency']) : '';
				
				//Verificamos
				if ($iddependency)
				{
					//Actualizamos el status
					$data = array(
						'status' => 1
					);
					$this->db->where('iddependency', $iddependency);
					$this->db->update('dependency', $data);
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $iddependency
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getONGs
			if ($msg == 'getONGs' && in_array('getONGs', $calls))
			{
				//Leemos los Campos
				$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
				$limit = '10';
				$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
				
				$query = $this->db->query("SELECT o.*,s.name as state FROM ong o INNER JOIN state s ON s.idstate = o.idstate ORDER BY o.createdAt DESC LIMIT " . $offset . "," . $limit);
				$ongs = $query->result();
				$query = $this->db->query("SELECT o.*,s.name as state FROM ong o INNER JOIN state s ON s.idstate = o.idstate ORDER BY o.createdAt DESC");
				$ongs_all = $query->result();
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => array(
						'ongs' => $ongs,
						'ongs_all' => $ongs_all,
						'page' => $page
					)
				);
	
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
				
				die();
			}
			
			// searchONG
			if ($msg == 'searchONG' && in_array('searchONG', $calls))
			{
				//Leemos los Datos
				$string = (isset($fields['string'])) ? (string)trim($fields['string']) : '';
				
				//Verificamos los Datos
				if ($string)
				{
					//Consultamos el Termino
					$query = $this->db->query('SELECT o.*,s.name as state FROM ong o INNER JOIN state s ON s.idstate = o.idstate WHERE o.name LIKE "%' . $string . '%"');
					
					//Procesamos el HTML
					$html = '';
					$html.= '<div class="row">';
					$html.= '	<div class="col s12">';
					
					//Verificamos los Resultados
					if ($query->num_rows() > 0)
					{
						$html.= '		<table class="striped responsive-table">';
						$html.= '			<thead>';
						$html.= '				<tr>';
						$html.= '					<th data-field="number">ID</th>';
						$html.= '					<th data-field="name">Nombre</th>';
						$html.= '					<th data-field="level">Nivel</th>';
						$html.= '					<th data-field="state">Estado</th>';
						$html.= '					<th data-field="actions">Acciones</th>';
	          			$html.= '				</tr>';
	        			$html.= '			</thead>';
	
						$html.= '			<tbody>';
						
						foreach ($query->result() as $ong)
						{
							$html.= '				<tr>';
							$html.= '					<td>'.$ong->idong.'</td>';
							$html.= '					<td>'.$ong->name.'</td>';
							$html.= '					<td>'.$ong->level.'</td>';
							$html.= '					<td>'.$ong->state.'</td>';
							
							$html.= '					<td>';
							$html.= '						<a class="waves-effect waves-light btn damask" href="/ong/edit/'.$ong->idong.'">Editar</a>'; 
							if ($dependency->status)
							{
								$html.= '						<a rel="'.$ong->idong.'" class="btnDeleteONG waves-effect waves-light btn gray" href="#">Borrar</a>';
							}
							else
							{
								$html.= '						<a rel="'.$ong->idong.'" class="btnActivateONG waves-effect waves-light btn gray" href="#">Activar</a>';
							}
							$html.= '					</td>';
	          				$html.= '				</tr>';
	          			}
	        			
	        			$html.= '			</tbody>';
	      				$html.= '		</table>';
					}
					else
					{
						$html.= '<p>No hay resultados, intenta con otro nombre de ONG o recarga la página para reiniciar la consulta de ongs.</p>';
					}
					
					$html.= '	</div>';
					$html.= '</div>';
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $html
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Necesitas escribir un nombre de ONG.',
						'data' => ''
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//processONG
			if ($msg == 'processONG' && in_array('processONG', $calls))
			{
				//Leemos los datos
				$id = (isset($fields['id'])) ? (string)trim($fields['id']) : '';
				$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
				$file = (isset($fields['file'])) ? (string)trim($fields['file']) : '';
				$photo = (isset($fields['photo'])) ? (string)trim($fields['photo']) : '';
				$level = (isset($fields['level'])) ? (string)trim($fields['level']) : '';
				$idstate = (isset($fields['idstate'])) ? (string)trim($fields['idstate']) : '';
				
				//Verificamos si es nueva Autoridad o actualización de Autoridad
				if ($id)
				{
					//Creamos el Arreglo de Autoridad
					$data = array(
						'name' => $name,
						'file' => $file,
						'photo' => $photo,
						'level' => $level,
						'idstate' => $idstate,
						'updatedAt' => date('Y-m-d H:i:s')
					);
					$this->db->where('idong', $id);
					$this->db->update('ong', $data);
					
					//Mostrar Resultados
					$array = array(
						'status' => (int)1,
						'msg' => (string)'success'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Creamos el Arreglo de Autoridad
					$data = array(
						'name' => $name,
						'file' => $file,
						'photo' => $photo,
						'level' => $level,
						'idstate' => $idstate,
						'createdAt' => date('Y-m-d H:i:s'),
						'status' => 1
					);
					$this->db->insert('ong', $data);
					
					//Mostrar Resultados
					$array = array(
						'status' => (int)1,
						'msg' => (string)'success'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//getONG
			if ($msg == 'getONG' && in_array('getONG', $calls))
			{
				//Leemos los Datos
				$idong = (isset($fields['idong'])) ? (string)trim($fields['idong']) : '';
				
				//Verificamos
				if ($idong)
				{
					//Consultamos la Dependencia
					$query = $this->db->query('SELECT o.idong as idong,o.name as name,o.file as file,o.photo as photo,o.level as level,o.idstate as idstate,s.name as state FROM ong o INNER JOIN state s ON s.idstate = o.idstate WHERE o.idong = '.$idong.' AND o.status = 1');
				
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $query->row()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Debes enviar el identificador de la ONG.',
						'data' => ''
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// deleteONG
			if ($msg == 'deleteONG' && in_array('deleteONG', $calls))
			{
				//Leemos el Id ONG
				$idong = (isset($fields['idong'])) ? (string)trim($fields['idong']) : '';
				
				//Verificamos
				if ($idong)
				{
					//Actualizamos el status
					$data = array(
						'status' => 0
					);
					$this->db->where('idong', $idong);
					$this->db->update('ong', $data);
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $idong
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Faltan campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// activateONG
			if ($msg == 'activateONG' && in_array('activateONG', $calls))
			{
				//Leemos el Id ONG
				$idong = (isset($fields['idong'])) ? (string)trim($fields['idong']) : '';
				
				//Verificamos
				if ($idong)
				{
					//Actualizamos el status
					$data = array(
						'status' => 1
					);
					$this->db->where('idong', $idong);
					$this->db->update('ong', $data);
					
					//Generamos el Arreglo
					$array = array(
						'status' => 1,
						'msg' => 'success',
						'data' => $idong
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => 0,
						'msg' => 'Faltan campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getMails
			if ($msg == 'getMails' && in_array('getMails', $calls))
			{
				//Leemos los Campos
				$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
				$limit = '10';
				$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
				
				$query = $this->db->query("SELECT * FROM mail ORDER BY createdAt DESC LIMIT " . $offset . "," . $limit);
				$mails = $query->result();
				$query = $this->db->query("SELECT * FROM mail ORDER BY createdAt DESC");
				$mails_all = $query->result();
				
				//Generamos el Arreglo
				$array = array(
					'status' => 1,
					'msg' => 'success',
					'data' => array(
						'mails' => $mails,
						'mails_all' => $mails_all,
						'page' => $page
					)
				);
	
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
				
				die();
			}
			
			// getNewsletters
			if ($msg == 'getNewsletters' && in_array('getNewsletters', $calls))
			{
				//Leemos los Campos
				$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
				$limit = '10';
				$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
				
				$query = $this->db->query("SELECT * FROM newsletter ORDER BY createdAt DESC LIMIT " . $offset . "," . $limit);
				$newsletters = $query->result();
				$query = $this->db->query("SELECT * FROM newsletter ORDER BY createdAt DESC");
				$newsletters_all = $query->result();
				
				//Generamos el Arreglo
				$array = array(
					'status' => 1,
					'msg' => 'success',
					'data' => array(
						'newsletters' => $newsletters,
						'newsletters_all' => $newsletters_all,
						'page' => $page
					)
				);
	
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			
			// createNewsletter
			if ($msg == 'createNewsletter' && in_array('createNewsletter', $calls))
			{
				//Leemos los Campos
				$subject = (isset($fields['subject'])) ? (string)trim($fields['subject']) : '';
				$body = (isset($fields['body'])) ? (string)trim($fields['body']) : '';
				
				//Verificamos los datos
				if ($subject && $body)
				{
					//Creamos el Arreglo de Autoridad
					$data = array(
						'subject' => $subject,
						'body' => $body,
						'createdAt' => date('Y-m-d H:i:s'),
						'status' => 1
					);
					$this->db->insert('newsletter', $data);
					
					//Mostrar Resultados
					$array = array(
						'status' => 1,
						'msg' => 'success'
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => 0,
						'msg' => 'Faltan campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// getNewsletter
			if ($msg == 'getNewsletter' && in_array('getNewsletter', $calls))
			{
				//Leemos los Campos
				$idnewsletter = (isset($fields['idnewsletter'])) ? (string)trim($fields['idnewsletter']) : '';
				
				//Verificamos los Campos
				if ($idnewsletter)
				{
					//Consultamos si existe el Newsletter
					$query = $this->db->query("SELECT * FROM newsletter WHERE idnewsletter = " . $idnewsletter . " AND status = 1 LIMIT 1");
					
					//Verificamos si existe el Newsletter
					if ($query->num_rows() > 0)
					{
						//Leemos el Objeto
						$row = $query->row();
						
						//Generamos el Arreglo
						$array = array(
							'status' => 1,
							'msg' => 'success',
							'data' => array(
								'idnewsletter' => $row->idnewsletter,
								'subject' => $row->subject,
								'body' => $row->body
							)
						);
			
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => 0,
							'msg' => 'El Newsletter no existe. Intenta de nuevo.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => 0,
						'msg' => 'Faltan campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// updateNewsletter
			if ($msg == 'updateNewsletter' && in_array('updateNewsletter', $calls))
			{
				//Leemos los Campos
				$idnewsletter = (isset($fields['idnewsletter'])) ? (string)trim($fields['idnewsletter']) : '';
				$subject = (isset($fields['subject'])) ? (string)trim($fields['subject']) : '';
				$body = (isset($fields['body'])) ? (string)trim($fields['body']) : '';
				
				//Verificamos los datos
				if ($idnewsletter && $subject && $body)
				{
					//Consultamos si existe el Newsletter
					$query = $this->db->query("SELECT * FROM newsletter WHERE idnewsletter = " . $idnewsletter . " AND status = 1 LIMIT 1");
					
					//Verificamos que existe el Newsletter
					if ($query->num_rows() > 0)
					{
						//Creamos el Arreglo de Autoridad
						$data = array(
							'subject' => $subject,
							'body' => $body,
							'updatedAt' => date('Y-m-d H:i:s')
						);
						$this->db->where('idnewsletter', $idnewsletter);
						$this->db->update('newsletter', $data);
						
						//Mostrar Resultados
						$array = array(
							'status' => 1,
							'msg' => 'success',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => 0,
							'msg' => 'El Newsletter no existe. Intenta de nuevo.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => 0,
						'msg' => 'Faltan campos.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			//Generamos el Log
			$this->saveLog($msg,$fields,$app,$array, $origin, $ip);
		}
		else
		{
			//Mostrar Error
			$array = array(
				'status' => 0,
				'msg' => 'App no registrada. Registra tu app para consumir la API.',
				'origin' => $origin,
				'app' => $app,
				'apikey' => $apikey,
				'platform' => $platform
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
			
			//Generamos el Log
			$this->saveLog($msg,$fields,$app,$array,$origin,$ip);
		}
		
		
		if (!$output)
		{
			//Mostrar Error
			$array = array(
				'status' => (int)0,
				'msg' => (string)'Llamada inválida.'
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
			
			//Generamos el Log
			$this->saveLog($msg,$fields,$app,$array,$origin,$ip);
		}
	}

	public function get_timeago( $timestamp )
	{
	    $time_ago = strtotime($timestamp);  
		$current_time = time();  
		$time_difference = $current_time - $time_ago;  
		$seconds = $time_difference;  
		$minutes      = round($seconds / 60 );           // value 60 is seconds  
		$hours           = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec  
		$days          = round($seconds / 86400);          //86400 = 24 * 60 * 60;  
		$weeks          = round($seconds / 604800);          // 7*24*60*60;  
		$months          = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60  
		$years          = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60  
		
		if($seconds <= 60)  
		{  
			return "Justo ahora";  
		}  
		else if($minutes <=60)  
		{  
			if($minutes==1)  
			{  
				return "hace un minuto";  
			}  
			else  
			{  
				return "hace $minutes minutos";  
			}  
		}  
		else if($hours <=24)  
		{  
			if($hours==1)  
			{  
				return "hace una hora";  
			}  
			else  
			{  
				return "hace $hours horas";  
			}  
		}  
		else if($days <= 7)  
		{  
			if($days==1)  
			{  
				return "ayer";  
			}  
			else  
			{  
				return "hace $days días";  
			}  
		}  
		else if($weeks <= 4.3) //4.3 == 52/12  
		{  
			if($weeks==1)  
			{  
				return "hace una semana";  
			}  
			else  
			{  
				return "hace $weeks semanas";  
			}  
		}  
		else if($months <=12)  
		{  
			if($months==1)  
			{  
				return "hace un mes";  
			}  
			else  
			{  
				return "hace $months meses";  
			}  
		}  
		else  
		{  
			if($years==1)  
			{  
				return "hace un año";  
			}  
			else  
			{  
				return "hace $years años";  
			}  
		}  
	}
	
	public function saveLog( $msg,$fields,$app,$response,$origin,$ip )
	{
		//Verificamos que estén todos los datos
		if ( $msg && $fields && $app && $response)
		{
			//Generamos el registro del Log
			$data = array(
				'msg' => $msg,
				'fields' => json_encode($fields),
				'app' => $app,
				'response' => json_encode($response),
				'origin' => $origin,
				'ip' => $ip,
				'createdAt' => date('Y-m-d H:i:s'),
			    'status' => 1
			);
			$this->db->insert('log', $data);
		}
	}
	
}