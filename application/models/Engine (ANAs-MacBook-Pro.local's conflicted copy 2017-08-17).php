<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// How to send the response header with PHP
if (isset($_SERVER['HTTP_ORIGIN'])) {
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		exit(0);
}

ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 10000);

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Aws;
use Aws\Common\Enum\Size;
use Aws\S3\Enum\CannedAcl;
use Aws\Common\Enum\Region;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\Common\Model\MultipartUpload\AbstractUploadBuilder;

class Engine extends CI_Model {
	
	public $bucket = 'incorruptible'; //nombre del bucket

    //private $_key = 'AKIAIGA3MI32SFTQGHBQ'; // tu key
    private $_key = 'AKIAJ6COTGMIJ7WLYIOQ';

    //private $_secret = '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'; // tu secret
    private $_secret = 'Zv7UjMyRFq0/pnr1hOPXPPC++3KeoL8vpAVeYS2s';

    public $s3 = null;

	public function __construct()
	{
		parent::__construct();
		
		$this->s3 = S3Client::factory(array(
			'version' => '2006-03-01',
			'region' => 'us-west-2',
            'credentials' => array(
                'key'    => $this->_key,
                'secret' => $this->_secret,
            )
        ));
	}

	//Funcion para identar JSON
	public function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	public function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo $this->indent($json);
	}

	//Manejador de Mensajes Recibidos en el JSON
	public function executeJSON($msg,$fields,$app,$apikey)
	{
		//Leemos la Configuracion
		$output = FALSE;
		
		//s3signin
		if ($msg == 's3signin')
		{
			//Leemos los Datos
			$type = (isset($fields['type'])) ? (string)trim($fields['type']) : '';
			$filename = now();
			$s3Client = S3Client::factory(array(
				'version' => '2006-03-01',
				'region' => 'us-west-2',
				'credentials' => array(
	                'key'    => 'AKIAJRCNU3A53GB532PQ',
	                'secret' => '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',
	            )
	        ));
			
			//Agregamos la Extensión
			switch ($type)
			{
				case 'video' : $filename = $filename . '.mov'; break;
				case 'audio' : $filename = $filename . '.wav'; break;
				case 'video' : $filename = $filename . '.mov'; break;
			}
			
			$cmd = $s3Client->getCommand('GetObject', [
		        'Bucket' => 'incorruptible/'.$type,
		        'Key'    => $filename
		    ]);
		
		    $request = $s3Client->createPresignedRequest($cmd, '+20 minutes');
		    $presignedUrl = (string) $request->getUri();
		    $presignedUrl = str_replace('%2F', '/', $presignedUrl);
		   
			//Generamos el Arreglo
			$array = array(
				'status' => (int)1,
				'msg' => 'success',
				'data' => array(
					'filename' => $filename,
					'url' => $presignedUrl,
					'request' => $request
				)
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
		
		// loginAdmin
		if ($msg == 'loginAdmin')
		{
			//Leemos los datos
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los datos
			if ($email && $password)
			{
				//Consultamos los datos
				$query = $this->db->query("SELECT * FROM admin WHERE email = '" . $email . "' AND status = 1 LIMIT 1");

				//Verificamos si existe el usuario
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Leemos el Hash
					$hash = $row->password;
					
					//Verificamos el Password
					if (password_verify($password, $hash)) 
					{
						//Consultamos
						$data = array(
							'idadmin' => $row->idadmin,
						    'email' => $row->email,
						    'name' => $row->name
						);
	
						//Save Admin in $_SESSION
						$this->session->set_userdata('admin', $data);
						$this->session->set_userdata('iaap_logged', true);
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Contraseña incorrecta. Intenta de nuevo.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Correo Electrónico no existe. Intenta de nuevo.'
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}

		// loginUser
		if ($msg == 'loginUser')
		{
			//Leemos los datos
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los datos
			if ($name && $password)
			{
				//Verificar si es email
				if ($this->functions->verificaremail($name))
				{
					//Consultamos los datos
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $name . "' AND status = 1 LIMIT 1");
				}
				else
				{
					//Consultamos los datos
					$query = $this->db->query("SELECT * FROM user WHERE name = '" . $name . "' AND status = 1 LIMIT 1");
				}

				//Verificamos si existe el usuario
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Consultamos el Perfil del Usuario
					$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row->idprofile . " AND status = 1");
					$row_profile = $query_profile->row();
										
					//Consultamos las Denuncias del Usuario
					$query_complaints = $this->db->query("SELECT * FROM complaint WHERE iduser = " . $row->iduser . " AND status = 1");
					
					//Consultamos las Denuncias Seguidas del Usuario
					$query_follow_complaints = $this->db->query("SELECT fc.* FROM follow_complaint as fc LEFT JOIN complaint as c ON c.idcomplaint = fc.idcomplaint WHERE fc.iduser = " . $row->iduser . " AND fc.status = 1 AND c.status = 1");
					
					//Leemos el Hash
					$hash = $row->password;
					
					//Verificamos el Password
					if (password_verify($password, $hash)) 
					{
						//Consultamos
						$data = array(
							'iduser' => $row->iduser,
							'name' => $row->name,
						    'email' => $row->email,
						    'avatar' => $row->avatar,
						    'fbid' => $row->fbid,
						    'twid' => $row->twid,
						    'complaints' => $query_complaints->num_rows(),
						    'follows' => $query_follow_complaints->num_rows(),
						    'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name
						);
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Contraseña incorrecta. Intenta de nuevo.'
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Usuario no existe. Intenta de nuevo.'
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// createAdmin
		if ($msg == 'createUser')
		{
			//Leemos los Datos
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los Datos
			if ($name && $email && $password)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() == 0)
				{
					//Guardamos los Datos
					$data = array(
						'email' => $email,
						'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
					    'name' => $name,
					    'createdAt' => date('Y-m-d H:i:s'),
					    'idprofile' => 1,
					    'status' => 1
					);
					$this->db->insert('user', $data);
					$iduser = $this->db->insert_id();
					
					//Consultamos el Perfil del Usuario
					$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = 1 AND status = 1");
					$row_profile = $query_profile->row();
					
					$data = array(
						'iduser' => $iduser,
						'email' => $email,
						'password' => $password,
						'name' => $name,
						'idprofile' => $row_profile->idprofile,
					    'profile' => $row_profile->name,
						'link' => 'https://app.incorruptible.mx'
					);
					
					$data_result = array(
						'iduser' => $iduser,
						'name' => $name,
						'email' => $email,
						'avatar' => '',
						'idprofile' => $row_profile->idprofile,
					    'profile' => $row_profile->name
					);

					//Correo de Notificación
					$subject = 'Bienvenido a Incorruptible';
					$body = $this->load->view('mail/welcome_user', $data, TRUE);
					$altbody = strip_tags($body);

					//Verificamos
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Mandamos el Correo
						$mail = new PHPMailer(true);
						$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
						$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
						$mail->CharSet = 'UTF-8';
						$mail->Encoding = "quoted-printable";
				        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
				        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
				        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
				        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
				        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
				        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
				        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
				        $mail->SMTPDebug  = 0;
				        $mail->SetFrom("incorruptible@nodo.pw", utf8_encode('Incorruptible App'));  //Quien envía el correo
				        $mail->AddReplyTo("incorruptible@nodo.pw", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
				        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
				        $mail->Body      = $body;
				        $mail->AltBody    = $altbody;
				        $mail->AddAddress($email, $name);
				        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');

				        if ($mail->Send())
						{
							$response = true;
						}
						else
						{
							$response = false;
						}
					}

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data_result
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Este correo de usuario ya esta registrado.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'fields' => $fields
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// forgotPassword
		if ($msg == 'forgotPassword')
		{
			//Leemos los Datos
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			
			//Verificamos los Datos
			if ($email)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Revisamos que el Usuario no sea de Facebook
					if ((string)trim($row->fbid))
					{
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'Este usuario usa Facebook para iniciar sesión.'
						);
					}
					else
					{
						//Token
						$token = sha1(now());
						//$token = substr($code, 0, 8);
						
						//Creamos el código de invite
						$data = array(
							'token' => $token
						);
						$this->db->where('iduser', $row->iduser);
						$this->db->update('user', $data);
						
						//Enviamos el Correo
						$name = $row->name;
						$name_array = explode(' ', $name);
						$data['name'] = $name_array[0];
						$data['token'] = 'http://app.incorruptible.mx/#/reset/' . $row->iduser . '/' . $token;
						
						//Correo de Notificación
						$subject = 'Restablecer Contraseña';
						$body = $this->load->view('mail/reset_password', $data, TRUE);
						$altbody = strip_tags($body);
						$email = $row->email;
				
						//Verificamos
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Mandamos el Correo
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
					        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("incorruptible@nodo.pw", utf8_encode('Incorruptible App'));  //Quien envía el correo
					        $mail->AddReplyTo("incorruptible@nodo.pw", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email, $name);
					        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
				
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'Se enviará un correo electrónico con un link para restablecer la contraseña.'
						);
					}

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Este correo de usuario no esta registrado.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'fields' => $fields
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// resetPassword
		if ($msg == 'resetPassword')
		{
			//Leemos los Datos
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			$token = (isset($fields['token'])) ? (string)trim($fields['token']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
			
			//Verificamos los Datos
			if ($iduser && $token && $password)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM user WHERE iduser = '" . $iduser . "' AND token = '" . $token . "' LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Revisamos que el Usuario no sea de Facebook
					if ((string)trim($row->fbid))
					{
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'Este usuario usa Facebook para iniciar sesión.'
						);
					}
					else
					{
						//Creamos el código de invite
						$data = array(
							'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
							'token' => ''
						);
						$this->db->where('iduser', $row->iduser);
						$this->db->update('user', $data);
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'Se ha actualizado la contraseña correctamente.'
						);
					}

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'El token ya ha sido utilizado.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'fields' => $fields
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getUsers
		if ($msg == 'getUsers')
		{
			//Leemos los Campos
			$page = (isset($_POST['page'])) ? (string)trim($_POST['page']) : '1';
			$limit = '10';
			$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);
			
			$query = $this->db->query("SELECT * FROM user WHERE status = 1 AND idprofile = 1 ORDER BY name ASC LIMIT " . $offset . "," . $limit);
			$users = $query->result();
			$query = $this->db->query("SELECT * FROM user WHERE status = 1 AND idprofile = 1 ORDER BY name ASC");
			$users_all = $query->result();
			
			//Generamos el Arreglo
			$array = array(
				'status' => (int)1,
				'msg' => 'success',
				'data' => array(
					'users' => $users,
					'users_all' => $users_all,
					'page' => $page
				)
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
		
		// getAdminProfile
		if ($msg == 'getAdminProfile')
		{
			//Leemos los Campos
			$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '';

			//Verificamos los Datos
			if ($idadmin)
			{
				//Consultamos los Datos del Admin
				$query = $this->db->query("SELECT * FROM admin WHERE idadmin = " . $idadmin . " AND status = 1 LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Leemos el objeto
					$row = $query->row();

					//Generamos el Elemento de Admin
					$admin = array(
						'idadmin' => $row->idadmin,
						'name' => $row->name,
						'email' => $row->email
					);

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $admin
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe este administrador.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}

		}

		// updateProfileAdmin
		if ($msg == 'updateProfileAdmin')
		{
			//Leemos los Datos
			$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '';
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los Datos
			if ($idadmin && $name)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM admin WHERE idadmin = " . $idadmin . " AND status = 1 LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Actualizamos los Datos
					$data = array();
					$data['name'] = $name;
					if ($password) { $data['password'] = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]); }
					$this->db->where('idadmin', $idadmin);
					$this->db->update('admin', $data);

					//Actualizamos los datos de SESSION
					$data = array(
						'idadmin' => $idadmin,
					    'email' => $email,
					    'name' => $name
					);
					$this->session->set_userdata('admin', $data);

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'Administrador actualizado con éxito.',
						'data' => $data
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe este administrador.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}

		// createAdmin
		if ($msg == 'createAdmin')
		{
			//Leemos los Datos
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los Datos
			if ($name && $email && $password)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM admin WHERE email = '" . $email . "' LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() == 0)
				{
					//Guardamos los Datos
					$data = array(
						'email' => $email,
						'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
					    'name' => $name,
					    'createdAt' => date('Y-m-d H:i:s'),
					    'status' => 1
					);
					$this->db->insert('admin', $data);
					$idadmin = $this->db->insert_id();
					
					$data = array(
						'idadmin' => $idadmin,
						'email' => $email,
						'password' => $password,
						'name' => $name,
						'link' => base_url()
					);

					//Correo de Notificación
					$subject = 'Bienvenido a Incorruptible';
					$body = $this->load->view('mail/welcome_admin', $data, TRUE);
					$altbody = strip_tags($body);

					//Verificamos
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Mandamos el Correo
						$mail = new PHPMailer(true);
						$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
						$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
						$mail->CharSet = 'UTF-8';
						$mail->Encoding = "quoted-printable";
				        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
				        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
				        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
				        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
				        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
				        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
				        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
				        $mail->SMTPDebug  = 0;
				        $mail->SetFrom("incorruptible@nodo.pw", utf8_encode('Incorruptible App'));  //Quien envía el correo
				        $mail->AddReplyTo("incorruptible@nodo.pw", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
				        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
				        $mail->Body      = $body;
				        $mail->AltBody    = $altbody;
				        $mail->AddAddress($email, $name);
				        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');

				        if ($mail->Send())
						{
							$response = true;
						}
						else
						{
							$response = false;
						}
					}

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Este correo de administrador ya esta registrado.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}

		// getAdmins
		if ($msg == 'getAdmins')
		{
			//Leemos los Datos
			$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
			$limit = '10';
			$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);

			//Consultamos
			$query = $this->db->query("SELECT * FROM admin WHERE status = 1 ORDER BY idadmin DESC LIMIT " . $offset . "," . $limit);
			$query_all = $this->db->query("SELECT * FROM admin WHERE status = 1");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Admins
				$admins = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$admins[] = array(
						'idadmin' => $row->idadmin,
						'email' => $row->email,
						'name' => $row->name,
						'status' => $row->status
					);
				}

				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $admins,
					'page' => $page,
					'offset' => $offset,
					'limit' => $limit,
					'total' => (string)$query_all->num_rows()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay administradores.',
					'data' => array(),
					'page' => $page,
					'offset' => $offset,
					'limit' => $limit,
					'total' => (string)$query_all->num_rows()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}

		// getAdmin
		if ($msg == 'getAdmin')
		{
			//Leemos los Datos
			$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '1';

			//Verificamos
			if ($idadmin)
			{
				//Consultamos
				$query = $this->db->query("SELECT * FROM admin WHERE idadmin = " . $idadmin . " LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admin
					$row = $query->row();

					//Generamos el Elemento de Area
					$admin = array(
						'idadmin' => $row->idadmin,
						'email' => $row->email,
						'name' => $row->name,
						'status' => $row->status
					);

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $admin
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe el administrador.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// deleteAdmin
		if ($msg == 'deleteAdmin')
		{
			//Leemos el Id Admin
			$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '';
			
			//Verificamos
			if ($idadmin)
			{
				//Actualizamos el status
				$data = array(
					'status' => 0
				);
				$this->db->where('idadmin', $idadmin);
				$this->db->update('admin', $data);
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $idadmin
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}

		// updateAdmin
		if ($msg == 'updateAdmin')
		{
			//Leemos los Datos
			$idadmin = (isset($fields['idadmin'])) ? (string)trim($fields['idadmin']) : '';
			$name = (isset($fields['name'])) ? (string)trim($fields['name']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los Datos
			if ($idadmin && $name)
			{
				//Consultamos al Admin
				$query = $this->db->query("SELECT * FROM admin WHERE idadmin = " . $idadmin . " AND status = 1 LIMIT 1");

				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Actualizamos los Datos
					$data = array();
					$data['name'] = $name;
					if ($password) { $data['password'] = sha1($password); }
					$this->db->where('idadmin', $idadmin);
					$this->db->update('admin', $data);

					//Actualizamos los datos de SESSION
					$data = array(
						'idadmin' => $idadmin,
					    'email' => $email,
					    'name' => $name
					);
					$this->session->set_userdata('admin', $data);

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'Administrador actualizado con éxito.',
						'data' => $data
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe este administrador.',
						'data' => array()
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getCategories
		if ($msg == 'getCategories')
		{
			//Leemos los Datos
			$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
			$limit = '10';
			$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);

			//Consultamos
			$query = $this->db->query("SELECT * FROM category WHERE category.status = 1 ORDER BY category.order ASC");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Admins
				$categories = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$categories[] = array(
						'idcategory' => $row->idcategory,
						'name' => $row->name,
						'order' => $row->order
					);
				}

				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $categories
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay categorías.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getAmounts
		if ($msg == 'getAmounts')
		{
			//Leemos los Datos
			$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '1';
			$limit = '10';
			$offset = ($page == 1) ? '0' : (string)(($page-1)*(int)$limit);

			//Consultamos
			$query = $this->db->query("SELECT * FROM amount WHERE amount.status = 1 ORDER BY amount.order ASC");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Admins
				$amounts = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$amounts[] = array(
						'idamount' => $row->idamount,
						'value' => $row->value,
						'order' => $row->order
					);
				}

				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $amounts
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay cantidades asignadas.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// saveComplaint
		if ($msg == 'saveComplaint')
		{
			//Leemos los Datos
			$title = (isset($fields['title'])) ? (string)trim($fields['title']) : '';
			$description = (isset($fields['description'])) ? (string)trim($fields['description']) : '';
			$lat = (isset($fields['lat'])) ? (string)trim($fields['lat']) : '';
			$lng = (isset($fields['lng'])) ? (string)trim($fields['lng']) : '';
			$address = (isset($fields['address'])) ? (string)trim($fields['address']) : '';
			$type = (isset($fields['type'])) ? (string)trim($fields['type']) : '';
			$media = (isset($fields['media'])) ? (string)trim($fields['media']) : '';
			$idcategory = (isset($fields['idcategory'])) ? (string)trim($fields['idcategory']) : '';
			$idamount = (isset($fields['idamount'])) ? (string)trim($fields['idamount']) : '';
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos Data
			if ($title && $description && $address && $type && $idcategory && $idamount && $iduser)
			{
				//Guardamos los Datos
				$data = array(
					'title' => $title,
					'description' => $description,
				    'lat' => $lat,
				    'lng' => $lng,
				    'address' => $address,
				    'type' => $type,
				    'media' => $media,
				    'idcategory' => $idcategory,
				    'idamount' => $idamount,
				    'icon' => 1,
				    'iduser' => $iduser,
				    'createdAt' => date('Y-m-d H:i:s'),
				    'status' => 1
				);
				$this->db->insert('complaint', $data);
				$idcomplaint = $this->db->insert_id();
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $idcomplaint
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getComplaints
		if ($msg == 'getComplaints')
		{
			//Consultamos
			$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 ORDER BY c.idcomplaint ASC");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Admins
				$complaints = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$complaints[] = array(
						'idcomplaint' => $row->idcomplaint,
						'title' => $row->title,
						'description' => $row->description,
						'lat' => $row->lat,
						'lng' => $row->lng,
						'address' => $row->address,
						'type' => $row->type,
						'media' => $row->media,
						'idcategory' => $row->idcategory,
						'category' => $row->category,
						'idamount' => $row->idamount,
						'amount' => $row->amount,
						'icon' => $row->icon,
						'iduser' => $row->iduser,
						'date' => $row->date
					);
				}

				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $complaints
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay denuncias creadas.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getComplaintsUser
		if ($msg == 'getComplaintsUser')
		{
			//Leer Valores
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos
			if ($iduser)
			{
				//Consultamos
				$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 AND c.iduser = ".$iduser." ORDER BY c.idcomplaint DESC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$complaints = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Area
						$complaints[] = array(
							'idcomplaint' => $row->idcomplaint,
							'title' => $row->title,
							'description' => $row->description,
							'lat' => $row->lat,
							'lng' => $row->lng,
							'address' => $row->address,
							'type' => $row->type,
							'media' => $row->media,
							'idcategory' => $row->idcategory,
							'category' => $row->category,
							'idamount' => $row->idamount,
							'amount' => $row->amount,
							'icon' => $row->icon,
							'iduser' => $row->iduser,
							'date' => $row->date
						);
					}
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $complaints
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay denuncias creadas.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getComplaint
		if ($msg == 'getComplaint')
		{
			//Leer los Valores
			$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
			
			//Verificamos
			if ($idcomplaint)
			{
				//Consultamos
				$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 AND c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomplaint ASC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$row = $query->row();
					$comments = array();
					
					//Consultamos los Comentarios
					$query_comments = $this->db->query("SELECT c.idcomment_complaint as idcomment_complaint, c.text as text, c.idamount as idamount, amount.value as amount, c.iduser as iduser, c.createdAt as date FROM comment_complaint c INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 AND c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomment_complaint DESC");
	
					//Generamos el Elemento de Area
					$complaint = array(
						'idcomplaint' => $row->idcomplaint,
						'title' => $row->title,
						'description' => $row->description,
						'lat' => $row->lat,
						'lng' => $row->lng,
						'address' => $row->address,
						'type' => $row->type,
						$row->type => $row->media,
						'idcategory' => $row->idcategory,
						'category' => $row->category,
						'idamount' => $row->idamount,
						'amount' => $row->amount,
						'icon' => $row->icon,
						'iduser' => $row->iduser,
						'date' => $row->date,
						'comments' => $row->comments,
						'follows' => $row->follows,
						'comments_data' => $query_comments->result()
					);
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $complaint
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe la denuncia.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getComplaint
		if ($msg == 'getComplaintAmounts')
		{
			//Leer los Valores
			$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
			
			//Verificamos
			if ($idcomplaint)
			{
				//Consultamos
				$query = $this->db->query("SELECT c.idcomplaint as idcomplaint, c.title as title, c.description as description, c.lat as lat, c.lng as lng, c.address as address, c.type as type, c.media as media, c.idcategory as idcategory, c.idamount as idamount, c.icon as icon, category.name as category, amount.value as amount, c.iduser as iduser, c.createdAt as date, (SELECT COUNT(*) FROM comment_complaint WHERE comment_complaint.idcomplaint = c.idcomplaint) as comments, (SELECT COUNT(*) FROM follow_complaint WHERE follow_complaint.idcomplaint = c.idcomplaint) as follows FROM complaint c INNER JOIN category ON c.idcategory = category.idcategory INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 AND c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomplaint ASC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Admins
					$row = $query->row();
					$comments = array();
					
					//Consultamos los Comentarios
					$query_comments = $this->db->query("SELECT c.idcomment_complaint as idcomment_complaint, c.text as text, c.idamount as idamount, amount.value as amount, c.iduser as iduser, c.createdAt as date FROM comment_complaint c INNER JOIN amount ON c.idamount = amount.idamount WHERE c.status = 1 AND c.idcomplaint = " . $idcomplaint . " ORDER BY c.idcomment_complaint DESC");
	
					//Consultamos
					$query_amounts = $this->db->query("SELECT * FROM amount WHERE amount.status = 1 ORDER BY amount.order ASC");
					
					//Declaramos el Arreglo de Admins
					$amounts = array();
	
					//Procesamos los Admins
					foreach ($query_amounts->result() as $row_amount)
					{
						//Generamos el Elemento de Area
						$amounts[] = array(
							'idamount' => $row_amount->idamount,
							'value' => $row_amount->value,
							'order' => $row_amount->order
						);
					}
					
					//Generamos el Elemento de Area
					$complaint = array(
						'idcomplaint' => $row->idcomplaint,
						'title' => $row->title,
						'description' => $row->description,
						'lat' => $row->lat,
						'lng' => $row->lng,
						'address' => $row->address,
						'type' => $row->type,
						$row->type => $row->media,
						'idcategory' => $row->idcategory,
						'category' => $row->category,
						'idamount' => $row->idamount,
						'amount' => $row->amount,
						'icon' => $row->icon,
						'iduser' => $row->iduser,
						'date' => $row->date,
						'comments' => $row->comments,
						'follows' => $row->follows,
						'comments_data' => $query_comments->result(),
						'amounts' => $amounts
					);
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $complaint
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe la denuncia.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// followComplaint
		if ($msg == 'followComplaint')
		{
			//Leemos los Datos
			$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos
			if ($idcomplaint && $iduser)
			{
				//Consultamos la Denuncia
				$query = $this->db->query("SELECT * FROM complaint WHERE idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
				
				//Verificamos
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Verificamos si el usuario no es el dueño
					if ($row->iduser != $iduser)
					{
						//Consultamos si el usuario es valido
						$query_user = $this->db->query("SELECT * FROM user WHERE iduser = ".$iduser." AND status = 1 LIMIT 1");
						
						//Verificamos si el usuario existe
						if ($query_user->num_rows() > 0)
						{
							//Consultamos la relación de la denuncia con el usuario
							$query_follow = $this->db->query("SELECT * FROM follow_complaint WHERE iduser = ".$iduser." AND idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
							
							//Verificamos que no exista la relacion
							if ($query_follow->num_rows() == 0)
							{
								//Generamos el Registro de Follow
								$data = array(
									'iduser' => $iduser,
									'idcomplaint' => $idcomplaint,
									'createdAt' => date('Y-m-d H:i:s'),
									'status' => 1
								);
								$this->db->insert('follow_complaint', $data);
								$idfollow_complaint = $this->db->insert_id();
								
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success',
									'data' => $idfollow_complaint
								);
			
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Mostrar Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'Tu ya sigues a esta denuncia.',
									'data' => array()
								);
				
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Mostrar Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'No existe el usuario o esta deshabilitado.',
								'data' => array()
							);
			
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Tu eres el propietario de la denuncia.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe la denuncia o esta deshabilitada.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// loginFacebook
		if ($msg == 'loginFacebook')
		{
			//Read Values
			$token = (string)trim($fields['token']) ? (string)trim($fields['token']) : '';
			
			//Verificamos
			if ($token)
			{
				//Obtenemos la información
				$usuario = $this->facebook->responseToken($token);
				
				//Verificamos
				if (isset($usuario['id']))
				{
					//Consultamos los datos
					$query = $this->db->query("SELECT * FROM user WHERE email = '" . $usuario['email'] . "' AND status = 1 LIMIT 1");
	
					//Verificamos si existe el usuario
					if ($query->num_rows() > 0)
					{
						//Leemos el Objeto
						$row = $query->row();
						
						//Consultamos el Perfil del Usuario
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row->idprofile . " AND status = 1");
						$row_profile = $query_profile->row();
						
						//Consultamos las Denuncias del Usuario
						$query_complaints = $this->db->query("SELECT * FROM complaint WHERE iduser = " . $row->iduser . " AND status = 1");
						
						//Consultamos las Denuncias Seguidas del Usuario
						$query_follow_complaints = $this->db->query("SELECT * FROM follow_complaint WHERE iduser = " . $row->iduser . " AND status = 1");
						
						//Actualizamos los datos
						$data = array(
							'fbid' => $usuario['id'],
							'fbtoken' => $usuario['token'],
							'avatar' => $usuario['avatar'],
							'updatedAt' => date('Y-m-d H:i:s')
						);
						$this->db->where('iduser', $row->iduser);
						$this->db->update('user', $data);
						
						//Consultamos
						$data = array(
							'iduser' => $row->iduser,
							'name' => $row->name,
						    'email' => $row->email,
						    'avatar' => $usuario['avatar'],
						    'fbid' => $usuario['id'],
						    'twid' => $row->twid,
						    'complaints' => $query_complaints->num_rows(),
						    'follows' => $query_follow_complaints->num_rows(),
						    'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name
						);
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Guardamos los Datos
						$data = array(
							'email' => $usuario['email'],
							'password' => password_hash($usuario['id'], PASSWORD_DEFAULT, ['cost' => 12]),
						    'name' => $usuario['first_name'].' '.$usuario['last_name'],
						    'fbid' => $usuario['id'],
							'fbtoken' => $usuario['token'],
							'avatar' => $usuario['avatar'],
						    'createdAt' => date('Y-m-d H:i:s'),
						    'idprofile' => 1,
						    'status' => 1
						);
						$this->db->insert('user', $data);
						$iduser = $this->db->insert_id();
						
						//Consultamos el Perfil del Usuario
						$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = 1 AND status = 1");
						$row_profile = $query_profile->row();
						
						$data = array(
							'iduser' => $iduser,
							'email' => $usuario['email'],
							'name' => $usuario['first_name'].' '.$usuario['last_name'],
							'link' => 'https://app.incorruptible.mx'
						);
						
						$data_result = array(
							'iduser' => $iduser,
							'name' => $usuario['first_name'].' '.$usuario['last_name'],
							'email' => $usuario['email'],
							'avatar' => $usuario['avatar'],
							'fbid' => $usuario['id'],
							'twid' => '',
							'complaints' => '0',
						    'follows' => '0',
						    'idprofile' => $row_profile->idprofile,
						    'profile' => $row_profile->name,
						);
	
						//Correo de Notificación
						$subject = 'Bienvenido a Incorruptible';
						$body = $this->load->view('mail/welcome_user', $data, TRUE);
						$altbody = strip_tags($body);
	
						//Verificamos
						if ($subject != '' && $body != '' && $altbody != '' && $email != '')
						{
							//Mandamos el Correo
							$mail = new PHPMailer(true);
							$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
							$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
							$mail->CharSet = 'UTF-8';
							$mail->Encoding = "quoted-printable";
					        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
					        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
					        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
					        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
					        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
					        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
					        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
					        $mail->SMTPDebug  = 0;
					        $mail->SetFrom("incorruptible@nodo.pw", utf8_encode('Incorruptible App'));  //Quien envía el correo
					        $mail->AddReplyTo("incorruptible@nodo.pw", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
					        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
					        $mail->Body      = $body;
					        $mail->AltBody    = $altbody;
					        $mail->AddAddress($email, $name);
					        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');
	
					        if ($mail->Send())
							{
								$response = true;
							}
							else
							{
								$response = false;
							}
						}
	
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $data_result
						);
	
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Ocurrió un error al procesar la información.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// joinComplaint
		if ($msg == 'joinComplaint')
		{
			//Leemos los datos
			$text = (isset($fields['text'])) ? (string)trim($fields['text']) : '';
			$idamount = (isset($fields['idamount'])) ? (string)trim($fields['idamount']) : '';
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			$idcomplaint = (isset($fields['idcomplaint'])) ? (string)trim($fields['idcomplaint']) : '';
			
			//Verificamos los Datos
			if ($text && $idamount && $iduser && $idcomplaint)
			{
				//Verificamos si existe la denuncia
				$query = $this->db->query("SELECT * FROM complaint WHERE idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
				
				//Verificamos
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Verificamos si el usuario no es el dueño
					if ($row->iduser != $iduser)
					{
						//Consultamos si el usuario es valido
						$query_user = $this->db->query("SELECT * FROM user WHERE iduser = ".$iduser." AND status = 1 LIMIT 1");
						
						//Verificamos si el usuario existe
						if ($query_user->num_rows() > 0)
						{
							//Consultamos la relación de la denuncia con el usuario
							$query_comment = $this->db->query("SELECT * FROM comment_complaint WHERE iduser = ".$iduser." AND idcomplaint = ".$idcomplaint." AND status = 1 LIMIT 1");
							
							//Verificamos que no exista la relacion
							if ($query_comment->num_rows() == 0)
							{
								//Generamos el Registro de Follow
								$data = array(
									'text' => $text,
									'idamount' => $idamount,
									'iduser' => $iduser,
									'idcomplaint' => $idcomplaint,
									'createdAt' => date('Y-m-d H:i:s'),
									'status' => 1
								);
								$this->db->insert('comment_complaint', $data);
								$idcomment_complaint = $this->db->insert_id();
								
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success',
									'data' => $idcomment_complaint
								);
			
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Mostrar Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'Tu ya te uniste a esta denuncia.',
									'data' => array()
								);
				
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Mostrar Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'No existe el usuario o esta deshabilitado.',
								'data' => array()
							);
			
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Tu eres el propietario de la denuncia.',
							'data' => array()
						);
		
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No existe la denuncia o esta deshabilitada.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// loginTwitter
		if ($msg == 'loginTwitter')
		{
			//Read Values
			$email = (string)trim($fields['email']) ? (string)trim($fields['email']) : '';			    
			$name = (string)trim($fields['name']) ? (string)trim($fields['name']) : '';
			$twid = (string)trim($fields['twid']) ? (string)trim($fields['twid']) : '';
			$twtoken = (string)trim($fields['twtoken']) ? (string)trim($fields['twtoken']) : '';
			$avatar = (string)trim($fields['avatar']) ? (string)trim($fields['avatar']) : '';
			
			//Verificamos
			if ($email && $name && $twid && $twtoken && $avatar)
			{
				//Consultamos los datos
				$query = $this->db->query("SELECT * FROM user WHERE email = '" . $email . "' AND status = 1 LIMIT 1");

				//Verificamos si existe el usuario
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Consultamos el Perfil del Usuario
					$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = " . $row->idprofile . " AND status = 1");
					$row_profile = $query_profile->row();
					
					//Consultamos las Denuncias del Usuario
					$query_complaints = $this->db->query("SELECT * FROM complaint WHERE iduser = " . $row->iduser . " AND status = 1");
					
					//Consultamos las Denuncias Seguidas del Usuario
					$query_follow_complaints = $this->db->query("SELECT * FROM follow_complaint WHERE iduser = " . $row->iduser . " AND status = 1");
					
					//Actualizamos los datos
					$data = array(
						'name' => $name,
						'twid' => $twid,
						'twtoken' => $twtoken,
						'avatar' => $avatar,
						'updatedAt' => date('Y-m-d H:i:s')
					);
					$this->db->where('iduser', $row->iduser);
					$this->db->update('user', $data);
					
					//Consultamos
					$data = array(
						'iduser' => $row->iduser,
						'name' => $name,
					    'email' => $row->email,
					    'avatar' => $avatar,
					    'fbid' => $row->fbid,
					    'twid' => $twid,
					    'complaints' => $query_complaints->num_rows(),
					    'follows' => $query_follow_complaints->num_rows(),
					    'idprofile' => $row_profile->idprofile,
					    'profile' => $row_profile->name
					);
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Guardamos los Datos
					$data = array(
						'email' => $email,
						'password' => password_hash($twid, PASSWORD_DEFAULT, ['cost' => 12]),
					    'name' => $name,
					    'twid' => $twid,
						'twtoken' => $twtoken,
						'avatar' => $avatar,
					    'createdAt' => date('Y-m-d H:i:s'),
					    'idprofile' => 1,
					    'status' => 1
					);
					$this->db->insert('user', $data);
					$iduser = $this->db->insert_id();
					
					//Consultamos el Perfil del Usuario
					$query_profile = $this->db->query("SELECT * FROM profile WHERE idprofile = 1 AND status = 1");
					$row_profile = $query_profile->row();
					
					$data = array(
						'iduser' => $iduser,
						'email' => $email,
						'name' => $name,
						'link' => 'https://app.incorruptible.mx'
					);
					
					$data_result = array(
						'iduser' => $iduser,
						'name' => $name,
						'email' => $email,
						'avatar' => $avatar,
						'fbid' => '',
						'twid' => $twid,
						'complaints' => '0',
					    'follows' => '0',
					    'idprofile' => $row_profile->idprofile,
					    'profile' => $row_profile->name,
					);

					//Correo de Notificación
					$subject = 'Bienvenido a Incorruptible';
					$body = $this->load->view('mail/welcome_user', $data, TRUE);
					$altbody = strip_tags($body);

					//Verificamos
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Mandamos el Correo
						$mail = new PHPMailer(true);
						$mail->Headers  = 'MIME-Version: 1.0' . "\r\n";
						$mail->Headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
						$mail->CharSet = 'UTF-8';
						$mail->Encoding = "quoted-printable";
				        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
				        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
				        $mail->SMTPSecure = "tls";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
				        $mail->Host       = "email-smtp.us-west-2.amazonaws.com";      // establecemos GMail como nuestro servidor SMTP
				        $mail->Port       = 587;                   // establecemos el puerto SMTP en el servidor de GMail
				        $mail->Username   = "AKIAIHOX43ECBLVJQAYA";  // la cuenta de correo GMail
				        $mail->Password   = "Avvy7y5bdNxj0f2Sox3p8UVg0puYthHqpOPfhafw57eC";            // password de la cuenta GMail
				        $mail->SMTPDebug  = 0;
				        $mail->SetFrom("incorruptible@nodo.pw", utf8_encode('Incorruptible App'));  //Quien envía el correo
				        $mail->AddReplyTo("incorruptible@nodo.pw", utf8_encode("Incorruptible App"));  //A quien debe ir dirigida la respuesta
				        $mail->Subject    = utf8_encode("=?UTF-8?B?" . base64_encode($subject) .  "?=");  //Asunto del mensaje
				        $mail->Body      = $body;
				        $mail->AltBody    = $altbody;
				        $mail->AddAddress($email, $name);
				        //$mail->AddBCC('milio.hernandez@gmail.com', 'Emiliano Hernández');

				        if ($mail->Send())
						{
							$response = true;
						}
						else
						{
							$response = false;
						}
					}

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data_result
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// getBlog
		if ($msg == 'getBlog')
		{
			//Leemos el Blog
			//$feed = new SimpleXMLElement(file_get_contents('http://incorruptible.mx/feed'));
			$feed = new SimpleXMLElement(file_get_contents('http://incorruptible.mx/feedmedia'));
			$items = array();
			foreach ($feed->channel->item as $item)
			{
				$items[] = $item;
			}
			
			//Generamos el Arreglo
			$array = array(
				'status' => (int)1,
				'msg' => 'success',
				'data' => $items
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
		
		// getNews
		if ($msg == 'getNews')
		{
			//Leemos el Blog
			//$feed = new SimpleXMLElement(file_get_contents('http://incorruptible.mx/feed'));
			$feed = new SimpleXMLElement(file_get_contents('http://incorruptible.mx/feednoticias'));
			$items = array();
			foreach ($feed->channel->item as $item)
			{
				$items[] = $item;
			}
			
			//Generamos el Arreglo
			$array = array(
				'status' => (int)1,
				'msg' => 'success',
				'data' => $items
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
		
		// getDependencies
		if ($msg == 'getDependencies')
		{
			//Consultamos
			$query = $this->db->query("SELECT * FROM dependency WHERE dependency.status = 1 ORDER BY dependency.iddependency ASC");

			//Verificamos si existe el Admin
			if ($query->num_rows() > 0)
			{
				//Declaramos el Arreglo de Dependencias
				$dependencies = array();

				//Procesamos los Admins
				foreach ($query->result() as $row)
				{
					//Generamos el Elemento de Area
					$dependencies[] = array(
						'iddependency' => $row->iddependency,
						'name' => $row->name
					);
				}

				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $dependencies
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay dependencias asignadas.',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		//saveGroupComplaints
		if ($msg == 'saveGroupComplaints')
		{
			//Leemos los Valores
			$title = (isset($fields['title'])) ? (string)trim($fields['title']) : '';
			$description = (isset($fields['description'])) ? (string)trim($fields['description']) : '';
			$level = (isset($fields['level'])) ? (string)trim($fields['level']) : '';
			$dependency = (isset($fields['dependency'])) ? (string)trim($fields['dependency']) : '';
			$complaints = (isset($fields['complaints'])) ? (string)trim($fields['complaints']) : '';
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos
			if ($title && $description && $level && $dependency && $complaints && $iduser)
			{
				//Creamos el Grupo
				$data = array(
					'title' => $title,
					'description' => $description,
					'level' => $level,
					'iddependency' => $dependency,
					'iduser' => $iduser,
					'send' => 0,
					'createdAt' => date('Y-m-d H:i:s'),
					'status' => 1
				);
				$this->db->insert('group', $data);
				$idgroup = $this->db->insert_id();
				
				//Procesamos las Denuncias
				$array_complaints = explode(',', $complaints);
				
				//Creamos la Relación de las Denuncias con el Grupo
				foreach ($array_complaints as $complaint)
				{
					//Generamos la Relación
					$data = array(
						'idgroup' => $idgroup,
						'idcomplaint' => $complaint,
						'createdAt' => date('Y-m-d H:i:s'),
						'status' => 1
					);
					$this->db->insert('group_complaint', $data);
					
					//Actualizamos el Icono de la Denuncia
					$data = array(
						'icon' => 2
					);
					$this->db->where('idcomplaint', $complaint);
					$this->db->update('complaint', $data);
				}
				
				//Mostrar Resultados
				$array = array(
					'status' => (int)1,
					'msg' => (string)'success',
					'data' => $idgroup
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		//getGroupsByUser
		if ($msg == 'getGroupsByUser')
		{
			//Leemos las Variables
			$iduser = (isset($fields['iduser'])) ? (string)trim($fields['iduser']) : '';
			
			//Verificamos el Valor
			if ($iduser)
			{
				//Consultamos todos los Grupos creados por el Usuario
				$query = $this->db->query("SELECT * FROM `group` g WHERE g.status = 1 AND g.iduser = " . $iduser . " ORDER BY g.idgroup ASC");
	
				//Verificamos si existe el Admin
				if ($query->num_rows() > 0)
				{
					//Declaramos el Arreglo de Dependencias
					$groups = array();
	
					//Procesamos los Admins
					foreach ($query->result() as $row)
					{
						//Generamos el Elemento de Grupos
						$groups[] = array(
							'idgroup' => $row->idgroup,
							'title' => $row->title,
							'description' => $row->description,
							'level' => $row->level,
							'iddependency' => $row->iddependency
						);
					}
	
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $groups
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'No hay grupos creados.',
						'data' => array()
					);
	
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos',
					'data' => array()
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		if (!$output)
		{
			//Mostrar Error
			$array = array(
				'status' => (int)0,
				'msg' => (string)'Llamada inválida.',
				'data' => $msg,
				'fields' => $fields
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
	}

}