<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Functions extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function call($param)
	{
		//extract data from the post
		extract($_POST);
		$fields_string = '';

		//set POST variables
		$url = base_url() . 'api';
		$fields = array
		(
			'param'=>urlencode($param)
		);

		//url-ify the data for the POST
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&amp;'; }
		rtrim($fields_string,'&amp;');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

		//execute post
		$result = curl_exec($ch);
		$status = curl_getinfo($ch);

		//close connection
		curl_close($ch);

		return $result;
	}
	
	public function clean($string) 
	{
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		$unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
		$string = strtr( $string, $unwanted_array );
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		$string = strtolower($string);

		return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}
	
	public function eliminarDir($carpeta)
	{
	    foreach(glob($carpeta . "/*") as $archivos_carpeta)
	    {
	        $archivos_carpeta;
	 
	        if (is_dir($archivos_carpeta))
	        {
	            eliminarDir($archivos_carpeta);
	        }
	        else
	        {
	            unlink($archivos_carpeta);
	        }
	    }
	 
	    rmdir($carpeta);
	}

	public function verificaremail($email)
	{ 
		$Sintaxis='#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';
		if(preg_match($Sintaxis,$email))
		{ 
	    	return TRUE; 
		} 
		else
		{ 
	       return FALSE; 
		} 
	}
}
